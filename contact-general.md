+++
title = "Contact information"
ogdescription = "Please do not send us your message!"
nodateline = true
noprevnext = true
notice = true
+++

{{% notice warning %}}
Please do <b>not</b> send us your message. We voluntarily provide this blog during leisure time, and are 100% independent. If you message us anyway, we will likely delete your message.
{{% /notice %}}

## Donations
We do not accept any donations since we are fully self-funded. Some websites ask for donations and promise something in return, or they collect money without paying taxes. We do not do this, and we aren't interested in any donations. Thanks.

If you encounter any websites that seem to collect donations on behalf of infosec-handbook.eu, please immediately report them.

## General support
If you need general support or have general questions regarding information security, please consider well-known websites like https://security.stackexchange.com/. As we provide this blog for free, we do not have the time to answer individual requests that aren't related to our blog posts. Thanks.

## Job offers
All contributors of infosec-handbook.eu are full-time employees and like their employers. We are absolutely not interested in any job offers. Thanks.

## Sponsoring / Social influencing
If you want to send us money to get your product/service sponsored, please keep it and don't contact us. We are also not interested in any kind of social influencing. We are 100% independent, and won't feature your product/service. Furthermore, we aren't interested in any free product samples, or exclusive test samples. (This is especially true for the Nitrokey UG which contacted us over and over again!) Thanks.
