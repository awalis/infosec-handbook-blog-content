+++
title = "Terminal tips"
ogdescription = "Security-related commands tips which we use often."
nodateline = true
noprevnext = true
+++

This page contains useful security-related command line tools and their commands, tested on Arch Linux. Commands on other Linux-based operating systems or Windows might differ and aren't included.

## Tools
* [cheat]({{< relref "#cheat" >}})
* [chromium]({{< relref "#chromium" >}})
* [curl]({{< relref "#curl" >}})
* [dig]({{< relref "#dig" >}})
* [gpg]({{< relref "#gpg" >}})
  * [How to create a GnuPG key]({{< relref "#gpg-gen" >}})
  * [Asymmetric encryption]({{< relref "#gpg-as" >}})
  * [Symmetric encryption]({{< relref "#gpg-s" >}})
* [imagemagick]({{< relref "#imagemagick" >}})
* [openssl]({{< relref "#openssl" >}})
* [pwgen]({{< relref "#pwgen" >}})
* [qrencode]({{< relref "#qrencode" >}})
* [subnetcalc]({{< relref "#subnetcalc" >}})
* [ykman]({{< relref "#ykman" >}})
* [zbarimg + oathtool]({{< relref "#zbarimg-oathtool" >}})
* [zmap]({{< relref "#zmap" >}})

{{< mastodonbox >}}

## cheat
cheat is a small tool to create and view interactive cheat sheets using the command line. For instance, enter `cheat gpg`, `cheat git`, `cheat openssl`, or `cheat nmap`. By default, cheat sheets are stored at `~/.cheat` and can be modified.

## chromium
The web browser Chromium can be configured by changing so-called switches. This allows you to restrict [cipher suites]({{< ref "glossary.md#cipher-suites" >}}) and the [TLS]({{< ref "glossary.md#tls" >}}) version used by Chromium. You could change the configuration in the terminal, however, future updates will overwrite this.

Therefore, create `~/.config/chromium-flags.conf` and add:

* `--cipher-suite-blacklist=0x009c,0x009d,0x002f,0x0035,0x000a,0xc013,0xc014` (disables weak cipher suites)
* `--ssl-version-min=tls1.2` (disables all [TLS]({{< ref "glossary.md#tls" >}}) versions except TLS 1.2)

Use [Qualys' SSL Client Test](https://www.ssllabs.com/ssltest/viewMyClient.html) to check if all weak cipher suites are disabled.

## curl
curl is a "command line tool and library for transferring data with URLs". It is basically a very handy multitool that supports many different network protocols. Some basic commands are:

* `curl --head [domainname]` displays HTTP response headers (including security-relevant headers).
* `curl --header "[header]" [domainname]` adds the `[header]` to your request.
* `curl --insecure https://[domainname]` connects to the domain and ignores any certificate errors.
* `curl --sslv3 https://[domainname]` connects to the domain using insecure SSLv3 (works also for other insecure SSL/TLS versions).
* `curl -u user:password -O ftp://[domainname]/[file]` downloads a file using username and password authentication via FTP.

There are many other options. Just test it and look at `man curl`.

## dig
dig is part of [BIND](https://www.isc.org/downloads/bind/) and can be used to check domains for DNSSEC:

* `dig [domain-name] +multiline`
  * "status" should be "NOERROR" ("SERVFAIL" means that there is a problem with the DNS server configuration, e.g., DNSSEC configuration is broken)
  * "flags" must contain "ad" (authentic data)
* `dig [domain-name] +multiline +dnssec`
  * This query sets the "DNSSEC OK" (DO) bit and requests DNSSEC records to be sent, if available
  * Look for "RRSIG" resource records
* `dig [domain-name] +trace`
  * This query emulates a DNS resolver. It starts from the root of the DNS hierarchy, and works down using iterative DNS queries.

## gpg
[GnuPG]({{< ref "glossary.md#gnupg" >}}) is mostly already installed on your machine and can be used for e-mail encryption and signing. You can use it in your terminal, of course.

### How to create a GnuPG key {#gpg-gen}
This section describes how to create an [Curve25519]({{< ref "/glossary.md#curve25519" >}}) key using gpg 2.2.13. If you don't know your version, open a terminal and enter `gpg --version`.

1. Open your terminal.
2. Enter `gpg --expert --full-generate-key`
3. Select `(9) ECC and ECC`
4. Select `(1) Curve 25519`
5. Enter validity period, and confirm it by entering `y`
  * for e-mail, we recommend `6m`
  * for Git signing, we recommend `1y`
  * for internal use, we recommend less than `2y`
6. Enter your full name
7. Enter your e-mail address
  * for e-mail, you need to enter your real e-mail address
  * for Git signing/internal use, you can enter an arbitrary address (e.g., `git@lenka.laptop`)
8. Enter a comment, if needed
9. Check everything, and confirm it
10. Enter a passphrase, used to locally encrypt your GnuPG key. You must enter this password every time you want to decrypt/sign something.
11. After creation, save the location of the revocation certificate. You need the certificate to revoke your key if you lose access to your private GPG key.
12. Your new key pair is ready now. Enter `gpg --list-secret-keys` to see it.
13. You can export your public key by entering `gpg --armor --export [key-id] > my-public-gpg-key.asc`

### Asymmetric encryption {#gpg-as}
We save our cleartext as `clear.txt`. You can also use `echo "your message"`, of course. The ciphertext is stored as `cipher.txt`.

* Encrypt and sign: `cat clear.txt | gpg -esar [key-id-of-the-recipient] -u [your-key-id] > cipher.txt`
  * `-e` means encrypt
  * `-s` means sign
  * `-a` means ASCII format
  * `-r` means encrypt for the following key id of the recipient
  * `-u` means use the following (your) key id for signing
* Decrypt: `cat cipher.txt | gpg -d > clear.txt`
  * `-d` means decrypt

### Symmetric encryption {#gpg-s}
gpg can be used to symmetrically encrypt data, too:

* Encrypt: `gpg -c --cipher-algo AES256 clear.txt`
  * `-c` means symmetrically encrypt
  * `--cipher-algo AES256` means use [AES]({{< ref "glossary.md#aes" >}})-256 for encryption
* Decrypt: `gpg -d ciphertext.gpg > clear.txt`
  * `-d` means decrypt

Please note that the key used for encryption/decryption is temporarily cached by your device. When you are running gpg 2.2.7 or newer, you can turn off caching by adding `--no-symkey-cache`.

## imagemagick
Well-known tools use imagemagick, so it is likely that imagemagick is already installed on your machine. You can use it to remove metadata from photos:

* Remove metadata: `mogrify -strip [filename]`
  * `-strip` means "strip the image of any profiles, comments or these PNG chunks: bKGD, cHRM, EXIF, gAMA, iCCP, iTXt, sRGB, tEXt, zCCP, zTXt and date"
* View metadata: `identify -format '%[EXIF:*]' [filename]`
  * shows Exif metadata in the file

## openssl
You can use openssl for many purposes. For example, whenever you need pseudo-random bytes:

* Print bytes to terminal: `openssl rand [number-of-bytes]`
* Hex format: `openssl rand -hex [number-of-bytes]`

Then, there is OpenSSL's SSL/TLS client program:

* `openssl s_client -connect infosec-handbook.eu:443` connects to the domain. The output contains information about certificates, TLS parameters, and TLS session tickets.

## pwgen
Do you need a password now? Use pwgen:

* Create passwords containing upper-case and lower-case chars, digits and special chars: `pwgen -scyn1 [number-of-characters] [number-of-passwords]`
* Create passwords containing upper-case and lower-case chars and digits: `pwgen -scn1 [number-of-characters] [number-of-passwords]`

## qrencode
qrencode can be used to transform arbitrary strings into QR codes:

* `qrencode -o [qr-filename].png "[string]"`
* Change the pixel size: `qrencode -o [qr-filename].png -s [pixel-size] "[string]"`

## subnetcalc
subnetcalc is a CLI-based calculator for subnets of IPv4 and IPv6 networks.

* `subnetcalc 192.168.1.1/24` prints network, netmask, broadcast address, max. hosts, properties, and more.
* `subnetcalc infosec-handbook.eu` prints IP addresses, properties, geo information about the IP address, and more.

## ykman
The YubiKey Manager can be used to manage most features of a YubiKey. Useful commands are:

* `ykman oath code`: Generate [OATH-TOTP]({{< ref "/glossary.md#oath-totp" >}}) codes in your terminal. (Actually, "ykman oath" can replace the Yubico Authenticator.)
* `ykman config set-lock-code`: Configure a 16 bytes long lock code to protect your YubiKey's configuration.
* `ykman config [nfc/usb]`: Enable/disable features of your YubiKey.

## zbarimg + oathtool {#zbarimg-oathtool}
Do you want to use [two-factor authentication]({{< ref "glossary.md#2fa" >}}) in the terminal? You can use [OATH-TOTP]({{< ref "glossary.md#oath-totp" >}}) with zbarimg + oathtool:

1. Enable 2FA on the website. Normally, you will see a QR code. Save this QR code.
2. Use `zbarimg [file-containing-qr-code]` to show the string representation of the QR code. This looks like `QR-Code:otpauth://totp/[blabla]?secret=T2LAELPYIS2NGNYE&issuer=[website-owner]&algorithm=SHA1&digits=6&period=30`.
  * secret=T2LAELPYIS2NGNYE is the important part here!
  * algorithm=SHA1 is the used [hash function]({{< ref "glossary.md#hash-function" >}})
  * digits=6 means that the [OTP]({{< ref "glossary.md#otp" >}}) is 6 digits long
  * period=30 means that the OTP changes every 30 seconds
3. Use `oathtool --base32 --totp "T2LAELPYIS2NGNYE"` to get your OTP each time.
4. The output is like `608166`.

**WARNING**: The secret (e.g., T2LAELPYIS2NGNYE) is a secret! Store it like a password and only use a second device to generate your OTPs. Do not use the device which you use for login! Do not store this secret and the normal password in the same database!

## zmap
zmap is a fast network scanning tool. It is very useful for large-scale (internet-wide) scans. One example command is `zmap -p [port] -o [output-filename].csv [network]`. You can add `--dryrun` to test your command without actually running it.
