+++
title = "Home network security – Part 1: Hello World"
author = "Benjamin"
date = "2018-05-11T11:52:30+02:00"
lastmod = "2019-07-28"
tags = [ "turris-omnia", "router", "lan", "wlan", "wpa2" ]
categories = [ "Home network security" ]
ogdescription = "We talk about several network basics, install our Turris Omnia and show the basic configuration."
slug = "hns1-hello-world"
banner = "banners/as-hns"
notice = true
+++

In this series, we show ways to improve the security of your computer network at home. In the first part, we introduce the Czech open-source router Turris Omnia. Besides, we talk about network basics, install our Turris Omnia and improve its basic configuration.
<!--more-->
## Contents
1. [Network basics]({{< relref "#network-basics" >}})
  * [Router]({{< relref "#router" >}})
  * [LAN]({{< relref "#lan" >}})
  * [WLAN]({{< relref "#wlan" >}})
  * [Basic network security]({{< relref "#bns" >}})
2. [Unpacking and installing the Turris Omnia]({{< relref "#install-to" >}})
3. [Basic configuration of the Turris Omnia]({{< relref "#config-to" >}})
  * [Improve WLAN security]({{< relref "#to-wlan" >}})
  * [Understand the purpose of the guest network]({{< relref "#to-guest-nw" >}})
4. [Summary]({{< relref "#summary" >}})
5. [Sources]({{< relref "#sources" >}})
6. [Changelog]({{< relref "#changelog" >}})

{{< rssbox >}}

## Network basics
First of all, we want to talk about some network basics. You can skip this section if you know things like IP addresses, MAC addresses, or routers. While this isn't a full guide for all aspects of networks, we want to talk about the most important things. There are [very good resources]({{< ref "/recommendations.md#hn" >}}) that explain network basics in detail.

A computer network consists of two or more devices that are connected. The physical or logical connection of two or more devices allows the devices to communicate using _network protocols_. One very common network protocol used by IT devices is HTTPS (Hypertext Transfer Protocol Secure). You very likely used HTTPS to navigate to this blog article. However, there are not only protocols that are used for communicating with the internet. Some network protocols are only used in a local area network. In this series, we mostly refer to your local area network at home as "home network".

In most cases, your home network uses a single _router_ to allow devices to connect to the internet. Some of your devices use a wireless connection, others use a wired connection. Your router is typically connected with your _internet service provider_ (ISP). Your home network may look like the network in the following picture:

{{< webpimg "/art-img/as-hns1-basicnetwork.png" "Basic network configuration (star topology). Dashed lines mean wireless connections, the dotted line to the internet is the connection to your ISP and solid lines are wired connections." >}}

Every device in a network needs an _IP address_ (IP means Internet Protocol). In your home network, your devices commonly get dynamically-assigned IP addresses from your router. These IP addresses are only internal IP addresses. Your router gets its public IP address from your ISP. As soon as you use a device on your home network to communicate with the internet, your router relays so-called _packages_ from your device to the internet and vice versa.

Besides, every device has a unique _MAC address_ (MAC means media access control). This is the physical address of a device while the IP address is its logical address. The MAC address is also needed for communication but only within a local network. Since you normally never change the MAC address (it is preset by the manufacturer of the device), we don't go into detail here.

Let's have a closer look at different network components:

### Router
The router is a very versatile device within your network and provides several services like internal distribution of IP addresses, firewalls or VPN (virtual private network). This also means that your router is a single point of failure in terms of availability of your internet connection and your network security.

Do you use a router provided by your ISP or did you buy your own router? Usually, routers provided by ISPs are limited in their functionality and sometimes there are also managed by ISPs. Buying your own (additional) router allows customization and gives you more control.

If you manage your own router, do the following:

* Regularly update the firmware of your router. Keep in mind that (very) cheap routers may never get any security updates.
* Change default passwords of your router, especially admin/root passwords.
* Disable functionality that you don't need (like UPnP).
* Check if you can restrict access to the router to LAN-only.
* Check if your router supports HTTPS-only and/or SSH for connections to it.

Instead of using the router provided by your ISP, you can also buy dedicated hardware and install one of many different open-source operating systems for routers. Well-known Linux-based distributions are OpenWrt and IPFire. Other distributions are pfSense (based on FreeBSD), and OPNsense (based on HardenedBSD/FreeBSD).

However, building your own router from scratch costs time and money and you need to know a lot about networks and Linux. Due to this, we use the [Turris Omnia]({{< relref "#install-to" >}}) in this series.

### LAN
In this series, we always mean "any devices connected to your router at home using cables" when talking about a LAN (local area network). If you live in your own house or apartment, it is normally easy to control wired connections since attackers can't directly connect to your network by wire without physically breaking in.

When it comes to choosing Ethernet cables for your home network, use Cat 5e or Cat 6 cables. There are different variants of these cables. The simplest form is a "U/UTP" cable. This means there neiter is overall shielding nor individual shielding. The most complex form is a "SF/FTP" cable. This means there is overall braiding and foil (so there is an overall shielding), and there is individual foil. If you only lay some cables, you can use "U/UTP" or "U/FTP". However, if you lay dozens of cables closely to each other, use "S/UTP", "F/FTP", or "SF/FTP". This is about electromagnetic interference, not about security, though.

For physical security, several companies sell locks for physical network ports. The aim of these locks is to prevent attackers from connecting their devices with the locked network ports. Keep in mind that attackers can easily remove already-attached network cables and use these unlocked ports.

### WLAN
Your WLAN (we mean local wireless connections here) can be easily attacked and it's hard to control! The reason for this is that people always want a robust wireless connection. This means that most routers provide high transmission power by default even when it is unnecessary. High transmission power likely means that you (and others) can be dozens of meters away and are still able to connect to your WLAN.

The most important points to do here are:

* Disable weak/legacy encryption: WEP, WPA, and WPA2-PSK-TKIP. Especially, WPA2-PSK-TKIP is oftentimes enabled by default. TKIP (Temporal Key Integrity Protocol) uses insecure RC4 for encryption! It was an interim solution to replace WEP and was deprecated more than five years ago.
* Enable strong encryption only: **WPA2-PSK-CCMP**. CCMP (Counter-Mode/CBC-MAC Protocol) is sometimes called "WPA2-Personal" or "WPA2-[AES]({{< ref "/glossary.md#aes" >}})".
* Disable WPS (Wi-Fi Protected Setup). Sometimes, WPS is very weakly protected or implemented in an insecure way. If successfully exploited, attackers could connect to your network even if you enabled WPA2-PSK-CCMP only and set a strong password.
* Use a loooong password for your WLAN. You can actually set **up to 63 characters**. Some sources say you should at least set 30 or more characters. Usability tip: Convert the password into a QR code and use this QR code for configuring smartphones etc. It is far easier to scan a QR code than entering 63 characters manually.
* Check if you can reduce the transmission power of your router (attackers can use better antennas, though).
* If your router offers different standards, use "IEEE 802.11ac" (5 GHz). Keep in mind that some old devices might only support 2.4 GHz WLAN.

Maybe your router allows you to choose between WPA2-PSK and WPA2-EAP. The difference is quite clear:

* PSK (Pre-shared key) uses the same password for every device. The actual key used for encryption is derived from this PSK using [PBKDF2]({{< ref "/glossary.md#pbkdf" >}}) afterwards. PSK-based networks are mostly used by private individuals and therefore sometimes called "WPA2-Personal".
* EAP (Extensible Authentication Protocol) uses RADIUS servers for [authentication]({{< ref "/glossary.md#authentication" >}}). Network administrators can add and remove single devices without changing the rest of the network. EAP also provides different methods for authentication. EAP is mostly used by companies and therefore sometimes called "WPA2-Enterprise".

{{% notice info %}}
We will update this section as soon as IEEE 802.11ax / WPA3 devices are widely available for private users.
{{% /notice %}}

### Basic network security {#bns}
When it comes to network security, you not only have to secure your router but every device in your network. For instance, some private users connect their IP camera with their home network. Then, they configure their router to allow access to their camera via the internet. Of course, a remote attacker can attack the weakly-protected IP camera to access the whole internal network. Basic documentation is a key element:

* Which devices are connected to your router? (name, IP address, MAC address, owner)
* Which services are provided by these devices? (e.g., a local XMPP server)
* Do these devices need internet connection?
* Do these devices need access to other local devices?
* Did you change your router's configuration for some reason?

You should regularly revise this document. Another important aspect is monitoring your home network. This will be discussed in an upcoming part of this series.

## Unpacking and installing the Turris Omnia {#install-to}
Now, let's have a look at the Turris Omnia. It is a crowdfunded router and manufactured by CZ.NIC, a Czech interest association. The Omnia's hardware is powerful enough to be used as a standalone home server. In January 2018, we bought the most expensive version, which provides 2GB DDR3 RAM and WLAN (RTROM01-2G), for about €260 in the Czech Republic.

The package includes the Turris Omnia itself with 3 antennas, 1 network cable (Cat 5e U/UTP), several power adapters, instruction manual and wall mounting bracket:

{{< img "/art-img/as-hns1-turris-omnia-content.jpg" "Turris Omnia and its accessories." >}}

Installing the Omnia is very straightforward: Screw the antennas onto the Omnia, connect its WAN port with a free network port of your current router, and plug it into the wall socket.

## Basic configuration of the Turris Omnia {#config-to}
You must connect a computer with the Omnia to open its router administration interface. Its software is based on OpenWrt, but the configuration is really simple. You have to complete several steps and you are done.

{{% notice note %}}
Please note that the following guide is based on Turris OS 3.10. Other versions of Turris OS may come with other features or configuration.
{{% /notice %}}

Your first steps are:

* Change the password of the admin account. One password is for "normal" access (Foris OS). The second password is for advanced administration (including LuCI and SSH).
* Configure WAN/DHCP, if necessary.
* Conduct a connectivity test.
* Set region for time synchronization.
* Enable automatic updates.
* Configure LAN settings (DHCP and router IP address), if necessary.
* Configure WLAN settings (passwords, SSIDs etc.). We enabled the 5 GHz network and its guest network only.
* Reboot your Omnia.

Mostly, we didn't change the defaults.

{{< mastodonbox >}}

### Improve WLAN security {#to-wlan}
We noticed that our Omnia allowed **insecure and deprecated TKIP** for WLAN. You can turn TKIP off using LuCI (the advanced management interface):

Open your web browser, enter the IP address of your Omnia (likely `192.168.1.1`), and click "LuCI - OpenWRT advanced web configuration". Log in using the password for advanced administration.

* Click on "Network" → "Wireless"
* Select your WLAN and click on "Edit"
* Select "Interface Configuration" → "Wireless Security"
* Set "Cipher" to "Force CCMP (AES)"
* Click on "Save & Apply"

You can also reduce the transmission power there.

### Understand the purpose of the guest network {#to-guest-nw}
The guest network is a separate VLAN. VLAN means _virtual LAN_. Instead of being physically separated, a VLAN is logically separated. So you actually have two different networks by default:

* VLAN 1 (guest network) uses the IPv4 address range `10.111.222.1/24`.
* VLAN 2 ("normal" network) uses the IPv4 address range `192.168.1.1/24`.

Devices connected to the Omnia's guest network (VLAN 1) are only allowed to communicate with the internet. They can't access your router or devices which are connected via VLAN 2. This setup allows untrustworthy devices in VLAN 1 to use the internet while they can't access your trusted devices in VLAN 2.

If you know how to configure networks (esp. VLANs), you can change the default configuration, of course. You could also use the 172.16.0.0/12 address range, or define more VLANs. You find their settings in LuCI (go to "Network" → "Interfaces").

At this stage, your home network looks like this:

{{< webpimg "/art-img/as-hns1-turris-network.png" "Network with Turris Omnia (192.168.1.1) and your former router (192.168.0.1). Dashed lines mean wireless connections, the dotted line to the internet is the connection to your ISP and solid lines are wired connections. VLAN 1 devices can only connect with the internet, VLAN 2 devices can additionally connect with your router and with each other." >}}

{{< hnsbox >}}

## Summary
Installing and configuring the Turris Omnia is very easy and you don't have to learn complex Linux commands or spend hours to get it to work. Its defaults allow fast integration in your home network and offer a good level of security.

We didn't encounter any problems during 1.5 years of operation. The Omnia automatically updated its software several times and you can even enable e-mail notifications for updates.

[The next part of this series]({{< ref "/blog/hns2-tls-hardening.md" >}}) is about improving the security of your HTTP connection to the router's administration interface.

## Sources
* {{< extlink "https://omnia.turris.cz/en/" "Turris Omnia" >}}
* {{< extlink "https://www.pfsense.org/" "pfSense" >}}
* {{< extlink "https://opnsense.org/" "OPNsense" >}}
* {{< extlink "https://www.openwrt.org/" "OpenWrt" >}}
* {{< extlink "https://www.ipfire.org/" "IPFire" >}}

## Changelog
* Jul 28, 2019: Removed redundant section. Added are more detailed explanation of networks. Added information about Ethernet cables.
