+++
title = "YubiKey 4C vs. Nitrokey Pro: Stalemate"
author = "Benjamin"
date = "2018-04-11T19:27:36+02:00"
lastmod = "2018-11-03"
tags = [ "2fa", "e-mail", "gnupg", "password", "yubikey", "nitrokey" ]
categories = [ "authentication" ]
ogdescription = "YubiKey and Nitrokey can both bolster up your security but there are some differences."
slug = "yubikey4c-nitrokeypro"
banner = "banners/yubikey4c-nitrokeypro"
+++

When it comes to [authentication]({{< ref "/glossary.md#authentication" >}}), most private individuals still rely on (weak) passwords or sometimes (insecure) [biometrics]({{< ref "/glossary.md#biometrics" >}}). Mostly, they only use a single factor for authentication: something they know, something they have or something they are. Attackers can easily access your accounts if they overcome this single hurdle.

Nowadays, you should use [two-factor authentication]({{< ref "/glossary.md#2fa" >}}) (2FA) wherever possible. There are several 2FA apps available, however, this article focuses on two quite similar hardware products which can be used as a second factor: YubiKey 4C and Nitrokey Pro. We also compare other security features of these products and talk about their successors.
<!--more-->
## Contents
1. [Additional hardware is more secure than apps]({{< relref "#usb-vs-app" >}})
2. [Your regular USB flash drive isn't as secure as a YubiKey or Nitrokey]({{< relref "#regular-vs-secure" >}})
3. [YubiKey and Nitrokey in comparison]({{< relref "#details" >}})
  * [YubiKey 4C]({{< relref "#yubikey-4c" >}})
  * [Nitrokey Pro]({{< relref "#nitrokey-pro" >}})
4. [Benefits]({{< relref "#benefits" >}})
5. [Problems]({{< relref "#problems" >}})
6. [Conclusions]({{< relref "#conclusions" >}})
7. [Upgrade to the next generation?]({{< relref "#ng" >}})
8. [Sources]({{< relref "#sources" >}})
9. [Changelog]({{< relref "#changelog" >}})

{{< rssbox >}}

## Additional hardware is more secure than apps {#usb-vs-app}
You could ask yourself why you should buy and use additional hardware when you could simply download a 2FA app from your favorite app store. There are dozens of 2FA apps available and it is very likely that you have your phone ready when you want to authenticate yourself online.

However, you have to consider that many phones run outdated operating systems with unpatched [vulnerabilities]({{< ref "/glossary.md#vulnerability" >}}) (esp. older Android phones). Even if you have an up-to-date LineageOS installed, it is likely that there are hardware components in your phone which have unpatched vulnerabilities in their firmware. There are also apps on your phone which can be outdated and introduce additional vulnerabilities.

Furthermore, most phones are always connected to mobile networks and the internet. People use their phones for online shopping, browsing, messaging and so on. Needless to say that the risk of being compromised is higher in comparison with hardware which is only used for certain purposes.

Additional benefits of YubiKeys or Nitrokeys are that you can securely store [GnuPG]({{< ref "/glossary.md#gnupg" >}}) keys, passwords and data on the devices.

## Your regular USB flash drive isn't as secure as a YubiKey or Nitrokey {#regular-vs-secure}
Okay, you realized that additional single-purpose hardware is more secure than using your outdated cellphone which is always online. But you could just buy a regular USB flash drive and use it for this purpose, couldn't you?

The most important difference is that YubiKeys and Nitrokeys aren't just normal USB flash drives. Both are implementations of the OpenPGP card standard. This means that cryptographic tasks like encryption, decryption, signing and authentication are performed on the card. Securely stored GnuPG keys never leave these cards, making it impossible for attackers to copy private keys. Normal USB flash drives don't offer this protection. There is even more than just GnuPG: Depending on your security token, technologies like generation of [time-based one time passwords]({{< ref "/glossary.md#oath-totp" >}}), [Universal 2nd Factor]({{< ref "/glossary.md#u2f" >}}) and [FIDO2]({{< ref "/glossary.md#fido2" >}}) may be supported.

## YubiKey and Nitrokey in comparison {#details}
YubiKeys as well as Nitrokeys are supported by most common operating systems. Both security tokens are almost equally expensive. They allow you to securely store your GnuPG keys and can be used as a second factor.

While all YubiKeys support [U2F (Universal 2nd Factor)]({{< ref "/glossary.md#u2f" >}}), only the [Nitrokey FIDO U2F]({{< ref "/blog/yubico-security-key-nitrokey-u2f.md#nitrokey-fido-u2f" >}}) supports this authentication standard. This means that you need to buy a Nitrokey Pro and a Nitrokey FIDO U2F to use U2F and TOTP/GnuPG.

### YubiKey 4C
YubiKeys are produced by Yubico which was founded in 2007 and is based in the USA and Sweden.

Noticeable features are:

* support for statical passwords (max. 64 characters)
* Yubico [OTP]({{< ref "/glossary.md#otp" >}}) (via YubicoCloud)
* [OATH-HOTP]({{< ref "/glossary.md#oath-hotp" >}})
* up to 28 [OATH-TOTP]({{< ref "/glossary.md#oath-totp" >}}) accounts (using the Yubico Authenticator or Yubikey Manager)
* generation and storage of [GnuPG]({{< ref "/glossary.md#gnupg" >}}) keys (up to 4096 bits RSA or 384 bits ECC)
* unlimited [U2F]({{< ref "/glossary.md#u2f" >}}) accounts
* [challenge-response authentication]({{< ref "/glossary.md#challenge-response-authentication" >}})
* NFC support (YubiKey 4 NEO only)

Please note that YubiKeys only contain two configurable slots. The first slot is configured for Yubico OTP at delivery. This means that you have effectively one slot which can be used for challenge-response authentication etc. You can also configure that you must press the physical button of the YubiKey to authorize certain actions.

The official [Yubikey Manager]({{< ref "/terminal-tips.md#ykman" >}}) can be used to enable or disable certain features of a YubiKey or limit features to certain interfaces (USB/NFC).

While YubiKeys were partially open source in the past, Yubico decided to migrate to closed source. For security reasons, firmware of YubiKeys can't be modified (including firmware updates). Most applications (software) are still open source, though.

### Nitrokey Pro
Nitrokeys are produced by Nitrokey UG which was founded in 2015 and is based in Germany. The people behind Nitrokey actually developed a predecessor, called Crypto Stick (2008–2010).

Noticeable features are:

* up to 3 [OATH-HOTP]({{< ref "/glossary.md#oath-hotp" >}}) accounts
* up to 16 [OATH-TOTP]({{< ref "/glossary.md#oath-totp" >}}) accounts
* generation and storage of [GnuPG]({{< ref "/glossary.md#gnupg" >}}) keys (up to 4096 bits RSA) or S/MIME
* password storage for up to 16 accounts (using the Nitrokey App)

We didn't test S/MIME since private users use GnuPG if at all. Furthermore, Nitrokey officially states that problems occur when you want to use GnuPG and S/MIME simultaneously.

The password storage offers only 16 slots. You can label each entry (max. 11 characters), store a username for each entry (max. 32 characters) and passwords can consist of up to 20 characters. Please note that some special characters are counted twice or more. For instance, the German characters ü or ß are counted twice. This means that some special characters effectively reduce the strength of passwords by forcing you to use less characters. Moreover, the 20 character limit makes it impossible to store long passphrases.

Nitrokeys consist of open hardware and open source software. If you want to spend more money, there are also three different models of Nitrokey Storage available. They offer secure storage (16 GB, 32 GB or 64 GB) and upgradeable firmware in addition to the features of Nitrokey Pro. We didn't test those models, though.

## Benefits
Both security tokens offer the following benefits:

* It is easier to use GnuPG because you only have to enter a short PIN instead of a long password when you want to use the stored key. A PIN is sufficient since you only authorize the cryptographic operation while passwords are used for encryption and decryption of your keys when they are stored on your hard drive.
* OATH-TOTP secrets are securely stored on your security token.
* You can use your keys and access your passwords on the go. Especially your GnuPG keys are protected, so malicious devices can't copy them.
* Both tokens are tamper-resistant. This means that you can't manipulate the hardware without destroying it.
* There are many websites and other use cases allowing you to use both tokens as second factors.
* Both tokens aren't connected to the internet and only serve certain purposes.

## Problems
Of course, there are some problems. Some of them are typical for certain implementations/standards:

* In case of loss or destruction, you lose your stored keys and passwords. This means that you need backups and backups must be at least as secure as the primary device. We think that a second Nitrokey/YubiKey is inevitable. Moreover, attackers can access several features of your Nitrokey/YubiKey without authentication when they have physical access to them. Keep this in mind and don't lose your Nitrokey/YubiKey!
* OATH-HOTP/OATH-TOTP is useless if an attacker knows the secret used to generate the one-time password. Actually, this isn't a weakness of the implementations but of the algorithms. Configure OATH-HOTP/OATH-TOTP only on trustworthy devices!
* OATH-TOTP requires a time source. However, synchronized clocks in security tokens require an energy source. Therefore, you must install additional software to use TOTP since neither YubiKeys nor Nitrokeys contain batteries.
* Sometimes, U2F secrets are generated by the manufacturer of the security token, then stored on the device and can't be replaced afterwards. In theory, manufacturers could copy your secret U2F key.
* As mentioned above, YubiKeys don't allow firmware updates due to security reasons. In the past, it was necessary to replace YubiKeys with new ones after vulnerabilities were discovered (CVE-2015-3298, CVE-2017-15361). The replacement was for free.
* The Windows 10 update of September 2018 rendered the Nitrokey App useless. Due to this, Windows 10 users weren't able to access stored passwords and TOTP codes for about 10 months. As of October 2019, the Nitrokey App 1.4 (released in June 2019) runs again on Windows 10 1908.

{{< mastodonbox >}}

## Conclusions
At the end of the day it is not easy to decide if either YubiKeys or Nitrokeys are better. The main difference is that the hardware/firmware of YubiKeys is closed source while Nitrokeys are based on open hardware. Both producers argue convincingly that their philosophy is better. So, we decided to list feature-based recommendations:

If you primarily want/need:

* a GnuPG storage: It doesn't matter. Buy a Nitrokey or YubiKey.
* open hardware: Buy a Nitrokey.
* password storage: Buy a Nitrokey (and keep in mind that there are only 16 slots available)
* OATH-TOTP: It doesn't matter as long as you don't want to store more than 16 accounts. Buy a Nitrokey (for up to 16 accounts) or YubiKey (for up to 28 accounts).
* U2F: Buy a YubiKey or Nitrokey (and keep in mind that you need two Nitrokeys to use U2F and GnuPG)
* FIDO2: Buy a YubiKey.
* NFC support: Buy a YubiKey.

## Upgrade to the next generation? {#ng}
In late summer of 2018, Nitrokey UG and Yubico announced their newest generation of security tokens. If you already own a YubiKey 4 or a Nitrokey Pro, you might ask yourself whether you should invest money for an upgrade. Our answer is: No. The upgrades aren't that significant.

* The Nitrokey Pro 2 now supports ECC-based GnuPG keys. ECC for GnuPG isn't widely adopted and legacy applications don't support ECC at all. That's the main difference compared with its predecessor, the Nitrokey Pro. [Curve25519]({{< ref "/glossary.md#curve25519" >}}) isn't supported.
* The YubiKey 5 series now supports [FIDO2]({{< ref "/glossary.md#fido2" >}}) and the upcoming [WebAuthn]({{< ref "/glossary.md#webauthn" >}}) standard. Both technologies aren't widely adopted and U2F may be sufficient for the next years. Furthermore, the cheapest model (YubiKey 5 NFC) now supports NFC. The remaining three models differ in size (normal vs. micro) or interface (USB-A vs. USB-C). In summary, there is no reason for an upgrade.

## Sources
* {{< extlink "https://www.yubico.com/" "Yubico" >}}
* {{< extlink "https://www.yubico.com/2016/05/secure-hardware-vs-open-source/" "Yubico – Secure Hardware vs. Open Source" >}}
* {{< extlink "https://www.nitrokey.com/" "Nitrokey" >}}
* {{< extlink "https://github.com/Nitrokey/wiki/wiki/Ideas" "Nitrokey – Development ideas" >}}
* {{< extlink "https://twofactorauth.org/" "Information about favorite websites and their 2FA support" >}}
* {{< extlink "https://www.dongleauth.info/" "List of websites and their OTP/U2F support" >}}

## Changelog
* Nov 3, 2018: Updated information about Nitrokey FIDO U2F.
* Oct 31, 2018: Reordered information and added information about U2F, FIDO2. Added information about the fifth generation of YubiKeys and the second generation of Nitrokeys. Included feedback of the CEO of Nitrokey. Added information about U2F problems, TOTP's time source and Windows 10 patch (0918). Clarified conclusions.
