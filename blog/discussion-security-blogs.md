+++
title = "How some 'security' blogs and websites actually cause insecurity"
author = "Benjamin"
date = "2018-12-01T10:45:00+01:00"
tags = [ "privacy", "blogging" ]
categories = [ "discussion" ]
ogdescription = "Many blogs and websites tell you something about 'security' but sometimes it's only about making money."
slug = "discussion-security-blogs"
banner = "banners/discussion-security-blogs"
+++

In 2013, Edward Snowden revealed global spy activities of the National Security Agency. In the years that followed, dozens of blogs and websites popped up to tell you about 'security' and how to defend against the evil US-American companies that passed your [personal data]({{< ref "/glossary.md#personal-data" >}}) to government agencies.

In practice, some of these blogs and websites only exists to spread FUD (fear, uncertainty and doubt) to eventually get your money. We discuss several indicators for this phenomenon and its influence on your security in this article.
<!--more-->
## Contents
1. [Our Top 5 indicators for shady blogs and websites]({{< relref "#top5" >}})
  * [Indicator 1: 'Technology is everything in information security']({{< relref "#i1" >}})
  * [Indicator 2: Articles are biased]({{< relref "#i2" >}})
  * [Indicator 3: There are contradictory recommendations]({{< relref "#i3" >}})
  * [Indicator 4: Articles spread typical myths]({{< relref "#i4" >}})
  * [Indicator 5: Authors have no background in InfoSec]({{< relref "#i5" >}})
2. [Summary]({{< relref "#summary" >}})

{{< rssbox >}}

## Our Top 5 indicators for shady blogs and websites {#top5}
Consider the following five indicators if you read articles or search for new blogs worth reading. There are many more indicators, of course, and one could add many other aspects to the list.

### Indicator 1: 'Technology is everything in information security' {#i1}
We often see blogs and websites that are completely focused on technology when it comes to information security. Of course, this is okay as long as readers see that the focus of an InfoSec blog or website is technology. A popular example is Matthew Green's blog "A Few Thoughts on Cryptographic Engineering". His blog is clearly focused on cryptography and we likely never look for information security awareness on his blog.

However, there are blogs which tell you that information security is only about technology. According to these blogs and websites, InfoSec means algorithms, cryptography, configuration, network protocols and so on. The authors never talk about people, processes, organization, risk management, security controls, incident response, disaster recovery etc. They simply ignore other major aspects of information security or actively tell their readers that these aspects are useless.

In our opinion, this leads to an incomplete impression of information security and readers likely focus upon technology. For instance, [social engineering]({{< ref "/glossary.md#social-engineering" >}}), risk management and processes are also very relevant to private individuals.

### Indicator 2: Articles are biased {#i2}
Another indicator we often see are biased articles. Biased articles either only contain lots of benefits of software/certain technology or lots of drawbacks. You won't find balanced assessments.

Our favorite example are biased articles about instant messengers: Some people try to present their favorite messenger as the ultimate solution to all problems while they traduce all other messaging solutions. However, there is no messenger that fits all needs of everybody and there is also [no secure instant messenger]({{< ref "/blog/discussion-secure.md#sm" >}}).

In our opinion, biased articles seem to be all about presenting the author's ideological beliefs as the right ones. In reality, these articles result in myths and insecurity since many readers rely on the expertise of the author. Subliminally presenting ideological beliefs or deliberately leaving out major drawbacks isn't proficient at all.

### Indicator 3: There are contradictory recommendations {#i3}
There are many reasons for contradictory recommendations on the internet. For example, a technical statement like "a 6-digit password is secure" was true several years ago but isn't true in 2019. Another reason for contradictory recommendations are different legal/technical requirements in different countries. There are some global standards like ISO/IEC 27001 or IEC 62443 but there are no global information security regulations or privacy laws.

Let's have a look at the Signal Messenger: While prism-break.org doesn't recommend Signal, privacytools.io tells its readers that this messenger is one of the best messengers when it comes to privacy. Then some people tell you that you can't use Signal without providing your phone number and all of your contacts will be uploaded to Signal servers ([both statements are myths]({{< ref "/blog/myths-signal.md" >}})). Other famous people from the InfoSec world tell you that Signal's security measures are exemplary. This short example clearly shows contradictory recommendations.

However, some blogs and websites tell you that certain software or approaches are bad/insecure only to recommend this software/these approaches in other articles written by the same author. Additionally, some authors recommend configuration or implementations that aren't based on any facts but paranoia or ideological beliefs (again).

This results in the same problems mentioned above: Readers wonder what to do next since different blogs/websites recommend different things.

### Indicator 4: Articles spread typical myths {#i4}
All indicators mentioned in this article lead to myths. Some readers repeat wrong stories they read or heard somewhere. We debunked some of the most common myths in several articles, e.g., "[private individuals can sue their friends for discompliance with the GDPR]({{< ref "/blog/myths-gdpr.md#m3" >}})", "[open-source software is more secure than proprietary software]({{< ref "/blog/myths-software-security.md#m1" >}})" or "[scanning servers externally discovers all security and privacy issues]({{< ref "/blog/myths-web-security.md#m1" >}})". Another popular myth is the "privacy-friendly and secure world of XMPP" while [in reality server admins (and server-side attackers) can log and modify nearly everything]({{< ref "/blog/xmpp-aitm.md" >}}), including cleartext passwords, contacts, reading statuses and device activity.

In our opinion, myths could be avoided if authors of InfoSec articles would thoroughly research into their topics. Unfortunately, some authors only repeat myths written somewhere else without checking whether these myths are true.

### Indicator 5: Authors have no background in InfoSec {#i5}
The last indicator are authors with no background in InfoSec. Does this mean that any author must be a Hollywood hacker? No.

In reality, there are many different domains of information security. You mustn't hold a degree in Computer Science to work for an InfoSec company. Common jobs are incident responder, security auditor, security awareness officer, penetration tester, chief information security officer, security code auditor, security analyst, forensics expert, security architect, security consultant, vulnerability assessor and many more. These jobs require different skills, different educational background and have different focuses in the InfoSec world. Besides, there is the privacy/data protection world that consists of many legal and contractual aspects in most countries and differs from information security.

There are also authors or people with no InfoSec background at all. This is fine as long as they provide fact-based information. However, some authors create their own facts and continue to spread myths. This leads to the already above-mentioned problems.

{{< mastodonbox >}}

## Summary
Nowadays, information security affects everybody but some people try to milk this topic to make money in a post-Snowden world. Articles based on ideological beliefs and hearsay quickly result in myths and insecurity.

We conclude this article with several tips:

* Keep in mind that information security is much more than only technology—read also about information security awareness, risk management and processes/organization.
* Keep in mind that there are no global privacy laws or InfoSec regulations—depending on the home country of the author, tips and recommendations differ.
* Check the professional background of an author, if possible.
* Check whether the author only promotes his own articles and tries to create a filter bubble to make money.
* Challenge blanket statements and check different blogs/websites for more information—an alleged fact can quickly turn into another myth.
* Search for lists with well-known blogs/websites about information security.
* Help debunking myths by carefully researching topics before sharing biased articles.
* Clearly define your goals when talking about information security—you can't implement everything and there is no state of 100% security.
