+++
title = "Web server security – Part 7: Policies, and security contact"
author = "Benjamin"
date = "2019-02-23T13:11:00+01:00"
tags = [ "blogging", "server-security", "gdpr", "gnupg", "privacy-policy", "policy" ]
categories = [ "Web server security" ]
ogdescription = "In this article, we present different policies for your website and modern security contact information."
slug = "wss7-policies-contact"
banner = "banners/as-wss"
notice = true
+++

In this part of the Web server security series, we present less technical but yet important parts of your web server: policies, and up-to-date security contact information.
<!--more-->
## Contents
1. [Basics]({{< relref "#basics" >}})
2. [Requirements]({{< relref "#requirements" >}})
3. [Write your policies]({{< relref "#write-your-policies" >}})
  * [Privacy policy]({{< relref "#privacy-policy" >}})
  * [Security policy]({{< relref "#security-policy" >}})
  * [Disclosure policy]({{< relref "#disclosure-policy" >}})
  * [Contact information]({{< relref "#contact-information" >}})
4. [Summary]({{< relref "#summary" >}})
5. [Links]({{< relref "#links" >}})

{{< rssbox >}}

## Basics
Many private users think of "privacy policies" in the context of websites. However, there are at least two additional, common policies: security policy, and disclosure policy. Then, there are less known policies to consider: terms and conditions, copyright and trademark information, disclaimers, accessibility information, and contact information for different purposes (e.g., abuse, complaints, privacy, support, security).

For simplicity, we will only talk about the most common policies, and contact information. Other policies like terms and conditions, or copyright and trademark information are legal topics that must be considered individually.

## Requirements
In the following, we discuss software-independent topics. However, we recommend to create a [GnuPG]({{< ref "/glossary.md#gnupg" >}}) key used to provide an [end-to-end encrypted]({{< ref "/glossary.md#end-to-end-encryption" >}}) communication channel for visitors of your website. We provide a [quickstart guide]({{< ref "/terminal-tips.md#gpg-gen" >}}) for people who don't know how to create GPG keys. Furthermore, you can use this GPG key to sign your security.txt file as shown below.

## Write your policies
Our standard legal warning first:

{{% notice warning %}}
Kindly note that the following article isn't legal advice. If you need legal advice, contact a lawyer. It is forbidden in many countries to legally advise people if the advisor isn't a lawyer. Subsequent GDPR-related topics describe the situation of InfoSec Handbook, and may not be applicable to your legal situation.
{{% /notice %}}

We recommend to provide at least a privacy policy. We can't tell you whether you need other policies.

### Privacy policy
Privacy/data protection are legal topics. While good practices for information security are almost identical around the globe, privacy/data protection differ from country to country. For instance, in 2016 the European Union introduced the General Data Protection Regulation (GDPR) that superseded the Data Protection Directive. One of the GDPR's goals is to unify data protection within the EU. However, EU member states had the possibility to modify several articles of the regulation. So, even within the EU, data protection differs. Another more global difference: In the US, people talk about "privacy" and "personally identifiable information" (PII); in the EU, it is "data protection" and "[personal data]({{< ref "/glossary.md#personal-data" >}})".

As you can see, writing a globally valid privacy policy is impossible. However, privacy policies should include information about:

* your contact details
* personal data processed by you
  * What do you process? (e.g., name, IP address, e-mail address, date of birth, username)
  * What is the legal basis for processing? (see Article 6 GDPR for examples)
  * What personal data do you disclose to third parties, and why (legal basis)?
  * How long do you store personal data?
* the rights of your readers/visitors according to the privacy law
* the scope of your policy (e.g., only valid for a specific URL)
* changes of your policy (e.g., include a changelog that lists significant changes of your policy)

It is almost always the case that you process the IP address of your visitors since IP addresses are necessary for internet communication. However, some websites never store IP addresses, while other websites store them for weeks.

Since writing a comprehensive privacy policy isn't a no-brainer, you should be as transparent as possible about anything you do with personal data belonging to your readers/visitors. Additionally, we recommend to read chapter 3 of the GDPR (articles 12–23) that contains more information about specific information you may need to include in your privacy policy. Moreover, keep in mind that there may be additional/differing privacy regulations in your country—even if it is an EU member state.

Finally, include links to your privacy policy that are easily accessible and findable. For instance, create a link in the `<footer>` section of your website that is shown on every page. Use a common URL like https://[your-domain-name]/privacy/ or https://[your-domain-name]/privacy-policy/.

### Security policy
In organizations, security policies define rules and procedures for employees and third parties that access/use resources and assets of the organization to ensure [confidentiality]({{< ref "/glossary.md#confidentiality" >}}), [integrity]({{< ref "/glossary.md#integrity" >}}), and [availability]({{< ref "/glossary.md#availability" >}}).

You could optionally provide a security policy to tell your visitors how you secure your systems, and what they must do to keep the level of security. We recommend to include:

* information about how people can quickly contact you in case of security incidents
* links to your privacy policy, and disclosure policy
* general information about your security concept

You may ask why you should publish something like this. The simple answer is that a good security policy creates trust and transparency. External parties see that you carefully consider information security, and server security. The policy isn't about including technical details like used ports, key strength, or version numbers.

Write simple non-technical statements how you secure your server. Avoid security through obscurity. Don't tell people that you secure your server by doing something when it's actually useless or wrong. For instance, don't tell your visitors that there is no JavaScript in use when it's actually present, or don't tell them that you embed certain HTTP headers that aren't actually widely supported by most web browsers. Keep it simple. Use a common URL like https://[your-domain-name]/security/ or https://[your-domain-name]/security-policy/.

Finally, we explicitly recommend to document detailed security information for your own purposes. Documentation is a really important part of web server security as already mentioned in [part 0 of this series]({{< ref "/blog/wss0-how-to-start.md" >}}).

### Disclosure policy
Another policy is the so-called "disclosure policy". This policy defines how people can report security vulnerabilities found in your server, web server, or products (like software). Furthermore, you should include information about:

* how you protect personal data of the reporting party
* how you would like to disclose the vulnerability to the public (e.g., "coordinated disclosure", "no disclosure")
* what you expect from the reporting party (e.g., "only publish details about the vulnerability after we patched it", "non-intrusive testing")
* that you don't take legal action if vulnerabilities are disclosed in accordance with your disclosure policy
* a public "hall of fame" where you acknowledge reporting parties
* bug bounties, if applicable

Of course, your private website that only presents family pictures likely doesn't need a disclosure policy. However, some private individuals host federated services like Mastodon instances for hundreds of people. In this case, a disclosure policy makes sense.

Bug bounties provide an incentive to comply with the disclosure policy. They don't need to be monetary. If you decide to offer bug bounties, we recommend to also include the following information:

* the scope of your bug bounties (e.g., only for certain products/URLs, only for certain types of vulnerabilities)
* if you want to offer different bug bounties for different types of vulnerabilities, clearly define these groups, e.g.
  * low severity (no bug bounty, public acknowledgement): information disclosure like version numbers of server-side software
  * medium security (€100, public acknowledgement): downgrade to HTTP
  * high severity (€500, public acknowledgement): leakage of personal data, injection attacks
* the process of receiving bug bounties (including time needed to validate reports)

Again, keep it simple. Use a common URL like https://[your-domain-name]/disclosure-policy/.

{{< mastodonbox >}}

### Contact information
Finally, you must clearly define communication channels. People may use them for different purposes: sending abuse reports or complaints, asking about privacy-related topics, requiring your support, or reporting security vulnerabilities.

If you only operate a small website or blog, offering dozens of communication channels doesn't make sense at all. You must be able to respond quickly in case of privacy or security topics. People should be able to confidentially communicate with you. They should also be able to verify your identity and the [authenticity]({{< ref "/glossary.md#authenticity" >}}) of your messages.

Two common communication channels for these purposes are e-mail addresses and contact forms. E-mails are unencrypted by default. Provide your public GPG key, so people can use it to encrypt e-mails for you, and verify e-mails that you sent. Contact forms allow you to ask for specific information, and people can send messages without disclosing their identity. This gives messages a structured format. However, contact forms require less strict server-side security configuration and can be misused for injection attacks. Moreover, they can be easily misused for spam. Hence, we recommend to not use contact forms since they represent a huge attack surface.

A proposed standard for security-related contact information is "A Method for Web Security Policies". There is a website about this draft: [security.txt – A proposed standard which allows websites to define security policies]({{< relref "#links" >}}). The idea is to create a "security.txt" file similar to the common "robots.txt" file on web servers. The file should be located in the well-known folder as described in RFC 5785: https://[your-domain-name]/.well-known/security.txt. There should also be a redirect from https://[your-domain-name]/security.txt to https://[your-domain-name]/.well-known/security.txt.

Since the content of the security.txt file changes as long as it isn't a final standard, you must monitor any changes to it. As of version 05, the draft proposes the following structure:

{{< highlight bash "linenos=table,linenostart=1" >}}
# Our security address
Contact: mailto:security@example.com

# Our OpenPGP key
Encryption: https://example.com/pgp-key.txt

# Our security policy
Policy: https://example.com/security-policy.html

# Our security acknowledgments page
Acknowledgments: https://example.com/hall-of-fame.html
{{< /highlight >}}

The standard recommends to [digitally sign]({{< ref "/glossary.md#digital-signature" >}}) your security.txt using an OpenPGP cleartext signature. You can use gpg to achieve this: `gpg --default-key [your-key-id] --clearsign security.txt`. The result looks like [our current security.txt file](https://infosec-handbook.eu/.well-known/security.txt):

{{< highlight bash "linenos=table,linenostart=1" >}}
-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

# Canonical URL
Canonical: https://infosec-handbook.eu/.well-known/security.txt

# Our security address
Contact: https://infosec-handbook.eu/contact/

# Our OpenPGP key
Encryption: https://infosec-handbook.eu/gpg.asc

# Preferred languages
Preferred-Languages: en, de, cs
-----BEGIN PGP SIGNATURE-----

iHUEARYKAB0WIQRHJvSOVge4pgbK9PkJ00z6wQJhfQUCXDzbQAAKCRAJ00z6wQJh
fVoOAP9H/dU+HixR5InG5DVd3YMb5hMj/cJuWTbCIuqEU3rrRAEAi4RXXzR2HxXL
zhEbiJAkX3rI+PTrUNroHCr0aFI0egE=
=f6gR
-----END PGP SIGNATURE-----
{{< /highlight >}}

Read the draft to learn about valid field formats. For instance, you can include the GPG key's [fingerprint]({{< ref "/glossary.md#fingerprint" >}}), or link to your disclosure/security policy.

We recommend to use a common URL like https://[your-domain-name]/contact/ or https://[your-domain-name]/contact-information/ to provide contact information.

{{< wssbox >}}

## Summary
Provide a comprehensive privacy policy at least. Be honest about what personal data you process and why you process it. The security policy and disclosure policy are optional, however, they provide an incentive to carefully document your security concept. Moreover, think about other policies like terms and conditions, copyright and trademark information, disclaimers, accessibility information and so on.

Last but not least, you should provide an e-mail address and a GPG key, so people can contact you confidentially. The proposed security.txt file allows you to provide standardized information for security researches, and people who want to send you security-related information.

## Links
* {{< extlink "https://securitytxt.org/" "security.txt – A proposed standard which allows websites to define security policies." >}}
