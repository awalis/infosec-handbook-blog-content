+++
title = "/e/ – privacy-enabled Android ROM, or Evil Corp?"
author = "Jakub"
date = "2019-03-15T14:52:37+01:00"
lastmod = "2019-03-17"
tags = [ "android", "rom", "lineageos", "e-foundation" ]
categories = [ "Privacy" ]
ogdescription = "/e/ promises a privacy-enabled smartphone OS–we look at it in this article."
slug = "e-foundation-first-look"
banner = "banners/e-foundation"
+++

Last November, LineageOS dropped support for more than 20 smartphones, leaving them vulnerable to future flaws in Android. Unfortunately, this also affected one of our 5 cell phones used for testing apps. We started to look for a replacement, and in January, we spotted the French /e/ Foundation that promises a privacy-enabled smartphone operating system.

In this article, we briefly look at the features offered by the /e/ Android ROM, and whether there is actual "better data privacy and data security for individuals and corporations" as promised by /e/.
<!--more-->
## Contents
1. [Getting started]({{< relref "#getting-started" >}})
  * [Who is behind /e/?]({{< relref "#e-foundation" >}})
  * [Installing the /e/ ROM]({{< relref "#installation" >}})
2. [Features]({{< relref "#features" >}})
3. [Security, and privacy]({{< relref "#sap" >}})
  * [Communication with the internet]({{< relref "#communication" >}})
  * [Patch level of the device, and updates]({{< relref "#patchlevel" >}})
  * [/e/ Foundation websites]({{< relref "#websites" >}})
4. [Summary]({{< relref "#summary" >}})
5. [Links]({{< relref "#links" >}})
6. [Changelog]({{< relref "#changelog" >}})

{{< rssbox >}}

## Getting started
### Who is behind /e/? {#e-foundation}
According to the [website of /e/ Foundation]({{< relref "#links" >}}), it is run by the French entrepreneur G. Duval. Guval founded the /e/ Foundation in May 2018, and released the first beta of the /e/ operating system in September 2018. The manifesto of the /e/ Foundation states that "/e/ is intended to provide alternative technological products and services", including operating systems for different platforms, and internet services like cloud storage, e-mail, and a search engine.

/e/ (named "eelo" back then) raised €94,760 on Kickstarter and €14,371 on Indiegogo. Its members must pay an annual fee.

### Installing the /e/ ROM {#installation}
At the moment, /e/ supports 60 smartphones built by 16 manufacturers. We needed an up-to-date ROM for our Motorola Moto G4 (athene), originally released in May 2016. /e/ provides ROMs via their own GitLab instance. We already unlocked the bootloader of the G4, so we only had to download /e/. Then, we flashed it using `adb sideload`.

In summary, installing /e/ is as easy as installing LineageOS. You do not need an /e/ account as stated in their requirements.

## Features
The current ROM for the Moto G4 (build date March 11, 2019) is based on Android 7.1.2 (like LineageOS before). The Android security patch level is February 5, 2019, so there is only March 2019 missing at the moment.

Preinstalled apps are among others:

* Bliss Launcher
* Chromium-based /e/ web browser
* LibreOffice Viewer
* Magic Earth (maps client)
* K-9 Mail-based mail client
* microG Services Core
* MuPDF mini (PDF viewer)
* Notes
* OpenKeychain (OpenPGP client)
* OpenTasks
* QKSMS (SMS messenger)
* Signal
* Telegram
* Weather client
* several apps included in LOS/Android (Clock, Contacts etc.)

The Bliss Launcher's design looks like iOS. Using the provided apps is straightforward. You can also download and install F-Droid. The /e/ ROM basically resembles LineageOS.

However, some people may not like Signal and Telegram preinstalled on their phone regardless of whether they use it.

## Security, and privacy {#sap}
Since this article isn't focused on features of /e/, let's proceed to some security and privacy aspects. The /e/ Foundation promotes their same-named mobile OS as "ungoogled", coming with "carefully selected apps".

### Communication with the internet {#communication}
In this section, we discuss several findings regarding /e/'s network traffic when connected to the internet. We uploaded [a full list of all domains]({{< relref "#links" >}}) contacted by /e/ to GitHub.

#### Hello, Google {#google}
We monitored all data connections of /e/ after restarting the phone. First of all, /e/ knocks on Google's door by using its connectivity check:

{{< highlight http "linenos=table" >}}
GET /generate_204 HTTP/1.1
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.82 Safari/537.36
Host: connectivitycheck.gstatic.com
Connection: Keep-Alive
Accept-Encoding: gzip
{{< /highlight >}}

There is an issue (#146) on /e/'s GitLab instance reporting this, however, there is no response.

Other users report that /e/ uses Google's DNS servers (e.g., 8.8.8.8) by default. Since we only deploy our test devices in a controlled VLAN, we weren't able to reproduce this. The settings allow you to set a default DNS resolver (Settings → Wireless & networks → More → DNS → disable "Use network DNS" → Set DNS to use). We tried this but the router enforces the DNS resolver of the network.

Moreover, there is TLS 1.2-encrypted traffic from/to www.google.com, gstaticadssl.l.google.com, googleadapis.l.google.com, and www3.l.google.com via IPv4/IPv6.

#### NTP synchronization around the world {#ntp}
Synchronizing the time of the device is automatically done by /e/. This results in many connection attempts to different domain names. All of these domain names will learn about your IP address at least. Each time, /e/ sent the reference timestamp "January 1, 1970 00:00:00".

{{< webpimg "/art-img/e-foundation-first-look-ntp.png" "NTP synchronization results in many different domain names that learn about your IP address." >}}

#### Weather app leaks personal data–in cleartext {#weather}
The preinstalled weather app (foundation.e.weather, version 4.4) has another problem: It leaks your location in real-time. Each time, you search for a location to get the current weather, the app sends a GET request to api.openweathermap.org—in cleartext:

1. `GET /data/2.5/find?q=l&type=like&cnt=15&APPID=50[…]8 HTTP/1.1`
2. `GET /data/2.5/find?q=li&type=like&cnt=15&APPID=50[…]8 HTTP/1.1`
3. `GET /data/2.5/find?q=lin&type=like&cnt=15&APPID=50[…]8 HTTP/1.1`
4. `GET /data/2.5/find?q=linz&type=like&cnt=15&APPID=50[…]8 HTTP/1.1`

These are consecutive HTTP GET requests sent while typing "linz" (see `?q=linz`). Some GET requests contain the GPS coordinates of the device (`lat=48.3059&lon=14.2862`):

* `GET /data/2.5/weather?appid=50[…]8&lat=48.3059&lon=14.2862&units=metric&lang=en HTTP/1.1`

There is even more. Each GET request contains the User-Agent of the app which contains the identity of the device:

* `User-Agent: Dalvik/2.1.0 (Linux; U; Android 7.1.2; Moto G4 Build/NJH47F)`

In summary, the weather app leaks your IP address, the identity of your device, and maybe your location (city name, GPS coordinates). All is sent in cleartext.

{{< webpimg "/art-img/e-foundation-first-look-weather.png" "The weather app leaks personal data in real-time." >}}

#### Magic Earth–mostly unencrypted traffic {#magicearth}
Another noticeable app is Magic Earth (com.generalmagic.magicearth, version 7.1.18), provided by General Magic. General Magic is located in Switzerland, Romania, and in the Netherlands.

During testing, the Magic Earth app talked to 12 different IP addresses owned by General Magic. Ten times the communication was in cleartext only, e.g., `POST /overlaystiles_maps6 HTTP/1.1`.

We didn't try to decode the traffic sent by the server. However, it is strange that communication is only sometimes encrypted.

On /e/'s GitLab is a "Privacy statement from Magic Earth publisher General Magic" that states "only transmit [data if] necessary, so minimal information from the user to the server [will be sent]". There is no explanation what kind of information is sent to General Magic's servers. Furthermore, the privacy policy of General Magic states:

>We may collect information such as occupation, language, zip code, area code, unique device identifier, location, and the time zone where a product is used so that we can better understand customer behavior and improve our products, services, and advertising.

Additionally:

>We may collect information regarding customer activities on our website, and store and from our other products and services. This information is aggregated and used to help us provide more useful information to our customers and to understand which parts of our website, products, and services are of most interest. Aggregated data is considered non-personal information for the purposes of this Privacy Policy.

So, it remains unclear what kind of information is sent/received to/from General Magic in cleartext.

{{< webpimg "/art-img/e-foundation-first-look-map.png" "Most servers contacted by Magic Earth only like unencrypted HTTP traffic." >}}

### Patch level of the device, and updates {#patchlevel}
Finally, we looked at the patch level of /e/. It reports "February 5, 2019". We used SnoopSnitch 2.0.8 (available on F-Droid) to check the patch status. SnoopSnitch reported only one patch missing. However, the current version doesn't check for patches newer than patch level October 2018.

However, /e/ provided frequent Android updates so far. The official, no longer maintained, LineageOS ROM for Moto G4 is stuck at patch level October 2018.

Keep in mind that keeping your Android ROM up-to-date is important, however, even an up-to-date Android patch level won't fix vulnerabilities in other chips (e.g., GPS, broadband) of your smartphone.

{{< webpimg "/art-img/e-foundation-first-look-patchstatus.png" "SnoopSnitch reports only one patch missing. However, the current version doesn't check for patches newer than patch level October 2018." >}}

### /e/ Foundation websites {#websites}
Finally, we looked at three different websites of the /e/ Foundation.

The main website is e.foundation. The website is based on WordPress 5.0.4, and WooCommerce 3.5.4. The current version of WooCommerce is 3.5.6 (released on March 7, 2019). WooCommerce 3.5.5 (released on February 20, 2019) contains security-related fixes. WordPress versions prior to 5.1.1 are likely vulnerable to several critical bugs discovered in the last weeks (we wrote about them on Mastodon). The web server seems to be hosted in France by Scaleway (Online SAS), and likely runs Apache 2.4.10 on Debian 8.

Besides, the main page embeds more than 150 JavaScript files and some inline JS. Considering that the /e/ Foundation website didn't set most modern security features like Content Security Policy, HSTS, X-Content-Type-Options, X-Frame-Options, X-XSS-Protection, or cookie security (HttpOnly, Secure, SameSite), this doesn't look that good. The website also offers outdated TLS 1.0 and 1.1 protocols as well as really old DES-based cipher suites.

/e/ Foundation uses Slimstat Analytics to collect information about users, and sets cookies to "[u]nderstand and save user’s preferences for future visits, and select the right language, if available". /e/ also states to "not use third-party services (like Google Analytics) that track [user] information on [their] behalf, as [they] do not use any third party tracker." However, their website embeds a Google font. Visiting their website likely results in communication with Google's servers.

/e/'s GitLab instance is hosted by OVH SAS. Their GitLab comes with more security features than their website.

Their search engine is a customized Searx 0.14 instance hosted by Online SAS.

We already reached out to /e/ Foundation regarding their usage of Google fonts in January, however, we didn't get an answer so far.

## Summary
_Update: **In August 2019, [we checked /e/ again]({{< ref "/blog/e-foundation-second-look.md" >}}).**_

Original summary: /e/ promises "better data privacy and data security for individuals and corporations" by offering an "ungoogled" smartphone OS. As shown above, this isn't completely true. There is still Google traffic when using their operating system. Besides, the weather app and Magic Earth transfer personal data in cleartext. Then, there is a Google font on their website.

Additionally, some people don't want Signal or Telegram preinstalled on their smartphone. We observed traffic between the smartphone and an IP address of the Telegram Messenger Network without using the app.

While /e/ looks promising, it isn't Google-free by now.

Mr. Guval wrote a statement after we published this article: {{< extlink "https://medium.com/@gael_duval/leaving-apple-google-how-is-e-actually-google-free-1ba24e29efb9" "Leaving Apple & Google: How is /e/ actually Google-free?" >}}

## Links
* {{< extlink "https://e.foundation/" "Website of /e/ Foundation" >}}
* {{< extlink "https://gist.github.com/infosec-handbook/3fcd8f875b0788a96380b9305c23027e" "full list of domain names contacted by /e/ within 30 minutes" >}}

## Changelog
* Mar 17, 2019: Added link to answer by Mr. Guval.
