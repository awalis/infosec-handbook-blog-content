+++
title = "What is 'secure'?"
author = "Benjamin"
date = "2018-09-02T11:19:01+02:00"
lastmod = "2019-06-04"
tags = [ "privacy", "encryption", "audit" ]
categories = [ "discussion" ]
ogdescription = "Many products and services claim to be 'secure'. What does this mean?"
slug = "discussion-secure"
banner = "banners/discussion-secure"
+++

Secure crypto, secure passwords, secure messaging, secure e-mail, secure browsing—we see "secure something" everywhere, but no one defines this term. On closer inspection, "secure" even becomes a catchword most of the time. We discuss different examples in this article.
<!--more-->
## Contents
1. [The "secure" something]({{< relref "#something" >}})
  * [Secure cryptographic algorithms]({{< relref "#sa" >}})
  * [Secure passwords]({{< relref "#sp" >}})
  * [Secure authentication]({{< relref "#sauth" >}})
  * [Secure messaging]({{< relref "#sm" >}})
  * [Secure e-mail]({{< relref "#se" >}})
  * [Secure browsing]({{< relref "#sb" >}})
  * [Secure devices]({{< relref "#sd" >}})
3. [Summary]({{< relref "#summary" >}})
4. [Sources]({{< relref "#sources" >}})
5. [Changelog]({{< relref "#changelog" >}})

{{< rssbox >}}

## The "secure" something {#something}
Let's have a look at different topics and the term "secure" in their context:

### Secure cryptographic algorithms {#sa}
There are at least two golden rules in cryptography:

* Never roll your own cryptography.
* Only use well-known and audited cryptographic algorithms.

We can basically say that "secure" means "unbreakable". However, nothing is unbreakable since attackers can always use [brute force]({{< ref "/glossary.md#brute-force-attack" >}}) to access something.

"Unbreakable" is also relative to the capabilities of an attacker. A single attacker probably doesn't have the technical resources, the knowledge, or the time to break cryptography while an intelligence agency may be capable to do this. However, "secure" cryptographic algorithms can become insecure due to technological advance over time. For instance, the Data Encryption Standard (DES) was considered "secure" and "unbreakable" once. However, as computers became faster and faster, the key length became insufficient. Therefore, even a single attacker might be able to break DES nowadays.

Even if developers follow the two rules mentioned above, their piece of software using only well-known and audited cryptographic algorithms isn't automatically secure. There are different reasons for this, for example:

* They incorrectly implement the code, making it possible to bypass cryptography.
* They use insecure modes of operation for block ciphers.
* They use an outdated crypto library that contains security vulnerabilities.

Therefore, it is important that developers use audited cryptographic libraries and that the whole product (esp. the implementation of cryptography) is audited by a third party afterwards. Even audited software isn't "secure" since developers can introduce new vulnerabilities during fixing bugs or developing new versions. See also "[Myth 2: Audited software is more secure than software which hasn't been audited]({{< ref "/blog/myths-software-security.md#m2" >}})".

We sometimes see software developers claiming that their app is secure since it uses AES-256 or something like that. No, it isn't. Moreover, we see users telling other users that an app is secure because the cryptographic algorithm in it has been independently audited. This isn't sufficient.

### Secure passwords {#sp}
Discussing password security is another unpopular topic. Ask 10 different security experts and you get 11 different opinions about password length, complexity and change interval. Some people add fuel to the fire by telling users that even the NIST (National Institute of Standards and Technology) or other agencies don't recommend to change passwords at all (which isn't true by the way) or that these people found a magic way to create secure passwords or passphrases.

To keep it simple, there is no "secure" password. A password can be stronger or weaker, but every password can be guessed by using [brute force]({{< ref "/glossary.md#brute-force-attack" >}}). Moreover, technical progress makes strong passwords weaker over time.

Instead of making your passwords longer and longer and adding more and more different characters or using the latest magic creation formula, you should make use of [two-factor authentication]({{< ref "/glossary.md#2fa" >}}) if available and stick with a well-known, maintained and regularly audited password manager.

We covered credential management (and passwords) in "[Modern credential management: security tokens, password managers, and a simple spreadsheet]({{< ref "/blog/modern-credential-management.md" >}})".

### Secure authentication {#sauth}
Besides passwords, there are many other less-known possibilities to authenticate yourself: cryptographic keys (e.g., SSH and GPG keys), digital certificates, passphrases, time-based one-time passwords ([OATH-TOTP]({{< ref "/glossary.md#oath-totp" >}})), Universal 2nd Factor ([U2F]({{< ref "/glossary.md#u2f" >}})), or [WebAuthn]({{< ref "/glossary.md#webauthn" >}}).

As before, you can't simply say that something is "secure" or "insecure". For instance, why is an SMS-based one-time password insecure or secure? Is a wannabe hacker from your neighborhood able to use this OTP by "hacking" your device or the cellular network? This may be very unlikely. Contrary to this, an intelligence agency or law enforcement may be able to easily bypass this by directly accessing the server. No authentication needed in this case.

Apart from this, many attacks are primarily successful due to [social engineering]({{< ref "/glossary.md#social-engineering" >}}), not due to successful technical exploitation. It doesn't matter whether you send your SMS-based or app-based OTP to an attacker due to successful social engineering. Even if you do everything right, the connection to the server could be compromised, or—as already mentioned—an attacker could directly access the server-side database.

Hence, as before, "security" depends on your threat model. You will never achieve 100% security, and security isn't an easy-to-determine black-and-white world.

### Secure messaging {#sm}
The Electronic Frontier Foundation (EFF) published a Secure Messaging Scorecard in 2014. The scorecard consisted of:

* transport encryption
* [end-to-end encryption]({{< ref "/glossary.md#end-to-end-encryption" >}}) (E2EE)
* verification of identities
* [perfect forward secrecy]({{< ref "/glossary.md#perfect-forward-secrecy" >}})
* proper documentation of the security design
* third-party audits
* recent code audit

In 2018, the [EFF tells us]({{< relref "#sources" >}}) that there "is no such thing as a perfect or one-size-fits-all messaging app", because the scorecard "oversimplified the complex question of how various messengers stack up from a security perspective" and "it wasn’t possible for [the EFF] to clearly describe the security features of many popular messaging apps, in a consistent and complete way, while considering the varied situations and security concerns of [the EFF's] audience".

As a result, they didn't update their scorecard but published a series of articles to discuss the nature of "secure" messaging so users can develop "an understanding of secure messaging that is deeper than a simple recommendation".

We think "security" means "somewhat encrypted" for most users. However, there is a big difference between encrypted client-server communication and end-to-end encrypted communication, for example. Then, there is optional encryption with unencrypted fallback in some messengers or messengers which store tons of [personal data]({{< ref "/glossary.md#personal-data" >}}) in cleartext on servers exposing this data to admins and server-side attackers.

The EFF also says that an "app with great security features is worthless if none of your friends and contacts use it, and the most popular and widely used apps can vary significantly by country and community." They argue that E2EE is "great for preventing companies and governments from accessing your messages" but "if someone is worried about a spouse, parent, or employer with physical access to their device, the ability to send ephemeral, 'disappearing' messages might be their deciding factor in choosing a messenger".

To sum up, we can say that there is no one-size-fits-all "secure" messenger. Different users want different security features. There is another EFF article which lists some security features:

* end-to-end encryption (many messaging apps use or are based on the Signal protocol)
* code quality (using "secure" algorithms [doesn't mean that they are securely implemented]({{< relref "#sa" >}}))
* user experience (can users easily send and receive encrypted messages?)
* service availability
* encrypted cloud backups (some messengers store unencrypted backups on the internet rendering E2EE useless)
* secure auto-updating mechanisms
* messenger of sufficiently high popularity that its use is not suspicious
* indicators of compromise that are recognizable to an end-user
* verification of identities
* aliases instead of phone numbers
* avoidance of network [metadata]({{< ref "/glossary.md#metadata" >}})
* contact discovery without disclosing your contacts to the service provider
* reproducible builds
* binary transparency
* the same level of security even in group chats

No need to say that no messenger offers all of these features. You have to be aware of these features and find the messenger which fits your needs.

### Secure e-mail {#se}
If you look for "secure" e-mail, you will mostly find [GnuPG]({{< ref "/glossary.md#gnupg" >}})- or OpenPGP-based solutions. GnuPG and OpenPGP are around for years but lack usability. Some services try to make them more usable by implemented encryption directly in the web browser but they force you and others to use their services in order to benefit from encryption.

Similar to "secure" messaging, most people seem to consider "encrypted" e-mails to be secure and we face similar problems again:

* end-to-end encryption using GnuPG/OpenPGP isn't widespread
* GnuPG/OpenPG aren't easy to use
* metadata and personal data stored on servers remain unencrypted

Considering the fact that messaging apps are far more popular when it comes to private communication nowadays, we don't discuss this here. However, we are absolutely convinced that the term "secure e-mail" is more than vague.

### Secure browsing {#sb}
When it comes to "security" in the web browser, everybody seems to focus on the presence of HTTPS/[TLS]({{< ref "/glossary.md#tls" >}}) nowadays. However, the sheer presence of HTTPS/TLS doesn't guarantee "security".

Web servers can:

* provide distrusted [certificates]({{< ref "/glossary.md#certificate" >}})
* provide revoked or illegally issued certificates
* offer insecure TLS protocol versions (1.0, 1.1) or even really old SSL
* enable [cipher suites]({{< ref "/glossary.md#cipher-suites" >}}) with weak cryptographic algorithms and without PFS or [AEAD]({{< ref "/glossary.md#aead" >}})
* offer HTTPS without enforcing it (by enabling HTTP _and_ HTTPS)
* provide insecure cookies or dangerous dynamic content like JavaScript
* embed insecure third-party content
* enable only weak elliptic curves and DH parameters
* provide useless Content Security Policies and other security-related HTTP headers, which aren't widely supported or misconfigured
* be vulnerable to common attacks due to outdated web server software (e.g., POODLE, Zombie POODLE, Golden POODLE, Heartbleed, Sleeping POODLE, OpenSSL vulnerabilities)

Needless to say that there are many more security features than just HTTPS. Furthermore, HTTPS can be rendered useless if configured wishy-washy.

Keep in mind that HTTPS/TLS is only there to protect data in transit! It doesn't protect server-side data at rest (e.g., your personal data stored in a server-side database), or data in use (e.g., your personal data, currently processed by the web server).

And that isn't all, because you must consider that not only the server must be configured securely but also your client. The list above doesn't include all available web security features and some modern security features aren't widely supported by non-mainstream web browsers.

All in all, "secure" browsing is far more than only enabling HTTPS and hoping for the best.

### Secure devices {#sd}
Finally, there is oftentimes an ongoing highly charged debate about "secure" devices. Some people state that all smartphones are "insecure", other tell you that all "internet of things" devices are insecure. However, is your laptop, desktop computer, or server "secure"?

What means "security" in this context? All security updates installed? Full-disk encryption enabled? All unused interfaces disabled? A non-root user account, which is used by default? No known security vulnerabilities in software installed on the device, including firmware?

Mostly, all of this remains totally unclear. You (again) must define your own threat model and your own criteria to classify the security of your devices. Blanket statements are oftentimes misleading and don't consider your specific use cases.

{{< mastodonbox >}}

## Summary
If we take some points mentioned above into account, "security" …

* … changes over time
* … isn't clearly defined in most cases
* … heavily depends on the capabilities of an attacker
* … is a process and not a property
* … requires a holistic view and not detached assessments of separate security features
* … isn't "one size fits all"

Keep this in mind when you discuss "secure somethings" in future.

## Sources
* {{< extlink "https://www.eff.org/deeplinks/2018/03/secure-messaging-more-secure-mess" "EFF: Secure Messaging? More Like A Secure Mess." >}}

## Changelog
* Jun 4, 2019: Added "secure authentication" and "secure devices". Revised the section "secure cryptographic algorithms".
