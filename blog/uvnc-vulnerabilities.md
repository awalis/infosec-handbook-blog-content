+++
title = "UltraVNC – a security nightmare"
author = "Benjamin"
date = "2019-03-30T09:31:00+01:00"
lastmod = "2019-04-03"
tags = [ "ultravnc", "vnc", "remote-access", "joomla", "phpbb" ]
categories = [ "vulnerability" ]
ogdescription = "This month, Kaspersky published 22 vulnerabilities in UltraVNC client and server. We looked at UltraVNC's website that is also insecure."
slug = "uvnc-vulnerabilities"
banner = "banners/security-risk"
+++

UltraVNC is open-source software to remotely control other systems and visually share desktops. If you look at its track record, it looks great: only 7 security [vulnerabilities]({{< ref "/glossary.md#vulnerability" >}}) in 13 years. However, this month, Kaspersky published not only one newly-found vulnerability in UltraVNC, not two, not five, but 22 security vulnerabilities (KLCERT-19-003 to KLCERT-19-024) that all have their own [CVE]({{< ref "/glossary.md#cve" >}}) identifiers. Most vulnerabilities come with a [CVSS]({{< ref "/glossary.md#cvss" >}}) v3.0 base score of 10.0 out of 10.0, which means that it can't be worse anymore.

The official website and forum of UltraVNC aren't better: there is no HTTPS, there are no modern security features, there was a file containing secrets, and the CMS is obviously totally outdated. In this article, we show several vulnerabilities of uvnc.com to raise awareness about insecure websites.
<!--more-->
## Contents
1. [A common problem: security by assumption, hope, and belief]({{< relref "#sbh" >}})
2. [Vulnerabilities in UltraVNC's code]({{< relref "#code" >}})
3. [UltraVNC's website]({{< relref "#website" >}})
  * [JavaScript everywhere]({{< relref "#js" >}})
  * [No HTTPS, no security headers]({{< relref "#https" >}})
  * [Publicly-accessible secrets]({{< relref "#secrets" >}})
  * [Joomla 1.5.15]({{< relref "#joomla" >}})
  * [Exposed web statistics]({{< relref "#webstats" >}})
4. [UltraVNC's forum]({{< relref "#forum" >}})
5. [UltraVNC's (non) reaction]({{< relref "#reaction" >}})
6. [Summary]({{< relref "#summary" >}})

{{< rssbox >}}

## A common problem: security by assumption, hope, and belief {#sbh}
First things first. Many people know the term "security by obscurity" (or "security through obscurity"): someone creates something, and keeps its details secret to "secure" it. However, this doesn't work and most people know it these days.

Then, there is a set of people who repeatedly state that open-source software is secure "since everybody can check the source code for security vulnerabilities". We already wrote in other articles that this doesn't work either. The problem is (broadly speaking):

* developers hope that someone checks their source code for security vulnerabilities
* third-party developers assume that someone already checked the source code for security vulnerabilities
* end users belief that the application is secure since it is open source and someone surely already checked the source code for security vulnerabilities
* in the end, no one checked the source code

We call this "security by assumption, hope, and belief". This isn't limited to UltraVNC, of course. In January, a cryptographic weakness in 7-Zip was found—an open-source file archiver. 7-Zip's source code is also available, however, "security by assumption, hope, and belief" kicked in. Nobody looked at the vulnerable crypto part of the code so far.

In summary, never assume that any code is "secure". In case of doubt, nobody checked it. And no, not "everybody can check open-source code for security vulnerabilities". Most people aren't developers, and developers neither "speak" every programming language possible nor master bug-free secure coding. Simply forget this urban legend.

## Vulnerabilities in UltraVNC's code {#code}
As mentioned above, Kaspersky found 22, mostly critical, security vulnerabilities in the UltraVNC source code (affecting client and server). According to Kaspersky, all UltraVNC versions are affected. They found the vulnerabilities in several revisions of UltraVNC (1198 to 1211), and point to revision 1212 that addresses all vulnerabilities so far. Revision 1212 is part of UltraVNC 1.2.2.4 that is available for about two weeks. Versions prior to 1.2.2.4 come with the already mentioned vulnerabilities.

We tried to contact the UltraVNC team to verify that they fixed all 22 vulnerabilities. However, there is no security contact and they didn't include detailed information about fixed vulnerabilities in their changelogs.

## UltraVNC's website {#website}
 Going to UltraVNC's website uncovers a new problem: there is little to no security. Actually, the website looks like somebody set it up many years ago and nobody maintained it afterwards. However, there is a forum (which we will discuss below) and developers still publish posts there. So, the developers are there but seemingly don't care about security.

### JavaScript everywhere {#js}
When you navigate to uvnc.com, the website will tell you that you must enable JavaScript (if you turned it off by default). As soon as you turn on JavaScript, all the nasty scripts will be executed: Google Analytics, Ads by Google, Facebook Connect, and more. The website also sets tracking [cookies]({{< ref "/glossary.md#cookie" >}}), and modifies the localStorage of the web browser (thanks to Google AdSense). There is no Subresource Integrity (SRI) that would allow to securely embed third-party content. This means that third parties (or attackers) can change the embedded content—uvnc.com will happily deliver it to your web browser. Malvertising, anyone?

### No HTTPS, no security headers {#https}
The second obvious observation is the absence of HTTPS. There is actually a 2048 bit Let's Encrypt HTTPS [certificate]({{< ref "/glossary.md#certificate" >}}) that is valid for www.uvnc.com, not for uvnc.com (no www part). However, the website doesn't redirect non-www to www requests (or vice versa). It also doesn't redirect HTTP to HTTPS. So, the HTTPS certificate is useless, and the certificate chain is in incorrect order, too. Even worse: The web server accepts totally outdated and insecure SSLv3, and outdated TLS 1.0/1.1 protocols. Moreover, it accepts stone-age cipher suites that use 3DES, RC4, IDEA, and RSA-based key exchange.

There is more! The web server, which is installed on a server of Kansas-based APH Inc. (operating as Codero), is vulnerable to POODLE (Padding Oracle On Downgraded Legacy Encryption, CVE-2014-3566) due to its support for stone-age crypto: attackers can downgrade TLS 1.x to SSLv3. Additionally, there is not a single modern security header present (HTTP Strict Transport Security, Content Security Policy, X-Frame-Options, X-XSS-Protection, X-Content-Type-Options, Referrer Policy, Feature Policy etc.).

By the way, this is all also true for the admin access of uvnc's CMS.

### Publicly-accessible secrets {#secrets}
The list of weaknesses and security problems continues. There is a file that contained configuration details and secrets. Since the UltraVNC team changed the content of the file to "index.php" after we contacted them, we show the remaining default content of the file in the screenshot below to raise awareness. Such publicly-accessible files are a rich source for any attacker.

{{< webpimg "/art-img/uvnc-vulnerabilities-secret.png" "One file on uvnc.com disclosed configuration details and secrets (we removed all secrets in the screenshot). The UltraVNC team changed the content of the file after we contacted them." >}}

### Joomla 1.5.15 {#joomla}
Furthermore, we looked at their CMS. The website is powered by Joomla. Joomla is a famous content management system (CMS) nowadays. Most content management systems (e.g., Drupal, WordPress, Joomla) are extremely complex software packages that contain security vulnerabilities. Security vulnerabilities are common. There were more than 130 vulnerabilities with CVE identifiers in Joomla alone. The important part for administrators of websites is that they update their content management systems as soon as possible.

Let's go back to uvnc.com. Its CMS identifies itself as "Joomla! 1.5 - Open Source Content Management". Using fingerprinting techniques to identify the actual version number results in "Joomla 1.5.15". Joomla 1.5.15 was released in November 2009. If uvnc.com really uses Joomla 1.5.15, it uses a totally outdated CMS with up to 100 publicly-known security vulnerabilities. This poses a high risk for any visitor of the website.

### Exposed web statistics {#webstats}
Finally, we found a publicly-accessible instance of Webalizer 2.01-10 that contains stats from April 2017 to March 2018. These stats contain IP addresses in cleartext. It is unclear whether these IP addresses belong to individuals or bots. If they belong to private users, UltraVNC leaks personal data, too.

{{< webpimg "/art-img/uvnc-vulnerabilities-webstats.png" "A publicly-accessible instance of Webalizer 2.01-10 contains stats of one year. These stats include IP addresses." >}}

## UltraVNC's forum {#forum}
UltraVNC's phpBB-powered forum is accessible on uvnc.com. If you click links on the forum page, its URL doesn't change. The source code tells us the reason: The forum (forum.ultravnc.info) is embedded with the help of an `<iframe>`. Inline frames can be used to embed a document within HTML pages. HTML 5 introduced additional controls (using the `sandbox` parameter) to restrict inline frames. However, inline frames pose a security risk. The embedded website could serve malware, and there is the risk of [clickjacking]({{< ref "/glossary.md#clickjacking" >}}).

Obviously, there are the same security vulnerabilities as before: no HTTPS, no security headers, and maybe an outdated version of phpBB. It is very likely that any website can embed the forum and conduct clickjacking. Moreover, the PHP version of the forum identifies itself as "PHP/5.3.3-7+squeeze19". This implies that the forum's operating system is Debian 6 "Squeeze", and it is likely that "apache2 2.2.16-6+squeeze12" is in use. The LTS support for Debian 6 ended in February 2016 while Apache and PHP are unsupported since 2014.

The overall (in)security of the website could also be an indicator for poorly-secured databases used by Joomla and phpBB. Hopefully, we won't observe just another data breach containing [personal data]({{< ref "/glossary.md#personal-data" >}}) and passwords of people who used the UltraVNC forum in future.

## UltraVNC's (non) reaction {#reaction}
We reached out to UltraVNC and asked for a comment on the most severe findings. As of now, they only removed the above mentioned secrets from the publicly-accessible file. We didn't get a reply, or observe any other reaction so far. Later, the UltraVNC team removed the `<meta name="generator" … />` tags from their HTML code to hide their Joomla version without updating anything.

{{< mastodonbox >}}

## Summary
This article shows that you should never assume that somebody already checked something for security vulnerabilities. This isn't only true for UltraVNC or open-source software but also for your personal website, forum, or any other digital content. Several "security" blogs simply recommend UltraVNC because it is open source (= secure and trustworthy, according to them).

If you run a web server, care about it—install updates, set up technical security measures (see our [web server security series]({{< ref "/as-wss.md" >}})), monitor it. Just setting up a web server under the motto "never touch a running system" is a big risk for every visitor of your website. Even if you don't host valuable data or don't process personal data of others, your server could become part of a [botnet]({{< ref "/glossary.md#botnet" >}}) to conduct illegal activities. Additionally, at least reply to security-related/privacy-related e-mails!

Finally, keep in mind that Virtual Network Computing (VNC) uses the Remote Framebuffer Protocol (RFB, RFC 6143). RFB 3.8 is insecure by design. The only optional security mechanism is a legacy [challenge–response authentication]({{< ref "/glossary.md#challenge-response-authentication" >}}) method, using 8-digit passwords and DES. If you use VNC, run it only in controlled networks (e.g., your LAN or via VPN). Some VNC clients like UltraVNC bring their own encryption features that aren't compatible with clients/servers offered by other organizations.

## Changelog
* Apr 3, 2019: Updated Joomla version, added information about PHP/Debian/Apache in use, added information about publicly-accessible statistics website
