+++
title = "The false sense of security"
author = "Benjamin"
date = "2019-08-03T14:34:00+02:00"
tags = [ "privacy", "encryption", "server-security", "ssh", "https", "tls" ]
categories = [ "discussion" ]
ogdescription = "You can't simply turn on some security and privacy."
slug = "discussion-false-sense"
banner = "banners/discussion-security-blogs"
+++

Last year, we wrote about ['security' blogs and websites that actually cause insecurity]({{< ref "/blog/discussion-security-blogs.md" >}}). This time, we look at three reasons for a false sense of security.
<!--more-->
## Contents
1. [Three reasons for a false sense of security]({{< relref "#top3" >}})
  * [Reason 1: Legacy configuration and outdated security tips]({{< relref "#r1" >}})
  * [Reason 2: No threat model]({{< relref "#r2" >}})
  * [Reason 3: No checks and no monitoring]({{< relref "#r3" >}})
2. [Summary]({{< relref "#summary" >}})

{{< rssbox >}}

## Three reasons for a false sense of security {#top3}
Oftentimes, private users are focused on "secure configuration". They ask whether their setup, tool, or hardware is "secure". We discussed this in "[What is 'secure'?]({{< ref "/blog/discussion-secure.md" >}})". Their mindset seems to be like shown in the picture below: There is a toggle switch somewhere, and you just have to turn on some security and privacy.

{{< webpimg "/art-img/false-sense-of-security-toggle.png" "Some people assume that security and privacy are binary properties. However, you can't simply turn on some security and privacy." >}}

The main reason for this mindset could be the omnipresent focus on technology when it comes to information security. However, as discussed in other articles, technology is only a small subset of information security.

In the following, we present three reasons for a false sense of security when it comes to configuring technology to make it "more secure".

### Reason 1: Legacy configuration and outdated security tips {#r1}
If you look for "[hardening]({{< ref "/glossary.md#hardening" >}}) guides" or "secure configuration" on the internet, you find hundreds of blog articles, online assessment tools, and recommendations.

#### What is the problem? {#r1-problem}
Guides and recommendations are snapshots in time. The next software update could remove parameters that were recommended, or good practices change since there are new cryptographic algorithms available. This means that you configure something as written in a guide, however, you don't improve anything in reality. You think that something is more secure now, but this is wrong.

#### Three examples {#r1-examples}
1. Legacy HTTP response headers: Many guides recommend setting the HTTP response headers "X-Frame-Options" and "X-Xss-Protection". There are even some bloggers who tell you that their blog is more secure since they set these headers. In reality, all major browsers don't support "X-Xss-Protection". Web browsers simply ignore the header. You don't get any additional security by setting this header. "X-Frame-Options" can be set as a directive of the Content Security Policy (Level 2), which is supported by all modern web browsers.
2. Contradictory TLS recommendations: Some blogs still recommend TLS 1.0 to 1.2. Others updated their recommendation to TLS 1.2 only. Then, there are already blogs recommending TLS 1.2 and 1.3. If you look at TLS curves, some recommend "prime256v1" and "secp384r1". Others tell you to use "secp521r1" or "X25519". Looking at cipher suites, this gets even more complicated. If you compare these recommendations with current good practices and look at parameters that are actually supported by web browsers, you see that some recommendations are outdated or unsupported.
3. Legacy OpenSSH parameters: Some tools still recommend to set parameters like "Protocol". In reality, these parameters aren't supported anymore by current versions of OpenSSH. The same is true for the "-o" flag when generating private keys.

#### How to solve it {#r1-solution}
A good approach is to actually read documentation of things you want to configure. For instance, read `man ssh` to see parameters that are supported by your OpenSSH version. Then, some tools come with built-in checks for their configuration. For example, use `sshd -T`, `nginx -t`, or `apachectl -t` to check server-side configuration. In some cases, these checks show which parameters are in use. Remove any unused parameters.

Be aware of parameters you configure. Understand their meaning. Don't blindly set any parameters. Furthermore, it is important to define your own threat model.

### Reason 2: No threat model {#r2}
Some people aimlessly implement tips from the internet. They configure their Android smartphone according to some guides, then they harden their desktop operating system a little bit, and finally they install some web browser plugins. In the end, they changed something without ever checking whether their setup covers their use cases.

#### What is the problem? {#r2-problem}
Different people have different hardware, software and use cases. For instance, some people only use Apple devices. Others only have an Android smartphone. Some people use their devices for entertainment only, others use them at work. However, the vast majority of guides only reflect particular use cases of their authors. There may be relevant use cases that aren't covered by guides. The obvious problem is that you forget something, because no guide reflects exactly your combination of hardware, software and use cases.

#### Three examples {#r2-examples}
1. Out-of-scope use cases: You look for a guide that covers server hardening. You implement every part of it, however, the guide covers configuration for static content only while you use a content management system. You are convinced that everything is secure now, but you forgot to lock down the database used by the CMS.
2. The forgotten risk: You want to secure your Android smartphone. As mentioned in a guide, you install F-Droid, Firefox and some browser plugins. Finally, you install some apps that mention "security" in their description. Seven days later, you lose your smartphone. Later, somebody easily reads all of your end-to-end encrypted messages, views all of your private photos, and accesses your e-mails. The root cause was the missing part about full-disk encryption and the risk of losing physical access to your smartphone.
3. The wrong assumption: You switch to an instant messenger, which was recommended due to being "secure". You manage to turn on end-to-end encryption. While you think that your messages are "secure" now, tons of metadata and data are managed and stored server-side—in cleartext. The recommendation was only focused on encrypting contents of messages in transit, and assumed that the server is always trustworthy.

#### How to solve it {#r2-solution}
Define your own threat model. There is no other solution. You are the only person who knows which hardware and software you use. You are the only one who knows all of your use cases. If you only do things that are explained in some guides, it is very likely that you forget something.

### Reason 3: No checks and no monitoring {#r3}
Many people configure their hardware and software exactly one time. Then, this configuration is left unchanged for a long period of time.

#### What is the problem? {#r3-problem}
If you never check whether your configuration actually works (or still works), you don't know it. You just assume that everything works as expected. We showed this in our [article on the LineageOS-based /e/ operating system]({{< ref "/blog/e-foundation-first-look.md" >}}): People just assumed that there is no Google anymore, however, many of them didn't actually check it.

#### Three examples {#r3-examples}
1. The attacker was already there: Our favorite example are servers. Nearly all guides on server security only show you how to configure it once. That's all. They mostly never mention that monitoring, alerting and reacting is essential for server security. This may originate from the wrong assumption that you can prevent every type of attack. Detecting ongoing attacks is as important as preventing them, though.
2. Data leaks everywhere: If you look for guides on privacy, you likely come across blogs recommending [VPNs]({{< ref "/glossary.md#vpn" >}}) for privacy. While VPNs can help you securing network traffic under certain conditions, you never know whether all of your traffic is actually routed to your VPN.
3. No control at all: Some guides tell you about "taking back control" by installing certain operating systems and changing some configuration. At the same time, most of your hardware components are likely proprietary and there may be other hardware or software components that are also connected to a network. If you don't check this, you never learn about it.

#### How to solve it {#r3-solution}
It is essential to monitor your complete network traffic and hardware. This is especially true for remotely-located hardware like servers in data centers. Detecting attackers is as necessary as preventing attacks. Furthermore, you need to check if your configuration actually works as expected. Don't assume it—check it.

{{< mastodonbox >}}

## Summary
Hopefully, you got an idea why blindly configuring your hardware and software without a threat model or without checking anything results in a false sense of security.

We update our guides from time to time to address the issue of giving outdated advice. However, we don't know your threat model or uses cases, too.
