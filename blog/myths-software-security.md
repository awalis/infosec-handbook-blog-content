+++
title = "Software security myths"
author = "Benjamin"
date = "2018-05-10T09:14:10+02:00"
lastmod = "2018-12-07"
tags = [ "software-security", "open-source", "audit" ]
categories = [ "myths" ]
ogdescription = "Open-source software is more secure than proprietary software, isn't it? This and other myths debunked in our new article."
slug = "software-security-myths"
banner = "banners/software-security-myths"
+++

Nowadays, everybody runs software and software gets more and more complex. Even a few lines of code can produce big security [vulnerabilities]({{< ref "/glossary.md#vulnerability" >}}) which put confidential information and [personal data]({{< ref "/glossary.md#personal-data" >}}) at risk.

But there is open-source software! Sometimes, we actually read statements like "open-source software is more secure than proprietary software" or people consider audits which were conducted three years ago as "security guarantees". We show you why these myths are wrong.
<!--more-->
## Contents
1. [Myth 1: Open-source software is more secure than proprietary software]({{< relref "#m1" >}})
2. [Myth 2: Audited software is more secure than software which hasn't been audited]({{< relref "#m2" >}})
3. [Myth 3: Many reported issues mean insecurity]({{< relref "#m3" >}})
4. [Myth 4: Using open-source software is secure]({{< relref "#m4" >}})
5. [Summary]({{< relref "#summary" >}})
6. [Sources]({{< relref "#sources" >}})
7. [Changelog]({{< relref "#changelog" >}})

{{< rssbox >}}

## Myth 1: Open-source software is more secure than proprietary software {#m1}
**Wrong**. The differences between "open source" and "proprietary" are licenses not security features. This blanket statement often implies that "everybody" can look at open-source code and then software magically becomes more secure. On the other hand, proprietary couldn't be audited and this must be at least highly suspicious!

Ask yourself whether you have the ability to understand and check the complete source code of a new app, imported libraries and so on. Are you used to programming? Then, this may be like a walk in the park for you. But did you also check every update, especially when software is automatically updated without any visual hints or prompts? Did you check all imported libraries? Did you check the source code of your operating system? Did you also check the source code of your mostly proprietary hardware? Can you understand every line of code, possible risks for the security and integrity of your system? Can you understand every algorithm? It is a safe bet that this isn't the case. Vulnerabilities like Dirty COW or Heartbleed showed that open-source software isn't magically secure only because everybody can look at the code.

A more correct statement is that **maintained** software is more secure than unmaintained software: You **may** get more secure software when its developers regularly check the code and fix vulnerabilities as fast as possible.

## Myth 2: Audited software is more secure than software which hasn't been audited {#m2}
Another myth is that "audited software is more secure than software which hasn't been audited". Some people tell you that audits conducted three years ago are some kind of guarantee that this code is secure. This is **wrong**, of course.

For instance, there were two independent audits of the well-known OpenVPN software in 2017. Vulnerabilities were fixed, everything seemed to be okay. However, Guido Vranken discovered four critical security vulnerabilities afterwards. This example shows that security audits won't discover every possible vulnerability in somebody's source code. It isn't actually the purpose of audits to find every possible security vulnerability in code.

Furthermore, you have to keep in mind that developers aren't obliged to fix any bugs found by auditors. They can just say "We won't fix that". They can also try to fix a vulnerability and introduce a new one or even more.

Another aspect is time: An audit conducted one, two or three years ago may be already irrelevant because developers modified hundreds of lines of code or dependencies got updated.

## Myth 3: Many reported issues mean insecurity {#m3}
Finally, some people state that hundreds of GitHub issues (or issues reported elsewhere) indicate insecurity of code. This is also **wrong**.

Imagine a small project of a Computer Science student: This student implements a lightweight web browser and uploads it to GitHub. About 150 users download the browser, 100 use it on a daily basis. No one reports any issues in the following months since the users don't encounter any issues.

On the other hand, there is a well-known and globally used web browser. People report issues every day and there are hundreds of issues still open.

The question is: Which web browser is more secure? The first one since there are 0 reported issues or the second one because millions of people use it every day and its developers a trained programmers?

We recommend to check some of the oldest and some of the newest issues. Some people misuse issue trackers for feature requests and not every issue is a security vulnerability. Check some security-related issues and whether the developers fixed these issues as soon as possible.

## Myth 4: Using open-source software packages is secure {#m4}
The final statement is about using open-source software packages. Normally, Linux-based operating systems come with their own package repository, and then there are software-specific repositories (e.g., Python Package Index, NPM repository, or Flathub). Some people think that using these repositories is secure since they are providing open-source software, and there is (again) some magical security present.

This is **wrong**, of course. In May 2018, an Ubuntu user spotted "2048buntu", a crypto currency miner on the Ubuntu Snap Store. In July 2018, people discovered several malicious packages in the Arch User Repository ("acroread", "balz", "minergate"), which gathered information about the victim's system. In October 2018, a security engineer scanned the Python Package Index and identified 12 packages, which performed different malicious actions. In November 2018, a malicious version of "event-stream" was discovered in the NPM repository, which was able to steal account data for crypto currencies.

So, don't believe that packages are automatically secure and good, even if you download them from official repositories.

{{< mastodonbox >}}

## Summary
The bottom line is that there is no magical security in open-source products, audits aren't security guarantees and many issues don't imply insecurity. These myths are nothing but blanket statements which aren't true in general.

Use maintained software and observe how its developers handle security vulnerabilities. The rest is about trust as always on the internet.

## Sources
* {{< extlink "https://guidovranken.com/2017/06/21/the-openvpn-post-audit-bug-bonanza/" "Guido Vranken: The OpenVPN post-audit bug bonanza" >}}
* {{< extlink "https://en.wikipedia.org/wiki/Dirty_COW#History" "Dirty COW on Wikipedia" >}}
* {{< extlink "https://en.wikipedia.org/wiki/Heartbleed" "Heartbleed on Wikipedia" >}}

## Changelog
* Dec 7, 2018: Added information about malware in various software repositories.
