+++
title = "There is more than only black and white in information security and privacy"
author = "Benjamin"
date = "2019-08-11T16:03:57+02:00"
tags = [ "privacy", "ethics", "appeals" ]
categories = [ "discussion" ]
ogdescription = "Double standards, pseudo ethics, and black and white thinking won't improve security."
slug = "discussion-filter-bubbles"
banner = "banners/discussion-filter-bubble"
+++

When it comes to information security and privacy, some people specialized in spreading black and white thinking. According to them, there is either a 100% approach to security/privacy or there is no security/privacy at all. This is not only totally wrong but results in a "toxic user mentality".

In this article, we briefly show related problems and the pointlessness of proclaimed behavior rules.
<!--more-->
## Contents
1. [Common black and white statements]({{< relref "#common-black-and-white-statements" >}})
2. [Switching filter bubbles]({{< relref "#switching-filter-bubbles" >}})
3. [The world is colorful]({{< relref "#the-world-is-colorful" >}})
4. [Conclusions]({{< relref "#conclusions" >}})

{{< rssbox >}}

## Common black and white statements
If we look at black and white statements, we mostly see the same declarations:

* "Oh, you are using Microsoft Windows? Seriously? You must use Linux!"
* "Oh, you are using banking apps on your smartphone? Your smartphone is totally insecure!"
* "Oh, you are using DuckDuckGo as a search engine? It is hosted on Amazon's AWS, and privacy-conscious people don't use AWS! (Besides, AWS supports the CIA!!)"
* "Oh, you are using PayPal? This is so evil! You must only pay cash!"
* "Oh, you are using Google/Facebook/Microsoft/(any other US-American company)? They are so evil! How dare you?"
* "Oh, you are using proprietary software? This is so bad for security! Only open source software can be secure since everybody can check the source code!"
* …

Most of these statements only contain a black and white view. There is no gray, no color, no alternative to following the unwritten rules of such people. However, the vast majority of these statements are either far from reality, biased, or come without any consideration of threat models, usability problems, or other things.

No, Linux isn't magically more secure, and no, there is no single Linux for everybody. No, somebody on the internet can't magically hack your smartphone by just entering its IP address in some hacker tool. And while some people claim that they never use Google/Facebook/Microsoft in their life, they are actually using technology invented by people who work for these companies on a daily basis.

If you dig deeper, you even discover more strange things. For instance, a guy, who claims that he—as a privacy-conscious something—would never use DuckDuckGo due to being hosted on AWS (as mentioned above), uses Amazon to buy goods, is member of the Amazon PartnerNet to collect "donations", and recommends other services that are hosted on AWS like the Signal messenger, projects on GitHub, or the Observatory by Mozilla. Another example is a guy claiming that PayPal is extremely bad for privacy, but entering his publicly posted account number on PayPal reveals that his banking account is registered with PayPal. A final example: A person claiming that only open source software/hardware can be secure. At the same time, this person posts about their new proprietary router, their proprietary smartphone, and their proprietary laptop.

So why do such people spread black and white thinking but violate their own rules? We don't know. However, it seems to be a common scheme to make like-minded people happy. The result is oftentimes a new filter bubble that only accepts black and white.

## Switching filter bubbles
The new filter bubble, inflated by black and white thinking of like-minded people, is proclaimed as the only truth. The final step is to convert people, who are unsure about the "right" way to address things, to "the free world". As shown below, they are just switching filter bubbles.

{{< webpimg "/art-img/discussion-filter-bubbles.png" "Sometimes, leaving your alleged filter bubble and entering the 'free world' (as described by your 'savior') means nothing but switching filter bubbles." >}}

Is it more secure and more privacy-friendly to switch from a closed-source instant messenger to an open-source instant messenger that is hosted by unknown people and stores tons of metadata? Is it more secure to host your own Nextcloud instance instead of using a well-known hosting provider? Is your smartphone so insecure that you shouldn't use any banking apps but end-to-end encrypted messengers? Obviously, most of these blanket statements don't make any sense, or there is no clear answer.

## The world is colorful
The important lesson here is that people should stop falling prey to the black and white world that doesn't exist. The world of information security and privacy is colorful. There isn't only one truth. There isn't a "good" and a "bad" way. There is more than only a binary state of secure/insecure or privacy-friendly/not that privacy-friendly.

The same is true for the legal point of view: There is no global law or regulation for information security or privacy. Actually, there are hundreds if not thousands of different laws and regulations. Even the European GDPR, which was introduced to harmonize data protection laws across the European Union, comes with several opening clauses that allow national customization. So even the legal situation isn't black and white.

{{< mastodonbox >}}

## Conclusions
It is very important to talk with everyone, and not only with people who have exactly the same point of view. People who are using services provided by Google/Facebook/Microsoft aren't better or worse than people who host everything on their own. People who are using their smartphone for everything—including online banking—aren't better or worse than people who always pay cash.

Considering that the purists don't even follow their own rules, their statements look like a way of social influencing to make a small target group happy. Then, the hardliners of these groups start expelling everyone who eludes their black and white world. This world is colorful, though.
