+++
title = "Android: AFWall+ brings Tor support"
author = "Thorsten"
date = "2018-11-10T15:00:00+01:00"
tags = [ "android", "afwall", "tor", "firewall" ]
categories = [ "tutorial" ]
ogdescription = "This tutorial shows how you can use Tor over AFWall+."
slug = "afwall-tor"
banner = "banners/afwall-tor"
+++

AFWall+ (_Android Firewall +_) is an open-source, IPtables-based firewall for Android. It requires a rooted Android 5+ device and can be retrieved from F-Droid, GitHub or Google Play Store.

AFWall+ 3.0.0 introduced support for Tor. In this tutorial, we show you how you can configure your device to use this feature.
<!--more-->
## Contents
1. [Installation and preparation]({{< relref "#installation-and-preparation" >}})
2. [Configuration]({{< relref "#configuration" >}})
  * [AFWall+]({{< relref "#afwall-plus" >}})
  * [OpenVPN for Android]({{< relref "#openvpn-for-android" >}})
3. [Troubleshooting]({{< relref "#troubleshooting" >}})
4. [Links]({{< relref "#links" >}})

{{< rssbox >}}

AFWall+ 3.0.0 allows you to redirect all of your device's traffic through Tor using Orbot. The VPN mode of Orbot isn't required anymore. The advantage is that you can use Tor and a VPN connection at the same time. Previously, this wasn't possible since Android only allows one active VPN connection at a time.

Unfortunately, using Tor via AFWall+ isn't as intuitive as usual. If you don't use Orbot so far, stay calm since we introduce this app now.

## Installation and preparation
First of all, download and install [Orbot]({{< relref "#links" >}}). You can get it from [F-Droid]({{< relref "#links" >}}) or Google Play Store.

If you want a permanent Tor connection, you can enable Orbot at startup. This includes restarting your Android device. Keep in mind that background connections quickly drain your device's battery.

Afterwards, download and install "AFWall+" (F-Droid) or "AFWall+ (Android Firewall +)" (Google Play Store). Alternatively, you can get the apk files on GitHub.

Moreover, we recommend "OpenVPN for Android" to manage your VPN connection. This app is available on F-Droid or Google Play Store, too.

## Configuration
There are two different possibilities to configure VPN and Tor:

* Configuration 1: The Tor connection is tunneled through your VPN connection. This must be supported by the VPN server.
* Configuration 2: You use Tor and VPN at the same time, e.g., if you're connected with your home network and your VPN server doesn't support tunneling through your traffic.

### AFWall+ {#afwall-plus}
Open AFWall+ and its menu (three dots in the top right corner). Select preferences. Enable controls for VPN and Tor (see screenshot below).

{{< img "/art-img/afwall-tor-preferences.jpg" "AFWall+ preferences: Rules are active and controls for LAN, VPN and Tor are enabled." >}}

Go back to the main menu (as shown in the screenshot below).

{{< img "/art-img/afwall-tor-main-menu.jpg" "AFWall+ main menu: Columns contain checkboxes for LAN control, WiFi control, mobile data control, VPN control and Tor control. Each row is another app. We are using whitelisting, so 'checked' means 'enabled'." >}}

In the main menu of AFWall+, you can manage permissions of your apps:

#### Orbot
* Configuration 1 as mentioned above: Check WiFi, mobile data and VPN. Since an active VPN connection drains your battery faster, you probably only enable it if necessary. If you always use VPN, it is sufficient to only check VPN (uncheck all other checkboxes).
* Configuration 2  as mentioned above: Check WiFi and mobile data. It is necessary to configure OpenVPN for Android, too. We will show you the required steps below.

#### Apps with built-in Tor support
Some apps come with built-in Tor support, e.g., F-Droid, dandelion*, Tusky and Tor Browser. Only check the "Tor" checkbox.

#### Apps without built-in Tor support
Other apps may not have native support for Tor, however, you can tunnel their traffic through Tor. Check the "Tor" checkbox and either "WiFi" and/or "mobile data". As a result, all traffic of these apps is tunneled through Tor, but only if the smartphone is connected with WiFi resp. mobile data connection.

Configure remaining apps as needed. For instance, if an app should only be allowed to connect to the internet via VPN, only check the "VPN" checkbox.

Don't forget to enable "OpenVPN for Android" rules.

### OpenVPN for Android
Open OpenVPN for Android on your device. You can import configuration files using the "+" button. Normally, OpenVPN tunnels the complete traffic through Android's single VPN slot. If you want to run VPN and Tor at the same time, you must change the configuration:

* Switch to edit mode by selecting the pencil icon
* Select "allowed apps" and enable "VPN is used only for selected apps"
* Check all apps that should be allowed to connect to the internet via VPN

## Troubleshooting
This complex setup can quickly lead to misconfiguration. If you observe strange behavior of apps that use your internet connection, you should use AFWall+'s logging capabilities. AFWall+ can show a notification when it denies apps to connect to the internet. This notification contains the name of the app and the IP address of the remote device. Using these notifications is helpful for troubleshooting.

To enable these notifications, go to preferences in AFWall+ and then to menu preferences log. Enable "Turn on log service" as well as "Enable show toasts".

{{< mastodonbox >}}

## Links
* {{< extlink "https://github.com/ukanth/afwall" "AFWall+ on GitHub" >}}
* {{< extlink "https://github.com/ukanth/afwall/wiki" "AFWall+ Wiki" >}}
* {{< extlink "https://f-droid.org/" "F-Droid" >}}
* {{< extlink "https://guardianproject.info/fdroid" "GuardianProject's F-Droid repository" >}}
* {{< extlink "https://guardianproject.info/apps/orbot" "Orbot" >}}
