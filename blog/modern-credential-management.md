+++
title = "Modern credential management: security tokens, password managers, and a simple spreadsheet"
author = "Jakub"
date = "2019-05-31T13:46:11+02:00"
tags = [ "fido2", "u2f", "password", "webauthn", "2fa" ]
categories = [ "authentication" ]
ogdescription = "If it comes to credential management, define your threat model and keep things simple instead of blindly following tips on the internet."
slug = "modern-credential-management"
banner = "banners/modern-credential-management"
+++

Every year, reports about the worst passwords of the year show up. Every year, all of us see the same worst passwords like "123456", "111111", "querty", or "password", still used by thousands of people. And every year, security people tell us that we must change our passwords regularly, our passwords must have at least n digits, and of course, using biometrics is insecure, short passwords are insecure, using words is insecure, reusing passwords is insecure, and so on.

While countless blogs flood the internet with tips and best practices to keep your passwords "secure" and creating "perfect" passwords, many of them forget about two important things: keeping it simple and defining a threat model.
<!--more-->
## Contents
1. [Basics]({{< relref "#basics" >}})
  * [Something only you have/own]({{< relref "#have" >}})
  * [Behavioral aspects or unique physical characteristics]({{< relref "#be" >}})
  * [Something only you know]({{< relref "#know" >}})
  * [Problems of credential management]({{< relref "#problems" >}})
  * [Threat modeling 101]({{< relref "#tm101" >}})
2. [Good practices for modern credential management]({{< relref "#gp" >}})
  * [Managing something only you have/own]({{< relref "#manage-have" >}})
  * [Managing behavioral aspects or unique physical characteristics]({{< relref "#manage-be" >}})
  * [Managing something only you know]({{< relref "#manage-know" >}})
  * [Combining these things]({{< relref "#mfa" >}})
3. [Summary]({{< relref "#summary" >}})
4. [Links]({{< relref "#links" >}})

{{< rssbox >}}

## Basics
When we talk about passwords, or credentials in general, we have to look at the basic concepts first. As a reader, you likely know the three categories of things (or factors) used for authentication:

* something only you have/own
* a unique behavioral aspect of you/a unique physical characteristic of you
* something only you know

Let's look at several examples and their benefits and drawbacks:

### Something only you have/own {#have}
This category is mainly about things you physically possess (excluding biometrics).

Examples are SIM cards, credit cards, and security tokens. Most of these things are easy to use, and cryptographic keys are oftentimes already implemented, or can be directly generated on the token, which is more secure since it is an isolated environment (keys can't be extracted).

However, things you possess can be lost or stolen. You probably need multiple tokens, credit cards etc. for backup purposes. Some of these things are expensive, and some of them can contain [security vulnerabilities]({{< ref "/glossary.md#vulnerability" >}}). Then, you have to replace them.

Furthermore, security tokens don't solve the problem of secure account recovery.

### Behavioral aspects or unique physical characteristics {#be}
This category is mainly about unique behavioral aspects of you, or unique physical characteristics of you. This is either something only you are or something only you do in a certain way.

Widely-known examples are keystroke dynamics and mouse usage characteristics (both are behavioral aspects of you) as well as your unique physical fingerprints and iris (both are physical characteristics of you). Some companies already started to offer vein authentication, or even heart scans for authentication.

Obviously, the main benefit is that you always carry these things around. They can't be easily lost or stolen, and you can't forget them (at least physical characteristics).

However, some of these things are extremely exposed (like your face and your fingerprints), so attackers could clone them to bypass authentication. Furthermore, using biometrics for authentication raises privacy concerns.

### Something only you know {#know}
This category is mainly about things you keep in mind. This is a very classic approach for authentication.

Examples are PINs, passwords, and passphrases. At first glance, things you know are easy to generate, easy to manage, and you don't need something to generate these things.

However, things you know are hard to remember and hard to use. You can generate weak passwords, and passphrases by choosing personal details like date of births, names, etc. And of course, humans tend to reuse these things over and over again since no one can remember dozens of passwords. This is why we need to talk about common problems of credential management.

### Problems of credential management {#problems}
As you can see, there are different ways to authenticate yourself. There is no perfect method since all of them have drawbacks. Besides, there are common problems of credential management:

* **Expiration:** For many years, password expiration was considered best practice. However, more and more studies show that humans develop ways to cope with the annoying task of changing passwords over and over again. For instance, people use passwords like "s3cur3!maY2019", and six months later, they change it to "s3cur3!novembeR2019". This is obviously not a best practice. It is predictable and an easy-to-spot pattern. Due to this common behavior, security experts started to speak out against password expiration.
* **Recovery:** Passwords can be forgotten, security tokens can be lost, biometrics can change due to injuries. Therefore, many (web) applications implemented backdoors to bypass primary authentication and recover your account. Some applications may allow very weak mechanisms like asking for personal information. Keep in mind that (in the era of social networks) attackers can sometimes easily clear these hurdles.
* **Reuse:** Another problem is that people tend to reuse their password (or parts of it) for multiple accounts. For example, for social network A, somebody uses "s3cur3!maY2019?social" while the password for the e-mail account B is "s3cur3!maY2019?email". The problem: If an attacker recovers the password for one account and identifies the pattern, all other accounts are in danger.
* **Strength:** In terms of credentials, strength is measured in bits of entropy (e.g., strength of passwords and passphrases) or key sizes (e.g., sizes of [GPG]({{< ref "/glossary.md#gnupg" >}}) keys). The "correct" strength totally depends on your threat model: A wannabe hacker with one single laptop won't be very likely able to recover your hashed password while a highly-motivated security agency with thousands of GPUs and human assets will be able to do this. Hence, the discussion about the perfect length or strength is pointless without looking at potential attackers.
* **Storage:** What is the best way to store passwords? Some people use offline password managers like KeePass, others use online password managing services. Then, there are people who write down their passwords. As before, the "best" way depends on your threat model. A private individual can perfectly write down all passwords on a piece of paper and use a physical safe to store it. In office environments, this isn't the preferred method …

So, there are different problems, and discussions about them likely confuse people even more. What is best now? Changing passwords regularly, or keeping your password for years? What is the best length for your password? 8 digits? 10? 20? Does it need special characters? And how to store it? Online or offline? But isn't your password database a single point of failure? We have to look at your threat model.

### Threat modeling 101 {#tm101}
It is important to understand that there is no "secure vs. insecure factors for authentication" as long as you don't define any threat model. Who is your enemy? What do you want to protect?

A very simple example: Look at a medieval castle. A wooden defensive wall may offer sufficient protection against random attacks conducted by small groups. On the other hand, there is insufficient protection against a well-trained army with dozens of ladders. Then, there are thick and high stone walls that can't be efficiently attacked by only using ladders. However, a huge army with trebuchets could easily reduce walls to rubble. And even extremely strong fortifications, which protect against trebuchets, can be useless if the gates of the castle are vulnerable to battering rams.

On the other hand, no lord would spent countless resources to protect the castle against every possible threat on earth. He has to consider possible types of attackers. Of course, this changes over the time. Nowadays, we all know that medieval fortifications don't protect against modern artillery. We have to adapt our defensive measures.

Back to credential management: Your 50-digit password could be useless if it is stored or transmitted in cleartext. It is also useless if there are ways to bypass the password check.

To cut a long story short, you need to think about your opponents. Consider four points:

* Time of the attacker
* Money of the attacker
* Motivation of the attacker
* Skills of the attacker

A highly-motivated and well-trained attacker with an unlimited amount of financial resources and time will always be able to get all your secrets.

A script kiddie (low-skilled attacker) with a small budget and an unlimited amount of time won't be probably able to do any harm, even if you write down all your passwords and store them on your physical or digital desktop.

>Think about your threat model and decide for yourself. In most cases, there is not only black and white in information security. Save your time and money. There will never be 100% security, and you can't even control every part of authentication.

Keep in mind that the real world is much more complicated than only choosing the "right" password length. You must share your secrets with other parties to allow them to identify you in future. The same is true for one-time password algorithms like [OATH-TOTP]({{< ref "/glossary.md#oath-totp" >}}). As soon as authentication relies on a shared secret, the other party also has to manage secrets securely. And this is out of your control. Credential management is mostly a shared responsibility like most topics in information security.

## Good practices for modern credential management {#gp}
After talking about different factors, common problems, and your threat model, we look at activities regarding credential management. The following are activities that reoccur all the time:

1. Generate credentials
2. Store credentials
3. Back up credentials
4. Use credentials
5. Change credentials
6. Delete credentials

Depending on the category of authentication (as discussed at the beginning), there are different things to consider as discussed in the following.

### Managing something only you have/own {#manage-have}
This category includes security tokens and digital keys.

1. Generate credentials: In some cases, security tokens already contain secrets ([FIDO U2F]({{< ref "/glossary.md#u2f" >}})), in other cases, you must generate them (GPG keys). If possible, directly generate your secrets on the token instead of generating them on another machine first.
2. Store credentials: Security tokens, credit cards, SIM cards etc. need physical storage. Store them according to your threat model (e.g., in a safe). Store digital keys (like GPG keys, SSH keys, or certificates) on security tokens, and encrypt them according to your threat model.
3. Back up credentials: You can lose physical tokens, or accidentally delete digital keys. In case of physical tokens (U2F, [W3C WebAuthn]({{< ref "/glossary.md#webauthn" >}})), you can register multiple tokens for backup purposes. Digital keys can be duplicated, or you can even print them out (using tools like "[Paperkey - an OpenPGP key archiver]({{< relref "#links" >}})"). Create multiple backups, if needed. Store backups at different physical locations, if needed.
4. Use credentials: Think about using your credentials. What do you need in this case? Passwords for decryption? PINs? How are they managed?
5. Change credentials: You can accidentally leak your digital keys, or your security tokens can contain security vulnerabilities. Furthermore, algorithms can become weak over time. Then, you have to regenerate secret keys or replace physical tokens/cards.
6. Delete credentials: Think about deleting credentials that you don't use anymore. Simply deleting them may be sufficient to protect against script kiddies while highly-skilled attackers may be able to recover them. The same is true for physical tokens. You may have to physically shred them.

{{< img "/art-img/yubikeys.jpg" "Security tokens like YubiKeys support time-based one-time passwords, U2F and WebAuthn. Such tokens can be used for single- or two-factor authentication. Some can also store GPG and SSH keys." >}}

How do you keep track of your tokens, cards, and keys? We recommend to create a very basic spreadsheet, containing the following columns:

* fingerprint (e.g., of SSH/GPG keys) and/or description (e.g., "My YubiKey 5C")
* algorithm (e.g., RSA 4096 bit, Curve 25519)
* owner (e.g., you, your wife/partner)
* purpose (e.g., used for e-mail encryption/server access)
* linked accounts (e.g., e-mail accounts, user on a server)
* state (e.g., in use, expired, deleted)
* backup locations (e.g., in another room/building)
* creation date (if applicable)
* expiration date (if applicable)

Keep it as simple as possible and use the spreadsheet to keep track of all these things that can't be managed by a password manager.

### Managing behavioral aspects or unique physical characteristics {#manage-be}
This category is about biometrics and behavior.

1. Generate credentials: In most cases, credentials are already there. We don't recommend to adopt any special behavior for authentication since you can forget it.
2. Store credentials: This is quite easy and self-explanatory.
3. Back up credentials: This isn't applicable since you can't easily duplicate your physical fingerprints, or face. The problem here is that something could irretrievably destroy your physical characteristics. For instance, you can lose a finger, which was used for authentication. Hopefully, this never happens, however, you see the point.
4. Use credentials: Using biometrics or behavior for authentication is very easy, in general.
5. Change credentials: This isn't applicable.
6. Delete credentials: This isn't applicable.

Using credentials of category cuts both ways: On the one hand, you get easy-to-use and easy-to-manage authentication. On the other hand, credentials of this category are exposed and can't be easily backed up or changed. And again, it is all about your threat model. A script kiddie can't easily retrieve your fingerprints or clone your face. A highly-motivated attacker with time and money could probably do this, though.

The keep track of these credentials, you should at least write down where you use them. You should also identify how these credentials are processed and stored. For instance, if you use your fingerprint or facial recognition to unlock your smartphone, how does your smartphones process this? Is this only done locally, or are remote systems involved? In other words, is there a risk that a third party collects your physical characteristics like fingerprints?

### Managing something only you know {#manage-know}
The final category is about PINs, passwords and passphrases.

1. Generate credentials: Use a widely-known and proven password manager to generate passwords. The length depends on your threat model.
2. Store credentials: Use a widely-known and proven password manager to store PINs, passwords and passphrases. Use multiple password databases to separate credentials, according to your threat model.
3. Back up credentials: Use tools like rsync, or Borg Backup to back up your password databases and configuration files.
4. Use credentials: Use a widely-known and proven password manager to automatically fill in password fields.
5. Change credentials: Change your PINs, passwords and passphrases according to your threat model. Use the expiration function of your password manager, if needed.
6. Delete credentials: Delete PINs, passwords and passphrases, if you don't need them anymore.

>More importantly, use a password manager to _actually manage your credentials_. A password manager is much more than only an encrypted file containing usernames and passwords.

Install a password manager, create a new database, and create one entry per account to store the following information:

* name of the website, service, or app where you use this entry
* username (this is sometimes your e-mail address)
* e-mail address used for registration and recovery
* unique password, PIN, or passphrase only used for this account
* creation date
* expiration date (if applicable)
* personal data you entered during registration or later (!)

Especially the last point becomes absolutely handy in case of data breaches. Data breaches are oftentimes like "Yeah, they hacked us, but we hashed and salted your password. Change it, and all problems are fixed." However, in many cases most leaked personal data isn't hashed, salted or encrypted. The majority of data is stored and leaked in plaintext. If you make use of your password manager to store information about personal data you entered, you always know which personal data can be or has been potentially leaked.

>The rule of thumb for password strength: One digit longer is always better than one additional special character. So, generate one more digit instead of adding a "!".

For every password database file, create a very simple spreadsheet (or a piece of paper) and keep track of:

* its name
* its owner (if applicable, e.g., you, your wife/partner)
* storage location (e.g., your laptop, external hard drive, or online)
* backup locations (e.g., in another room/building)
* how to unlock it (e.g., security token, passphrase)—manage these credentials according to your threat model

If you use online password managing services, keep in mind that these companies/services can be blocked (by network operators/firewalls), or temporarily/permanently unavailable (due to outages/business closure). Think about backups!

In recent times, there is a trend to get rid off "something you know" and to switch to "something you have/own" (WebAuthn, U2F). Carefully consider these possibilities, and use them if you want.

### Combining these things {#mfa}
Instead of creating a 50-digit password, or just relying on your physical fingerprint, more and more services and products allow you to combine multiple factors, so-called multi-factor authentication (or two-factor authentication if you combine not more than two factors).

Common standards for this are:

* [OATH-TOTP]({{< ref "/glossary.md#oath-totp" >}}): Server and client(s) share the same secret key. Clients use the secret key and the current time to derive a one-time password (OTP). The server does the same and compares the OTP provided by the client with the server-side OTP. Several security tokens (e.g., YubiKey, Nitrokey) or apps (e.g., FreeOTP) allow you to manage TOTP credentials. Some password managers also support managing TOTP credentials, however, using the same database/tool to manage two (normally) different factors somewhat defeats the purpose.
* [FIDO U2F]({{< ref "/glossary.md#u2f" >}}): U2F tokens contain a secret [asymmetric key]({{< ref "/glossary.md#public-key-cryptography" >}}) that can't be extracted. This key is used to [sign]({{< ref "/glossary.md#digital-signature" >}}) a challenge sent by the server. Then, the server checks the response for a valid signature. The benefit is that this scheme only needs server-side storage of public data. All secrets involved are securely embedded in the client-side token, which doesn't need to store anything, hence it supports an unlimited amount of accounts.
* [W3C WebAuthn]({{< ref "/glossary.md#webauthn" >}}): Web Authentication is a (relatively new) W3C recommendation for web applications. Web Authentication supports a wide range of authentication mechanisms, which are based on asymmetric keys. If used for single-factor authentication, WebAuthn clients need to store additional secrets ("Residential Credentials"). Otherwise, clients can be used for two-factor authentication—similar to U2F—without storing additional secrets.

We recommend to combine multiple factors, if possible. This is generally considered more secure than only using one single factor and this is also something you likely do every day: You use a credit card/SIM card (something you have) in combination with a PIN (something you know).

{{< mastodonbox >}}

## Summary
Using SMS-based two-factor authentication or even old-school printed TAN lists for online banking isn't secure or insecure by definition. You always need to consider your own threat model. Your (theoretically ultra secure) 50-digit password can become instantly insecure if a web service stores and leaks it in plaintext. The same is true for a 4096 bit RSA key that was generated using a vulnerable key generator (see "ROCA vulnerability", CVE-2017-15361).

Therefore, define your own threat model and act accordingly instead of blindly implementing "best practices". Only talking about password length and the best password manager is not only insufficient, but also a waste of time. In case of data breaches, the vast majority of your (personal) data is very likely leaked in plaintext. This is totally independent from your credentials.

Combine multiple factors (e.g., password + TOTP, or password + U2F token) instead of generating longer and longer passwords. Use a simple spreadsheet for everything that can't be managed by a password manager. Use a well-known and proven password manager to _actually manage_ (not only store) credentials. Keep in mind that there is no perfect password manager, and no type of factor comes without drawbacks. Use tools to automate authentication, and keep things simple.

## Links
* {{< extlink "https://freeotp.github.io/" "FreeOTP Two-Factor Authentication (Android, iOS)" >}}
* {{< extlink "https://www.yubico.com/store/" "YubiKeys (security tokens supporting OATH-TOTP, U2F, and WebAuthn)" >}}
* {{< extlink "https://shop.nitrokey.com/shop" "Nitrokeys (security tokens supporting OATH-TOTP, and U2F)" >}}
* {{< extlink "https://keepass.info/" "KeePass Password Safe, a well-known and proven password manager" >}}
* {{< extlink "https://www.jabberwocky.com/software/paperkey/" "Paperkey - an OpenPGP key archiver" >}}
