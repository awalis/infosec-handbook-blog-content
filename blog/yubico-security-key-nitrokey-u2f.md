+++
title = "Yubico Security Key vs. Nitrokey FIDO U2F"
author = "Benjamin"
date = "2018-11-10T14:14:14+01:00"
tags = [ "2fa", "password", "yubikey", "nitrokey", "u2f", "fido2", "webauthn" ]
categories = [ "authentication" ]
ogdescription = "The Yubico Security Key and Nitrokey FIDO U2F are affordable solutions to make identity management on the internet less painful."
slug = "yubico-security-key-nitrokey-u2f"
banner = "banners/yubico-security-key-nitrokey-u2f"
+++

[Universal 2nd Factor (U2F)]({{< ref "/glossary.md#u2f" >}}) is an open authentication standard originally developed by Yubico and Google and now hosted by the FIDO Alliance. Security devices with U2F support allow you to use [two-factor authentication]({{< ref "/glossary.md#2fa" >}}) more easily since they contain a secret key that provides a second factor only by pressing the device's button. You don't need to manage more credentials.

We already compared the [YubiKey 4C and Nitrokey Pro]({{< ref "/blog/yubikey4c-nitrokeypro.md" >}}) that offer more features than only U2F. In this article, we compare the Yubico Security Key and Nitrokey FIDO U2F. Both tokens offer similar features and come with support for U2F.
<!--more-->
## Contents
1. [The basic idea of U2F]({{< relref "#u2f-function" >}})
2. [Yubico Security Key and Nitrokey FIDO U2F in comparison]({{< relref "#details" >}})
  * [Yubico Security Key]({{< relref "#yubico-security-key" >}})
  * [Nitrokey FIDO U2F]({{< relref "#nitrokey-fido-u2f" >}})
3. [Benefits]({{< relref "#benefits" >}})
4. [Problems]({{< relref "#problems" >}})
5. [Conclusions]({{< relref "#conclusions" >}})
6. [Sources]({{< relref "#sources" >}})

{{< rssbox >}}

## The basic idea of U2F {#u2f-function}
The registration process consists of the following steps:

1. The web browser checks the identity of the website using its [certificate]({{< ref "/glossary.md#certificate" >}})
2. The identity of the website is sent to the token (by the web browser)
3. The user confirms the operation by pressing the device's button
4. The token generates a [nonce]({{< ref "/glossary.md#nonce" >}})
5. The token creates a [hash value]({{< ref "/glossary.md#hash-function" >}}) using [HMAC]({{< ref "/glossary.md#hmac" >}}) derived from the website's identity, the nonce and the secret key of the token
6. The token derives a unique secret key (and the corresponding public key) from the hash value
7. The token creates a second hash value (checksum) using HMAC derived from the unique secret key, the website's identity and the secret key of the token
8. The token sends the second hash value (checksum), nonce and public key of the newly-generated unique private key to the web browser
9. The web browser sends this data to the website
10. The website stores key handle (nonce + checksum) and public key

Subsequent login attempts require verification consisting of the following steps:

1. You start the login process and may enter credentials like your username and password
2. The website generates a [challenge]({{< ref "/glossary.md#challenge-response-authentication" >}}) (nonce)
3. The website sends challenge and key handle (nonce + checksum) to the token (via the web browser)
4. The token calculates the same private key based on the old nonce, its own secret key and the website's identity
5. The token calculates the same checksum as before and compares it with the one provided by the server
6. If everything is okay, the token cryptographically signs the challenge sent by the server using the unique secret key of the server
7. The token sends its signed response to the web browser
8. The web browser sends the signed response to the website
9. The website verifies the signed response using the public key received during registration

If you look for a more technical and detailed description, see  [Nitrokey's detailed description of U2F]({{< relref "#sources" >}}).

## Yubico Security Key and Nitrokey FIDO U2F in comparison {#details}
YubiKeys as well as Nitrokeys are supported by most common operating systems. You don't have to install additional software to use U2F tokens except a web browser with U2F support. As of November 2018, all major web browser should support U2F.

### Yubico Security Key
YubiKeys are produced by Yubico which was founded in 2007 and is based in the USA and Sweden.

All YubiKeys support U2F. The latest generation (5th generation) also introduced support for [FIDO2]({{< ref "/glossary.md#fido2" >}}) that incorporates the upcoming [W3C WebAuthn API]({{< ref "/glossary.md#webauthn" >}}) and FIDO's Client-to-Authenticator Protocol (CTAP).

In April 2018, Yubico introduced the Security Key that is less expensive than other YubiKeys while only supporting U2F and FIDO2. The physical design is identically equal to USB-A YubiKeys. Like other YubiKeys, it is shipped containing closed-source firmware.

In summary, its features are:

* support for U2F
* support for FIDO2 (including W3C WebAuthn and FIDO CTAP)

Additionally, the YubiKey Manager can be used to set a PIN for FIDO2: `ykman fido set-pin`. Since there are currently no websites with FIDO2 support, we can't test the PIN protection at the moment.

### Nitrokey FIDO U2F
Nitrokeys are produced by Nitrokey UG which was founded in 2015 and is based in Germany. The people behind Nitrokey actually developed a predecessor, called Crypto Stick (2008–2010).

The Nitrokey FIDO U2F is based on the U2F Zero. The differences are another touch button, a smaller printed circuit board and bigger flash for the microcontroller unit.

In November 2018, Nitrokey UG released the Nitrokey FIDO U2F. It is the only Nitrokey with U2F support and doesn't support other technologies like [GnuPG]({{< ref "/glossary.md#gnupg" >}}). In contrast to YubiKeys, you must buy a Nitrokey FIDO U2F and another Nitrokey if you want to use more features (not only U2F or GPG etc.). The physical design of the Nitrokey FIDO U2F is identically equal to the Nitrokey Pro. Like other Nitrokeys, it is shipped containing open-source firmware and open hardware.

In summary, its feature is solely U2F support. There is no support for FIDO2 at the moment.

## Benefits
U2F tokens contain a secret key. This secret key can't be extracted, cryptographic operations are performed by the token. To avoid unwanted operations by the token, users must press the device's button to authorize a single operation. This is far more secure than using apps on mobile devices for [two-factor authentication]({{< ref "/glossary.md#2fa" >}}) (2FA) and more convenient than entering a one-time password. You don't have to store more secrets.

Moreover, web browsers check the identity of websites using their [certificates]({{< ref "/glossary.md#certificate" >}}) and websites have to provide the correct key handle. This offers basic [phishing]({{< ref "/glossary.md#phishing" >}}) protection, because wrong key handles result in wrong checksums. Furthermore, a manipulated web browser or operating system can't access the secret key of the token. They could only log successful login attempts but the challenge involved protects against [replay attacks]({{< ref "/glossary.md#replay-attack" >}}).

The design of U2F [as written above]({{< relref "#u2f-function" >}}) comes with another benefit: Since servers must store key handle and public key, no data has to be written to the token. This means that a single U2F token can be used for an unlimited amount of accounts.

Finally, your token can't be fingerprinted by a website since there is no unique property that is transmitted to the website.

## Problems
Of course, there are some problems:

First of all, U2F requires a client with U2F support. As of today, most major web browsers support U2F. However, if your web browser doesn't support it, you can't use your U2F token. The same is true for server-side support: Many websites don't support U2F, but seemingly U2F becomes more wide-spread.

Secondly, U2F tokens aren't for free. The Yubico Security Key costs about € 18, while the Nitrokey FIDO U2F costs € 22. Given the fact that you can use them for an unlimited amount of accounts, the price is manageable.

Thirdly, if you lose your token, you lose access to your accounts. Some websites require you to also configure [OATH-TOTP]({{< ref "/glossary.md#oath-totp" >}}) as a backup solution. More secure is a second U2F token as a backup token. Most websites should allow you to register multiple U2F tokens.

Fourthly, if someone steals your token, you lose access to your accounts and this person may access your accounts if other credentials are also known to him. During testing, we set a PIN for FIDO2, however, there are no websites with FIDO2 support (as far as we know), so we weren't able to see if this PIN protects the token. We will update this section after successful testing.

Sometimes, U2F secrets are generated by the manufacturer of the security token, then stored on the device and can't be replaced afterwards. In theory, manufacturers could copy your secret U2F key.

{{< mastodonbox >}}

## Conclusions
The main difference is that the hardware/firmware of YubiKeys is closed source while Nitrokeys are based on open hardware/open-source software. Both producers argue convincingly that their philosophy is better. Both U2F tokens are almost identical in size (the Nitrokey is half as high again) and weigh 3 grams (Yubico Security Key) resp. 5 grams (Nitrokey FIDO U2F).

However, the Nitrokey FIDO U2F lacks support for FIDO2 (including WebAuthn) and you need at least two Nitrokeys to use U2F _and_ GPG etc. Moreover, the Yubico Security Key costs less than the Nitrokey FIDO U2F.

## Sources
* {{< extlink "https://www.yubico.com/" "Yubico" >}}
* {{< extlink "https://www.nitrokey.com/" "Nitrokey" >}}
* {{< extlink "https://github.com/Nitrokey/nitrokey-fido-u2f-firmware" "Nitrokey FIDO U2F firmware" >}}
* {{< extlink "https://github.com/Nitrokey/nitrokey-fido-u2f-firmware/blob/master/doc/security_architecture.md" "Nitrokey's detailed description of U2F" >}}
* {{< extlink "https://www.dongleauth.info/" "List of websites and their OTP/U2F support" >}}
