+++
title = "Monthly review – September 2019"
author = "Benjamin"
date = "2019-09-30T19:16:13+02:00"
tags = [ "simjacker", "wibattack", "pdfex", "signal", "privacytools" ]
categories = [ "Monthly review" ]
ogdescription = "Our monthly review of September 2019."
slug = "2019-9-monthly-review"
banner = "banners/monthly-review"
+++

Each month, we publish a review that covers the most important activities of the last 30 days. This month, we talk about Simjacker/WIBattack, PDFex, signal-cli, privacytools.io and more.
<!--more-->
## Contents
1. [News of the month]({{< relref "#notm" >}})
2. [Tool of the month]({{< relref "#toolotm" >}})
3. [Tip of the month]({{< relref "#tipotm" >}})
4. [Readers' questions of the month]({{< relref "#rqotm" >}})
5. [Our activities of the month]({{< relref "#ishotm" >}})
6. [Closing words]({{< relref "#cw" >}})
7. [Links]({{< relref "#links" >}})

{{< rssbox >}}

## News of the month {#notm}
In September 2019, two major [security vulnerabilities]({{< ref "/glossary.md#vulnerability" >}}) were disclosed:

1. The [Simjacker attack and WIBattack]({{< relref "#links" >}}) both rely on software on the SIM card of targeted phones. These attacks can be used to locate phones, or retrieve information about them (IMEI, battery, network, language). The problem here is that such software on SIM cards is 100% controlled by the mobile network operator, and can be transparently installed, uninstalled, enabled, or disabled by mobile network operators. This means that the owner of a SIM card can't control such software, or spot these changes. However, these attacks rely on special commands that are sent to the SIM card of the victim. There seem to be some apps that are able to detect such SMS like [SnoopSnitch]({{< relref "#links" >}}). Then, there is [SIMtester]({{< relref "#links" >}}), an app that can detect various security vulnerabilities of SIM cards. Kindly note that these apps are only for detection. Blocking these commands can also be done by mobile network operators. In summary, such attacks clearly demonstrate that you can't "take back control" of your phone by just installing a custom operating system on your device. Such attacks will remain possible due to proprietary chips that can't be controlled by the phone's operating system (e.g., Android, iOS). Even a smartphone that comes with 100% open hardware relies on proprietary SIM cards.
2. [PDFex]({{< relref "#links" >}}) demonstrates several attacks on (legacy) PDF encryption. On the one hand, an attacker can manipulate parts of an encrypted PDF file without knowing the password used for encryption. On the other hand, the insecure Cipher Block Chaining (CBC) encryption mode allows ciphertext malleability since there are no integrity checks. Researchers showed that 23 out of 27 tested PDF viewers were vulnerable to these attacks. Furthermore, research suggests that PDF signing is also vulnerable to several attacks, allowing attackers to change contents of signed PDF files without invalidating the signatures.

Moreover, there were some data breaches. Have I Been Pwned added information about the following breaches:

* XKCD (breached in July 2019)
* Mastercard Priceless Specials (breached in August 2019)
* Poshmark (breached in mid-2018)
* void.to (breached in June 2019)
* Minehut (breached in May 2019)
* KiwiFarms (breached in September 2019)
* Lumin PDF (breached in April 2019)
* Wanelo (breached in December 2018)

Check if you were affected, and change your credentials. Besides, feel free to [subscribe to our RSS/Atom feed](https://mastodon.at/users/infosechandbook.atom), or directly [follow us in the Fediverse](https://mastodon.at/users/infosechandbook/remote_follow) to learn about data breaches and much more.

## Tool of the month {#toolotm}
This month, we present [signal-cli]({{< relref "#links" >}}), a Java-based CLI tool. signal-cli can be used as a Signal messenger client in the terminal. In an upcoming article, we will write about using signal-cli to notify server admins about server-side security events. In the following, we briefly introduce basic configuration of signal-cli.

First of all, you need to download and install the tool. The official guide on GitHub suggests different possibilities. Arch Linux users can use the AUR package. There is also a guide for Ubuntu users. signal-cli requires at least Java Runtime Environment (JRE) 7 installed.

After installing the tool, you need to register a phone number. You don't need to register it on your phone beforehand.

The command for this step is: `signal-cli -u [phone-number] register`. This command triggers Signal to send a verification SMS to the phone number. So you must be able to receive the SMS on a phone, or via SMS service on the internet. If you can't receive SMS, use `signal-cli -u [phone-number] register --voice`.

Then, you need to verify your phone number by entering: `signal-cli -u [phone-number] verify [verification-code]`. After this step, you can immediately use your phone number with the CLI tool.

However, you should always set a Registration Lock PIN. This PIN is required if somebody tries to re-register your phone number for Signal. The command for the step is: `signal-cli -u [phone-number] setPin [registration-lock-pin]`. The PIN can consist of up to 20 characters. Store it securely, e.g., in an encrypted password database.

To send your first message, type: `signal-cli -u [phone-number] send -m "[message]" [phone-number-recipient]`. You second device should receive the message.

The final step is to establish verified trust between both phone numbers (devices). Type `signal-cli -u [phone-number] listIdentities`. The output should be similar to "[phone-number-recipient]: **TRUSTED_UNVERIFIED** Added: Wed Sep 18 12:56:44 CEST 2019 Fingerprint: [fingerprint]  Safety Number: [safety-number]". As you can see, this identity is marked as "TRUSTED_UNVERIFIED".

Retrieve the safety number of the second device/phone number, and compare it. If both numbers match, type `signal-cli -u [phone-number] trust -v [fingerprint] [phone-number-recipient]`. Note that you enter the "[fingerprint]" of the other device. You don't need to enter the "[safety-number]".

Recheck your setup by entering `signal-cli -u [phone-number] listIdentities` again. Now, the output should be similar to "[phone-number-recipient]: **TRUSTED_VERIFIED** Added: Wed Sep 18 12:56:44 CEST 2019 Fingerprint: [fingerprint]  Safety Number: [safety-number]". Finally, mark the safety number as "verified" on the other device.

In our opinion, signal-cli is a simple tool for sending alerts and notifications. Note that JRE is required. In some cases, you don't want to install additional systems on servers. Do you have other use cases? Feel free to write about it to help other people.

## Tip of the month {#tipotm}
This month, our tip is about a [privacytools.io]({{< relref "#links" >}}). Privacytools.io is focused on providing privacy-oriented alternatives to well-known apps and services. Additionally, they are hosting some services like Gitea, Mastodon, Matrix, Searx, and PrivateBin. There is also a [forum]({{< relref "#links" >}}) (we are an active member). Unlike other websites, which recommend alternatives, recommendations on privacytools.io are based on community discussions and feedback. So everybody can help improving recommendations and share their opinions.

We think privacytools.io is a place of open-minded people. Feel free to check their websites if you are interested in sharing some privacy tips or discuss it. (As always, no sponsoring involved! 😉)

## Readers' questions of the month {#rqotm}
Each month, readers send us questions via e-mail, Keybase, Mastodon (Fediverse), Threema, Signal, or via the forum of privacytools.io. In general, we directly reply to questions. However, we would like to list some questions and answers that are interesting for more than only one person:

* "Is infosec-handbook.eu a reliable source for Wikipedia?": This depends on the edition of Wikipedia since the rules for "reliable sources" differ. In case of the English edition, infosec-handbook.eu (and most other personal blogs) doesn't meet the requirements of a primary source. While we always try to provide high-quality content, our content can't be used as the sole source for Wikipedia. In our opinion, this is actually good. Wikipedia (as an online encyclopedia) should contain only information that was reviewed by many different people. On the other hand, personal blogs aren't encyclopedias.
* "Which software do you use for your blog?": We use different applications for different purposes. The core of our blog is Hugo, a static website generator (see [this article for more information]({{< ref "/blog/static-blogging.md" >}})). Hugo takes different files as input and produces our blog as output. Then, we use OpenSSH and rsync to upload all files of our blog to our server. Our server runs Nginx (web server) to serve our content. Besides, we use Git to back up our content, and GPG to sign each commit. Other tools are pngcrush and ImageMagick (image compression), and minify (content compression). Of course, there are more tools involved like Inkscape (drawing icons), or Atom (editor). The big advantage is less server-side software. For instance, we don't use content management systems like WordPress or PHP, drastically improving security of our blog.
* "Do you plan to implement 'OpenPGP Web Key Directory' for your OpenPGP keys?": No, not at the moment since it is still an informational draft, not a standard or recommendation. Then, the vast majority of readers doesn't send us encrypted e-mails (even companies, which sell OpenPGP products, didn't manage sending encrypted e-mails), or contact us via other encrypted channels that don't make use of our OpenPGP keys.
* "Will you publish content in other languages, e.g., in German?": No, this isn't planned at the moment. Publishing content in multiple languages makes updating content much more time-consuming.

Just send us your questions. Maybe, your question will be listed in the next monthly review.

## Our activities of the month {#ishotm}
In September, we published only one new article since most of us were on vacation:

* [GnuPG for e-mail encryption and signing]({{< ref "/blog/gpg-for-emails.md" >}}): In this article, we show recent security vulnerabilities in OpenPGP/GPG. Furthermore, we talk about the basic workflow when using GPG for e-mail encryption, and show several alternatives and their use cases.

Besides, we split the main Git repository of our blog, and [published the Git repo]({{< relref "#links" >}}) that contains the content of our blog. This means that you can see every single change of our articles on GitHub now. We did this mainly for transparency reasons, however, readers with a GitHub account can also use this way to report issues, or suggestions for improvements.

Moreover, we tweaked the CSS style of the blog a little bit, and added [additional ways for contacting us]({{< ref "/contact-details.md" >}}).

{{< mastodonbox >}}

## Closing words {#cw}
In October, we will proceed revising our [Web server security series]({{< ref "/as-wss.md" >}}) as announced last month. This is a more complex process that takes some time. Our goal is splitting general content and software-specific content for better maintainability.

Then, it is already October. Every year, October is the European Cyber Security Month (ECSM). As announced on some ECSM-related websites, we will attend by publishing several shorter articles that address topics of this year's ECSM.

## Links
* Simjacker attack: {{< extlink "https://www.adaptivemobile.com/blog/simjacker-next-generation-spying-over-mobile" "Simjacker – Next Generation Spying Over Mobile" >}}
* WIBattack: {{< extlink "https://ginnoslab.org/2019/09/21/wibattack-vulnerability-in-wib-sim-browser-can-let-attackers-globally-take-control-of-hundreds-of-millions-of-the-victim-mobile-phones-worldwide-to-make-a-phone-call-send-sms-to-any-phone-numbers/" "WIBattack: Vulnerability in WIB sim-browser" >}}
* Simjacker/WIBattack – Simjacker/WIBattack – protection tools now available: {{< extlink "https://srlabs.de/bites/sim_attacks_demystified/" "New SIM attacks de-mystified, protection tools now available" >}}
* {{< extlink "https://opensource.srlabs.de/projects/snoopsnitch" "SnoopSnitch" >}}
* {{< extlink "https://opensource.srlabs.de/projects/simtester" "SIMtester" >}}
* {{< extlink "https://pdf-insecurity.org/" "PDFex: how to break PDF encryption" >}}, and {{< extlink "https://pdf-insecurity.org/encryption/evaluation_2019.html" "PDFex: Evaluation of PDF viewers" >}}
* {{< extlink "https://github.com/AsamK/signal-cli" "signal-cli" >}}
* {{< extlink "https://www.privacytools.io" "privacytools.io" >}} ({{< extlink "http://privacy2zbidut4m4jyj3ksdqidzkw3uoip2vhvhbvwxbqux5xy5obyd.onion" "via Tor Browser" >}}), {{< extlink "https://forum.privacytools.io/" "privacytools.io – forum" >}}
* {{< extlink "https://github.com/infosec-handbook/blog-content" "Content of infosec-handbook.eu on GitHub" >}}
