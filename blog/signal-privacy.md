+++
title = "How to use Signal more privacy-friendly"
author = "Benjamin"
date = "2018-04-21T09:02:36+02:00"
lastmod = "2019-04-21"
tags = [ "metadata", "privacy", "signal" ]
categories = [ "privacy" ]
ogdescription = "When it comes to security, Signal is one of the best messengers available. We show you some additional privacy tips."
slug = "signal-privacy"
banner = "banners/signal-privacy"
+++

Signal is one of the most secure messengers available, making it practically impossible for server operators or other [men-in-the-middle]({{< ref "/glossary.md#man-in-the-middle-attack" >}}) to decrypt your conversations, learn about your group memberships, or spy on you in another way.

However, some people raise privacy concerns due to Signal's need for an arbitrary phone number. This time, we show you how to use Signal even more privacy-friendly.
<!--more-->
## Contents
1. [Setup]({{< relref "#setup" >}})
2. [Configuration]({{< relref "#configuration" >}})
3. [Operation]({{< relref "#operation" >}})
4. [Conclusions]({{< relref "#conclusions" >}})
5. [Sources]({{< relref "#sources" >}})
6. [Changelog]({{< relref "#changelog" >}})

{{< rssbox >}}

## Setup
First of all, the Signal client is available for Android or iPhone.¹ You may have concerns about using your (insecure) phone to run Signal.  However, you can actually install Android using a virtual machine (e.g., VirtualBox), download the Signal APK from signal.org, and install Signal in this virtual environment. This renders some features of Signal useless, of course, since the VM can't simulate all the hardware of your real phone.

On the other hand, the absence of most sensors and chips of phones and stricter control about your VM is more secure than using an unpatched and outdated Android device.

We installed Signal 4.18.3 on Android x86, using Oracle's VirtualBox. As we showed you before, there is [no need to let Signal access your contacts]({{< ref "/blog/myths-signal.md#m1" >}}) and [you don't have to register your cellphone number]({{< ref "/blog/myths-signal.md#m2" >}}) in order to use it. Our freshly installed Android x86 didn't contain any contacts and we only permitted Signal to access our storage.

But, but, but … What about the arbitrary phone number? You can either …

* buy a SIM card in countries which don't need you to disclose your identity and only use the SIM card to register Signal (for the paranoid: Use a burner phone during this registration process!), or
* register a VoIP number and only use it to register Signal, or
* use one of many free online services which allow you to receive SMS for free (for the paranoid: Access their websites via Tor Browser).

We successfully registered Canadian and Russian phone numbers during multiple tests. Furthermore, we accessed the SMS service via the Tor Browser making it impossible to connect our real IP address with the usage of the virtual phone number.

Registration is fairly simple: Enter the virtual phone number in Signal, wait for a six-digit number (via SMS), enter it, done. Stop! What about others who can also access these free services? They could try to re-register your virtual phone number … After registration, go to "Settings" → "Privacy" → "Registration Lock PIN" and set a PIN (up to 20 digits, representing 10<sup>20</sup> combinations or 66 bits of entropy). Signal will ask you at fixed intervals after your PIN is set to enter your PIN again helping you memorizing it.

¹_Yes, there is also Signal Desktop but you need to connect it with a mobile Signal client!_

## Configuration
You installed Signal and you set your Registration Lock PIN. Now, you can change the default configuration of Signal to make it more secure and privacy-friendly:

* Picture and name: This feature is optional. If you use this feature, this information is encrypted and will be send to new contacts when you contact them for the first time. When you enter groups, you have to manually confirm that you want to show this information to others. Signal servers don't learn about your picture or name.
* SMS and MMS: In the past, Signal offered encrypted SMS additionally to encrypted online chat, and encrypted online calls. In 2015, Signal developers decided to drop the SMS encryption feature. Nowadays, Signal still has the ability to send/receive SMS, however, they are always in cleartext. We recommend to **not** use Signal as your default SMS app since you can get confused, and accidentally send cleartext SMS to someone while expecting to send an end-to-end encrypted message. Besides, SMS produce tons of [metadata]({{< ref "/glossary.md#metadata" >}}). Use a dedicated app for insecure SMS.
* Privacy: We recommend to
  * set screen lock (locks Signal using your Android screen lock or fingerprint)
  * set screen security (screenshots will be blocked)
  * set incognito keyboard (avoids that your keyboard app stores your conversations due to personalization)
  * disable receipts (so others can't see if you read their messages)
  * [4.30.7] display indicators when "sealed sender" was used
  * [4.30.7] **not** enable "allow from anyone" to prevent abuse
  * [4.31.6] **not** enable "Typing indicators"
  * [4.33.x] **not** enable "Send link previews"
* Disappearing messages: Signal allows you to set disappearing message timers per contact/group. This timer starts immediately after a message has been sent (sender) resp. after a message has been read (recipient).

Signal doesn't disclose your online status by default (your contacts can't see whether you are online at the moment or the last time you were online). However, Signal shows one check mark (in a circle) when your message was sent to a Signal server and two check marks (in two circles) when a message has been delivered to the device of the recipient. If receipts are enabled, the check marks will be in filled circles after the recipient opened the conversation.

## Operation
After setup and configuration, you can use Signal in day-to-day life. However, you should …

* verify all of your contacts to avoid [man-in-the-middle attacks]({{< ref "/glossary.md#man-in-the-middle-attack" >}}) (open a conversation with one of your contacts → tap on the menu icon → open "Conversation settings" → open "View safety number"). Use an external channel (not Signal!) when you compare your numbers and ensure that you are really talking with your contact (not with a man-in-the-middle).
* keep in mind that your Registration Lock PIN expires after 7 days of inactivity. "Inactivity" means that no Signal clients of this account are connected to the internet. This timer is immediately reset when your Signal client connects to Signal servers.
* never forget that there is no 100% security. For instance, if somebody infects your device with [malware]({{< ref "/glossary.md#malware" >}}), he could easily read all of your messages and wiretap your Signal calls. Furthermore, someone could steal your device, and law enforcement could seize it. Enforced [end-to-end encryption]({{< ref "/glossary.md#end-to-end-encryption" >}}) only ensures that the entire connection between your device and the device of the recipient is secure.

{{< mastodonbox >}}

## Conclusions
Signal is already really secure and privacy-friendly by default, however, you can improve this by using a strict configuration and a phone number which can't be connected with your real identity. You don't need a phone or SIM card to use Signal and Signal doesn't need to access your contacts!

## Sources
* {{< extlink "http://www.android-x86.org/" "Android x86" >}}
* {{< extlink "https://signal.org/android/apk/" "Signal APK download" >}}
* {{< extlink "https://support.signal.org/hc/en-us/articles/360001842431-What-is-Registration-Lock-" "Signal Registration Lock" >}}
* {{< extlink "https://support.signal.org/hc/en-us/articles/212535538-How-do-I-know-if-a-message-has-been-delivered-or-read-" "Signal Check marks" >}}

## Changelog
* Apr 21, 2019: Clarified information about Signal's SMS feature. Thanks to @se7en via Pleroma. Added "link previews". Removed "Silence" app.
* Dec 16, 2018: Added recommendation for "typing indicator".
* Nov 19, 2018: Added information about "sealed sender".
