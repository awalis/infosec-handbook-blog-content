+++
title = "Signal messenger myths"
author = "Benjamin"
date = "2018-03-23T04:21:17+01:00"
lastmod = "2018-10-31"
tags = [ "signal" ]
categories = [ "myths" ]
ogdescription = "Signal is one of the best end-to-end encrypted communication apps but there are some myths in circulation."
slug = "signal-myths"
banner = "banners/signal-myths"
+++

When it comes to security, Signal is one of the best [end-to-end encrypted]({{< ref "/glossary.md#end-to-end-encryption" >}}) communication apps. There is no unencrypted fallback and servers store very little [metadata]({{< ref "/glossary.md#metadata" >}}). However, there are some myths about Signal rattling around, mostly spread by people who never used Signal before or who want to promote their favored communication apps.

In this article, we discuss three myths about Signal and show why they are wrong.
<!--more-->
## Contents
1. [Myth 1: Signal needs access to your contacts]({{< relref "#m1" >}})
2. [Myth 2: You have to disclose your cellphone number to use Signal]({{< relref "#m2" >}})
3. [Myth 3: Signal is just like WhatsApp]({{< relref "#m3" >}})
4. [Further reading]({{< relref "#fr" >}})
5. [Sources]({{< relref "#sources" >}})
6. [Changelog]({{< relref "#changelog" >}})

{{< rssbox >}}

## Myth 1: Signal needs access to your contacts {#m1}
**Wrong**. We installed Signal on an Android (LineageOS 14.1) device which contained 0 contacts and no SIM card. Additionally, Signal didn't get permission to access contacts or storage. After initial setup, we were able to send messages to other Signal users or call them by manually entering their registered phone number.

Thus, Signal doesn't need access to your contacts. Signal works perfectly fine without accessing your contacts, however, you have to manually enter phone numbers of your friends when you contact them for the first time. When your Signal contacts answer, Signal shows profile data (display name + picture) if available for the corresponding phone number. This feature is called Signal Profiles and was introduced in September 2017. All profile data is encrypted and stored client-side. Signal servers don't get to know this data and you don't have to enter it if you don't want to use it.

There is another drawback: When Signal can't access your contacts, it can't notify you that one of your contacts became a Signal user. This is because Signal clients periodically send truncated hashes of the phone numbers of your contacts to Signal servers. Signal servers already know whether a phone number is registered or not. They respond by saying "Yes, this phone number is a registered Signal user" or the opposite. Without accessing your contacts, this process is impossible.

## Myth 2: You have to disclose your cellphone number to use Signal {#m2}
**Wrong**. We often read comments complaining about the fact that you have to register a phone number in order to use Signal. People argue that using phone numbers as identifiers isn't privacy-friendly because you can be easily identified by a third party and you have to disclose your personal cellphone number, so others can communicate with you.

Actually, you don't have to register your cellphone number. You can register another arbitrary phone number, but you must be able to confirm that you have access to this phone number during initial setup. Furthermore, you should retain control of this phone number and specify a "Registration Lock PIN" so that others can't reset or unregister your account.

## Myth 3: Signal is just like WhatsApp {#m3}
**Wrong**. Signal developers went to great lengths to fully implement modern end-to-end encryption. Servers only store very little metadata like previously mentioned. For instance, the "sealed sender" feature hides the identity of message senders, so Signal servers don't learn about the sender of a message. Client implementations and server code are open source. Signal shows prominent security warnings if the safety number of a contact changed. Currently, Signal developers examine new privacy-enhancing features based on Intel Software Guard Extensions (SGX) and work on additional resistance to traffic correlation via timing attacks and IP addresses.

On the other hand, WhatsApp also offers Signal's end-to-end encryption. However, it collects much more metadata and there is the widely discussed data exchange with Facebook. Additionally, WhatsApp isn't fully open source and allows storing (unencrypted) backups in the cloud.

Of course, there are more differences between the messengers. You can clearly see that Signal isn't just another WhatsApp.

{{< mastodonbox >}}

## Further reading {#fr}
We published another article to show you how you can use Signal even more privacy-friendly: [How to use Signal more privacy-friendly]({{< ref "/signal-privacy.md" >}}).

## Sources
* {{< extlink "https://signal.org/" "signal.org" >}}

## Changelog
* Oct 31, 2018: Added information about "sealed sender".
* Apr 3, 2018: Added Registration Lock PIN (> 4.17.5)
