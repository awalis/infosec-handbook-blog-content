+++
title = "How to verify encryption keys for instant messaging"
author = "Thorsten"
date = "2018-07-28T10:15:01+02:00"
tags = [ "keybase", "encryption", "verification", "xmpp", "matrix" ]
categories = [ "tutorial" ]
ogdescription = "This tutorial shows how you can easily verify encryption keys of your friends and publish your ones signed."
slug = "keybase-verification"
banner = "banners/keybase-verification"
+++

Nowadays, we are using encryption on a daily basis. For instance, many instant messaging clients offer encryption, some good ones enforce it. However, one really important aspect of encryption is that you also verify the identity (keys) of your chat partners.

In this article, I show you how you can easily verify encryption keys of your friends and publish your ones using Keybase.
<!--more-->
## Contents
1. [The need for verification]({{< relref "#need-verification" >}})
2. [Online verification using Keybase]({{< relref "#keybase" >}})
3. [Conclusions]({{< relref "#conclusions" >}})
4. [Links]({{< relref "#links" >}})

{{< rssbox >}}

I describe people who give thought to privacy and information security as "privacy community"—especially in the digital world. I class me with this community. Instead of using mainstream software, we use free and open-source software (FOSS) alternatives. We don't trust big tech companies like Google, Microsoft, Apple or Amazon which offer proprietary software for free. "For free" mostly means that they get our personal data in return.

However, when it comes to instant messaging, opinions are divided. We agree that WhatsApp, Facebook Messenger, Skype and others shouldn't be used by us, but we argue about alternatives.  I always try to point to the basics when I take an active part in such arguments since some people seem to lose sight of them.

Nowadays, every instant messaging client should at least offer verifiable end-to-end encryption using state-of-the-art and open-source encryption algorithms. However, not only software is important for [confidential]({{< ref "/glossary.md#confidentiality" >}}) conversations, but also its users. You must keep in mind that you have to always check if you are really talking to your chat partner.

## The need for verification {#need-verification}
Imagine a one-to-one on the internet. It's like talking to someone at a well-visited party: Everyone can hear you talking. So, both of you go to an empty and bugproof room. This is comparable to an end-to-end encrypted conversation. Since you can see your dialog partner, you can be sure that you really talk to him or her. On the internet, this is more difficult.

To ensure [authenticity]({{< ref "/glossary.md#authenticity" >}}), all involved devices must be identifiable. There must be some kind of proof on each device, so you can check that this is your chat partner's device. The way this is technically implemented varies depending on the encryption algorithm, however, the concept and result mostly remain the same. I would like to explain this based on chat systems which support multiple clients (like XMPP with [OMEMO]({{< ref "/glossary.md#omemo" >}}) or Matrix with Olm), since these systems are popular among the privacy community.

Every device has its own key pair used for encryption: a public key and a private key. Encryption algorithms which don't support [perfect forward secrecy]({{< ref "/glossary.md#perfect-forward-secrecy" >}}) (PFS) directly use these keys for encryption and decryption, while modern encryption algorithms with PFS support use a more complex system for these tasks.

Whatever encryption algorithm your chat client uses, it is likely that it shows you one or more so-called [fingerprints]({{< ref "/glossary.md#fingerprint" >}}) which are based on your key pairs. They are comparable with your own physical fingerprints: They are unique and distinct. They allow you to identify a certain device.

As soon as you start to communicate with your chat partner, devices get the fingerprints of other devices (or accounts) which participate in this chat. This is some kind of "blind trust" since devices never "met" before and can't be sure that fingerprints really belong to the parties which pretend to own them. The important term here is the [man-in-the-middle attack]({{< ref "/glossary.md#man-in-the-middle-attack" >}}).

The logical consequence is to compare the fingerprints you automatically got with the fingerprints shown by the devices of your chat partner. This requires a secure channel to ensure that you compare the real fingerprints. Two examples here: Imagine your friend published his key's fingerprints on Facebook. You compare the fingerprints with the ones which are shown by your own device. Can you be sure that these are the real fingerprints? Maybe someone changed them on Facebook. Maybe your internet connection isn't secure and someone exchanges fingerprints on the fly. In another scenario, you use the new chat you just started to send your fingerprint to your chat partner. This is also insecure since the authenticity of your chat partner hasn't been verified before.

Of course, the most secure variant of verification is offline verification: You meet your chat partner in real life and compare your fingerprints. There is no internet connection involved and you know that this is your chat buddy. Nowadays, this variant becomes more and more unrealistic: What about your friend overseas? Or your former neighbor who just moved out of town? Do you want to spend time and money just to compare some digital fingerprints?

An easy and relatively secure method to verify fingerprints is Keybase.

## Online verification using Keybase {#keybase}
Keybase was once a simple [GPG]({{< ref "/glossary.md#gnupg" >}}) wrapper to allow non-technical people to use GPG more easily. Nowadays, it is some kind of security software with interesting features for developers. Most platforms like Windows, Linux, Android etc. are supported. You can get it at no charge and it's open-source software. Some features are:

* **Key management**: You can upload your GPG keys and use the Keybase infrastructure as a key server
* **Identity management**: You can connect different online accounts and add cryptographic proof, so your friends can verify that these accounts belong to you
* **Encrypted chat**: You can chat with other people on Keybase (1-to-1 and group chats)
* **Encrypted file system**: You can exchange or backup files securely
* _and more_

We will introduce some of Keybase's features in upcoming articles. Let's return to our topic: Securely verifying the fingerprints of devices. For this purpose the encrypted file system comes in handy.

After installation, Linux users find the new folder /keybase/, while Windows users directly see the new Keybase folder in the Windows Explorer. The structure of this folder is simple:

| Folder name | Purpose                    | Mode               | Accessible by         |
|-------------|----------------------------|--------------------|-----------------------|
| private/    | file backup, file exchange | encrypted & signed | you or you/one person |
| public/     | file distribution          | signed             | everyone              |
| team/       | teamwork, file exchange    | encrypted & signed | all team members      |

We use the "public" folder since fingerprints are no secrets. If you don't want to tell everybody that you use a particular messenger, you can also use teams for family members or only use Keybase to sign your fingerprints and then directly send them to your friend.

I structured my folders this way:

{{< webpimg "/art-img/keybase-verification-tree.png" "My folder tree in Keybase." >}}

Save your OMEMO fingerprints in "omemo-fingerprint-verification.txt". You can also store image files which contain QR codes (Conversations, Gajim). Now use your social media accounts to link to these files. For example, my Diaspora account displays:

<p align="center">
Matrix: @l18r3j0k3r:disroot.org |
<a href="https://keybase.pub/librejoker/e2ee-verification/xmpp/omemo-fingerprint-verification.txt">Verify my fingerprints</a>
</p>

If you click on the link, you see:

{{< webpimg "/art-img/keybase-verification-txt.png" "My verification file for Matrix." >}}

As you can see, this txt file is embedded in HTML and signed by one of my devices. You can directly check that this is the original file which wasn't replaced or modified by a third party.

Another benefit is that you only have to change your files in one place if you switch devices and get new fingerprints.

{{< mastodonbox >}}

## Conclusions
It is really easy to use Keybase's file system and you get an easy solution for exchanging and comparing fingerprints of your chat partners.

Keybase offers far more interesting features, of course, that we will show you in upcoming articles.

## Links
* {{< extlink "https://keybase.io" "Keybase" >}}
* {{< extlink "https://keybase.io/download" "Keybase downloads" >}}
