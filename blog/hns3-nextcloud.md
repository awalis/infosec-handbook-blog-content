+++
title = "Home network security – Part 3b: Turris Omnia as network-attached storage via Nextcloud"
author = "Benjamin"
date = "2018-08-11T07:46:17+02:00"
tags = [ "turris-omnia", "router", "lan", "nas", "privacy", "nextcloud" ]
categories = [ "Home network security" ]
ogdescription = "In this part, we show you how you can use your Turris Omnia as network-attached storage via Nextcloud."
slug = "hns3-nextcloud"
banner = "banners/as-hns"
+++

In this part of our [home network security series]({{< ref "/as-hns.md" >}}), we will show you how you can use your Turris Omnia as network-attached storage (NAS) via Nextcloud. This is very useful since your data is kept within your home network. You can share files with your family members and other devices at home. There is no need for flash drives or online storage.

Compared with [NAS via SMB]({{< ref "/blog/hns3-nas.md" >}}), this option is easier to implement, however, the RAM usage is higher.
<!--more-->
## Contents
1. [Requirements]({{< relref "#requirements" >}})
2. [Step by step to your NAS using Nextcloud]({{< relref "#sbs-guide" >}})
  * [Step 1: Format and set the mSATA]({{< relref "#s1" >}})
  * [Step 2: Download Nextcloud using LuCI]({{< relref "#s2" >}})
  * [Step 3: Install Nextcloud using SSH]({{< relref "#s3" >}})
  * [Step 4: Secure the database]({{< relref "#s4" >}})
  * [Step 5: Open Nextcloud in your web browser]({{< relref "#s5" >}})
3. [Tips]({{< relref "#tips" >}})
4. [Summary]({{< relref "#summary" >}})
5. [Sources]({{< relref "#sources" >}})

{{< rssbox >}}

## Requirements
For this guide we need:

* our Turris Omnia which is connected with our computer and the internet
* an installed and empty mSATA drive (see [part 3a for installation instructions]({{< ref "/blog/hns3-nas.md#s1" >}}))
* an SSH client on our computer

## Step by step to your NAS using Nextcloud {#sbs-guide}
This time, we use the "Storage" module of Foris OS to format the mSATA drive and then install Nextcloud via LuCI and SSH.

### Step 1: Format and set the mSATA {#s1}
First of all, connect to your Turris Omnia using your web browser (e.g., https://192.168.1.1/foris/config/). Log in and go to "Storage".

There should be a red warning message like "Device currently in use is … (internal flash)". Your mSATA drive should be selectable below (e.g., "sda").

Select your device and click on "Format & Set". After confirming this choice, your Omnia will format and set your drive and then reboot. Log in again and check whether your drive is in use now (see image below). The file system is "btrfs" this time.

{{< webpimg "/art-img/as-hns3-5storage.png" "Our mSATA drive after formatting and setting is as /srv location." >}}

### Step 2: Download Nextcloud using LuCI {#s2}
Now, we must download the Nextcloud packages. This option isn't available in Foris OS, but in LuCI. Reconnect to your Omnia using your web browser but select LuCI this time. Log in.

* Go to System / Software
* Search for "nextcloud"
* Select "nextcloud" and "nextcloud-install" and download both packages

You can [optionally set the idle mode as described in our previous article]({{< ref "/blog/hns3-nas.md#s4" >}}) for your mSATA drive:

* Go to Services / hd-idle
* Enable hd-idle for "sda"
* Save & Apply your settings
* Go to System / Startup
* Check whether "hd-idle" is enabled and running

### Step 3: Install Nextcloud using SSH {#s3}
You can log out of LuCI. We need an SSH connection for this step. Use the terminal on your computer and connect to your Omnia using SSH: `ssh root@192.168.1.1` Use the password for "advanced administration" like [configured in the first part]({{< ref "/blog/hns1-hello-world.md#config-to" >}}) of this series.

Start the installer by entering `sudo nextcloud_install` and then "YES". There will be a lot of output regarding your database which we will configure in the next step. This is the easiest way to configure Nextcloud.

Choose a `user` as admin and enter a `password` for this user when prompted. The setup will finally show you the link of your Nextcloud instance and your credentials which you have just entered.

### Step 4: Secure the database {#s4}
After installing Nextcloud, we have to configure our database. Enter `sudo /usr/bin/mysql_secure_installation` while being still connected with your Omnia via SSH.

Confirm the initial password prompt with ENTER since we haven't set a password before. Then you can set the `root password` of the database. After that, you have to press "Y" several times:

* Remove anonymous users? [Y/n] `Y`
* Disallow root login remotely? [Y/n] `Y`
* Remove test database and access to it? [Y/n] `Y`
* Reload privilege tables now? [Y/n] `Y`

All done! Disconnect your terminal and close it.

{{< mastodonbox >}}

### Step 5: Open Nextcloud in your web browser {#s5}
Open your web browser and enter https://192.168.1.1/nextcloud (change IP address accordingly). You should see your Nextcloud instance after entering your credentials set before (see image below).

{{< webpimg "/art-img/as-hns3-6final.png" "Nextcloud is ready." >}}

## Tips
* [Enable and enforce **HTTPS**]({{< ref "/blog/hns2-tls-hardening.md" >}}) (encryption of data in transit)
* Enable **encryption for data at rest** for your Nextcloud instance (in Nextcloud's settings)
* **Check all settings** of your Nextcloud instance for additional configuration and security tips
* Keep in mind that users of the [guest network of your Turris Omnia]({{< ref "/blog/hns1-hello-world.md#to-guest-nw" >}}) **can't access** your Nextcloud instance since they are in another VLAN!
* If you want to access this Nextcloud instance **remotely**, **use VPN**! We will show you how to configure VPN in an upcoming article.

{{< hnsbox >}}

## Summary
Using Nextcloud has many benefits since you can use most of its features in your home network. However, compared with [NAS via SMB]({{< ref "/blog/hns3-nas.md" >}}) it needs about 40 to 60% of Omnia's RAM.

The biggest advantage is that you don't have to trust any web provider. Your data is securely stored at home under your physical control.

## Sources
* {{< extlink "https://doc.turris.cz/doc/en/howto/storage_plugin" "Turris doc: Foris: Storage setting plugin" >}}
