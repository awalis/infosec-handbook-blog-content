+++
title = "Yubico Security Key: Local 2FA with PAM"
author = "Benjamin"
date = "2018-08-22T10:03:01+02:00"
lastmod = "2019-04-22"
tags = [ "2fa", "pam", "yubikey", "nitrokey", "gdm" ]
categories = [ "authentication" ]
ogdescription = "We show you how you can use a YubiKey Security Key for 2FA with PAM."
slug = "yubikey-2fa-pam"
banner = "banners/yubikey-security-key"
+++

Some time ago, we [compared the YubiKey 4C and the Nitrokey Pro]({{< ref "/blog/yubikey4c-nitrokeypro.md" >}}) which we both use on a daily basis. This time, we show you how you can use a Yubico Security Key with the pluggable authentication module (PAM) on Linux for local [two-factor authentication]({{< ref "/glossary.md#2fa" >}}) (2FA).
<!--more-->
## Contents
1. [What is U2F, FIDO and FIDO2?]({{< relref "#u2f-fido" >}})
2. [Requirements]({{< relref "#requirements" >}})
3. [Step 1: Download and install pam_u2f]({{< relref "#s1" >}})
4. [Step 2: Use pamu2fcfg to generate your config]({{< relref "#s2" >}})
5. [Step 3: Update your PAM file]({{< relref "#s3" >}})
  * [Optional: Use U2F for sudo]({{< relref "#u2f-sudo" >}})
  * [Optional: Get prompted to insert your security token]({{< relref "#u2f-prompt" >}})
  * [Optional: Add a second U2F key]({{< relref "#2nd-u2f" >}})
6. [Other security tokens]({{< relref "#alternative" >}})
7. [Summary]({{< relref "#summary" >}})
8. [Links]({{< relref "#links" >}})

{{< rssbox >}}

## What is U2F, FIDO and FIDO2? {#u2f-fido}
_Universal 2nd Factor_ (U2F) is an open authentication standard originally developed by Yubico and Google and now hosted by the FIDO Alliance. Security devices with U2F support allow you to use [two-factor authentication]({{< ref "/glossary.md#2fa" >}}) more easily since they contain a secret key which can provide a second factor only by pressing the device's button. You don't need to manage more credentials. [We listed further benefits]({{< ref "/blog/yubikey4c-nitrokeypro.md#usb-vs-app" >}}) in our last article about security tokens.

The _Fast IDentity Online Alliance_ (FIDO Alliance) consists of different companies which recognized that security tokens for strong authentication lack interoperability. Their aim is to provide widely-accepted specifications to make the usage of online services more secure. The FIDO Alliance published two different specifications:

* UAF: Universal Authentication Framework (no passwords or user interaction involved)
* U2F: Universal 2nd Factor (using a security token as a second factor)

Another project of the FIDO Alliance is _FIDO2_. FIDO2 connects the new Web Authentication standard ([WebAuthn]({{< ref "/glossary.md#webauthn" >}})) with FIDO's Client-to-Authenticator Protocol (CTAP). The goal is to enable users to authenticate to online services by using their security devices (mobile and desktop).

To put it in a nutshell, U2F enables two-factor authentication to strengthen existing password-based logins while FIDO2 will enable global passwordless authentication (UAF) or two-factor authentication (U2F) as before.

## Requirements
For this tutorial, you need:

* a Yubico Security Key (or another U2F security token like Nitrokey FIDO U2F, or SoloKey)
* your Linux device (we use Arch Linux in this guide)

## Step 1: Download and install pam_u2f {#s1}
First of all, we have to install `pam-u2f`. This is a PAM module which implements U2F. You can use this module for all U2F security tokens not only for YubiKeys. Arch users can install it using AUR: `aurman -S pam_u2f`. This also installs Yubico's U2F Server and Host C libraries (`libu2f-host` and `libu2f-server`).

At this point, you can check whether your system recognizes the YubiKey: `dmesg | grep -i "Yubico Security Key"`. This shows "… USB HID v1.10 Device [Yubico Security Key by Yubico] …". If your device doesn't recognize the YubiKey, reboot it.

## Step 2: Use pamu2fcfg to generate your config {#s2}
After installing `pam-u2f`, you can use `pamu2fcfg` to easily generate a configuration file which we will add to the PAM configuration later.

Enter `pamu2fcfg`. The physical button of the Security Key blinks now. Press the button of the YubiKey to proceed. You will immediately see an output like "[username]:[key-handle],[public-key]" in your terminal.

Open your favorite text editor and save this output in a file which is accessible for the user (e.g., in the `home` folder of the user). We will put it in `~/.config/Yubico/u2f_keys` since there is already a Yubico folder and `u2f_keys` is the default location for this PAM module's auth file.

Advanced users can directly redirect the output of `pam-u2f` to the file by entering: `pamu2fcfg > ~/.config/Yubico/u2f_keys`.

## Step 3: Update your PAM file {#s3}
Finally, we have to tell PAM to use our U2F security key. You can either create a new file in `/etc/pam.d/` or edit an existing file. We will edit `/etc/pam.d/gdm-password` here to use U2F with the GNOME Display Manager (GDM).

Add `auth sufficient pam_u2f.so debug` to the file and save it. The `debug` keyword enables debug output in case of any errors. Log out and test whether you can log in with your U2F key and/or password.

In case of success, remove `debug`, and change `sufficient` to `required`. The difference is explained below:

* `sufficient`: This is a PAM keyword telling PAM that if this module succeeds, all following sufficient modules are also satisfied. In our case, PAM will accept:
  * only our password (single factor)
  * only our U2F token (single factor)
  * both factors (if the U2F token is connected with the device when GDM asks for the password)
* `required`: This enforces usage of the U2F token and enables true two-factor authentication.

Save the file, and log out again.

### Optional: Use U2F for sudo {#u2f-sudo}
In order to use U2F for sudo, you only need to add the line `auth sufficient pam_u2f.so` to `/etc/pam.d/sudo`. This results in exactly the same behavior as with `gdm-password`.

### Optional: Get prompted to insert your security token {#u2f-prompt}
Using the configuration above, there is no visual indication to insert your security token. This can be changed by adding the keyword `interactive` to the end of the line. For instance: `auth sufficient pam_u2f.so interactive`.

Using this for sudo results in the prompt "Insert your U2F device, then press ENTER." in the Gnome Terminal and/or in GDM.

### Optional: Add a second U2F key {#2nd-u2f}
There is the risk of losing or destroying the U2F token. We recommend to use a second one as a backup. You have to run [step 2]({{< relref "#s2" >}}) again but this time you press the button of the backup device.

Change the string in `~/.config/Yubico/u2f_keys` to "[username]:[key-handle],[public-key]:[backup-key-handle],[backup-public-key]" and test it again.

## Other security tokens {#alternatives}
In November 2018, Nitrokey UG released their Nitrokey FIDO U2F (which doesn't support FIDO2/WebAuthn). Check our [comparison of both U2F tokens]({{< ref "/blog/yubico-security-key-nitrokey-u2f.md" >}}). Moreover, there is the SoloKey that supports FIDO2/WebAuthn.

{{< mastodonbox >}}

## Summary
In this article, we showed two of many examples for using a U2F security token. The new WebAuthn standard has the potential to spread acceptance of such tokens on the internet making credential management less painful. We will show you more examples in upcoming articles.

You can also check open-source projects like the Solokeys or the Nitrokey FIDO U2F if you don't want to buy YubiKeys. Most U2F-only tokens cost about € 20.

## Links
* {{< extlink "https://www.yubico.com/" "Yubico" >}}
* {{< extlink "https://shop.solokeys.com/" "SoloKeys" >}}
* {{< extlink "https://developers.yubico.com/pam-u2f/" "pam-u2f" >}}
* {{< extlink "https://fidoalliance.org/" "FIDO Alliance" >}}
* {{< extlink "https://www.w3.org/TR/webauthn/" "Web Authentication: An API for accessing Public Key Credentials Level 1" >}}
* {{< extlink "https://twofactorauth.org/" "Information about favorite websites and their 2FA support" >}}
* {{< extlink "https://www.dongleauth.info/" "List of websites and their OTP/U2F support" >}}

## Changelog
* Apr 22, 2019: Added sections about using U2F for sudo, and getting a visual prompt.
* Nov 3, 2018: Updated information about Nitrokey FIDO U2F.
