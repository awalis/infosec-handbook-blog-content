+++
title = "More secure blogging with static site generators"
author = "Jakub"
date = "2018-10-21T16:00:00+02:00"
lastmod = "2019-01-16"
tags = [ "blogging", "wordpress", "cms", "hugo" ]
categories = [ "discussion" ]
ogdescription = "We introduce static site generators for more secure blogging."
slug = "static-blogging"
banner = "banners/static-blogging"
+++

Some readers asked which WordPress theme we use. The simple answer: We do not use WordPress at all. WordPress and other big content management systems (CMS) like Joomla or Drupal are extremely overpowered for personal blogs and smaller projects like our InfoSec Handbook blog. In this article, we give a basic introduction to the static site generator Hugo and discuss security issues of CMS.
<!--more-->
## Contents
1. [CMS vs. static site generators]({{< relref "#cms-vs-ssg" >}})
2. [Benefits of static websites]({{< relref "#sw-benefits" >}})
3. [Drawbacks of static websites]({{< relref "#sw-drawbacks" >}})
4. [How to use Hugo]({{< relref "#how-to" >}})
  * [Install Hugo]({{< relref "#s1" >}})
  * [Understand the folder structure]({{< relref "#s2" >}})
  * [See your content in real-time]({{< relref "#s3" >}})
  * [Generate your websites]({{< relref "#s4" >}})
  * [What to do next]({{< relref "#s5" >}})
5. [Summary]({{< relref "#summary" >}})
6. [Links]({{< relref "#links" >}})
7. [Changelog]({{< relref "#changelog" >}})

{{< rssbox >}}

## CMS vs. static site generators {#cms-vs-ssg}
Content management systems allow you to _manage digital content_. CMS are mostly created for team collaboration and provide ways to search, index and format content. CMS like WordPress are modular and user-friendly. They are easy to install and come with dozens of themes. Users don't have to write any HTML code.

On the other hand, static site generators like Hugo _take a set of files and generate output_ (the ready-to-publish website). There is no content management. You must edit HTML/CSS files and there is no PHP. The website is static. Of course, you could add JavaScript, however, PHP won't be available. It is a static website.

This may sound like big disadvantages but this is wrong. Static site generators like Hugo only run on your local computer. You don't have to install Hugo on your server. You don't need PHP, you don't need a database. There isn't even some sort of online account management. See the following picture that shows software needed for CMS and for static websites.

{{< webpimg "/art-img/static-blogging-cms-vs-static.png" "This picture shows that content management systems like WordPress need much more software components in comparison with static site generators." >}}

Let's have a look at [security vulnerabilities]({{< ref "/glossary.md#vulnerability" >}}) found in three famous content management systems (according to cvedetails.com, 1/16/2019):

* WordPress (278 in total, 17 in 2018)
* Drupal (173 in total, 8 in 2018)
* Joomla (219 in total, 24 in 2018)

In January 2019, Imperva (a provider of data and application security solutions) published a report that lists 542 vulnerabilities in WordPress which is a 30% increase from 2017. 98% of vulnerabilities were related to WordPress plugins. This shows the huge risk of adding arbitrary plugins to a CMS that already comes with its own security vulnerabilities.

However, you need even more software like PHP and a database to run a content management system:

* PHP (577 in total, 21 in 2018)
* MySQL (242 in total)
* MariaDB (74 in total)
* … (additional libraries and packages needed for your CMS, PHP or database)

If you use Hugo, you won't be affected by any of these security vulnerabilities since there is no CMS, no PHP, and no database server. Instead of adding more and more security features and plugins which have the potential to introduce new security vulnerabilities, you need less software. **Attackers can't attack something that isn't there.** You don't have to keep an eye on news tickers to learn about the latest security vulnerabilities in your CMS.

Furthermore, you execute Hugo offline. There is no Hugo on your server. You only need web server software and the underlying operating system.

## Benefits of static websites {#sw-benefits}
In summary, you get the following benefits in comparison with a typical CMS if you use Hugo or other static site generators:

* no need for PHP or databases—attackers can't hack your database since there is none
* attackers can't attack your web login since there is none
* your output is pure HTML, CSS and other static files—it's very unlikely that static content contains security vulnerabilities
* static content can be delivered way faster than dynamic content, so your website's loading speed is fast
* Hugo generates real-time preview on your local machine—this isn't only really fast, it is also offline (you can write articles even if you are offline!)
* you can disable HTTP POST and set a strict [Content Security Policy]({{< ref "/glossary.md#csp" >}}) since there is no need for JavaScript or HTML forms
* your static website won't be affected by injections, or [XSS]({{< ref "/glossary.md#xss" >}})—the two most common vulnerabilities in 2018, according to Imperva
* you can back up your complete website instantly—simply copy all files
* you can use Git for versioning of your content

## Drawbacks of static websites {#sw-drawbacks}
Of course, there are some drawbacks:

* If you use Hugo, you must learn its concepts: folder structures, commands and the generation process
* If you switch from your CMS to Hugo, you may miss some features
* If different people write content, you must find a solution to work collaboratively and synchronize all files (we use Git)

## How to use Hugo {#how-to}
Seeing your first website is quite easy: Download and install Hugo, create a demo website and start the local development server.

### Install Hugo {#s1}
Most Linux users can use their package managers to install Hugo and there are pre-built binaries for Windows, macOS, OpenBSD and FreeBSD.

Install Hugo and create a new website: `hugo new site [name-of-your-site]`. This command creates the default folder structure and shows where you can find Hugo themes.

### Understand the folder structure {#s2}
As of Hugo 0.49.2, default project folders are:

* archetypes: Archetypes are like templates that are used when you call "hugo new site". You can define new templates, if necessary. You can also simply create a new file without using Hugo. Archetypes are optional.
* content: This folder contains the actual content (Markdown files, .md) of your website. You can create subfolders to organize different sections of your website.
* data: This folder contains additional configuration files like "l10n.toml" for multi-language support.
* layouts: This is another template folder for HTML files.
* static: This folder contains static content like CSS, JPG and PNG files. You can create subfolders to organize different file types.
* themes: This folder contains your theme. In general, theme folders contain some of the folders mentioned above, so you can safely delete "archetypes" and "layouts" in your main folder since they are defined by the theme.

There is also the "config.toml" file. This is the project's main configuration file. You can define custom project-wide variables in this file. Download several themes and study their configuration files to understand its purpose.

### See your content in real-time {#s3}
Navigate to the main folder of your website and enter `hugo server`. This command starts a local development server on port 1313. Open your web browser and go to http://localhost:1313. There is the live preview (requires JavaScript) of your website. Of course, the local development server can be used offline.

### Generate your websites {#s4}
You can directly generate your website by entering `hugo --theme=[your-theme]`. This command creates a new folder "public" which contains your ready-to-publish website. You can upload its content without any modification to your web server's main folder. That's all.

In summary, you have:

* configuration files (like .toml) that define parameters of your website
* theme files (.html) with Go code that tell Hugo how to create your website
* content files (.md) that represent a single page of your website and contain text as well as metadata
* static files (.png, .jpg, .css, .pdf, .woff2, …)

### What to do next {#s5}
* Secure blogging requires hardened web server software and a hardened server operating system, too. Feel free to read and implement our [web server security series](/as-wss/).
* Some Hugo themes contain unnecessary elements like JavaScript or external fonts. Remove these parts, host files on your own server or use [Subresource Integrity]({{< ref "/glossary.md#subresource-integrity" >}}) if you still need external files.
* The default configuration of your web server can be optimized to speed things up. Enable HTTP/2, Brotli compression and apply HTML/CSS minification.
* The whole creation process can be automatized by using shell scripts. For example, we only have to enter one single command that automatically creates, optimizes, uploads and backs up our blog.
* Use Shortcodes. Shortcodes are small code snippets that can call raw HTML files as templates. See the Hugo documentation for examples.

{{< mastodonbox >}}

## Summary
Some people may be convinced that static files are old-school. We don't think so. You can create a modern-looking blog that is more secure and faster than most CMS-based blogs. You can also heavily modify your theme and add custom functions. Moreover, you need less software on your web server and you don't have to bother your head about patching your CMS, PHP or your database software.

However, the biggest disadvantage is the complex initial startup. Getting started with Hugo isn't as easy as creating your CMS-based blog. Keep in mind that Hugo is only one of many static site generators.

## Links
* {{< extlink "https://gohugo.io/" "Hugo – the world's fastest framework for building websites" >}}
* {{< extlink "https://gohugo.io/documentation/" "Hugo – documentation" >}}
* {{< extlink "https://themes.gohugo.io/" "Hugo – themes" >}}
* {{< extlink "https://www.staticgen.com/" "StaticGen – A List of Static Site Generators (for JAMstack Sites)" >}}
* {{< extlink "https://www.imperva.com/blog/the-state-of-web-application-vulnerabilities-in-2018/" "imperva.com – The State of Web Application Vulnerabilities in 2018" >}}

## Changelog
* Jan 16, 2019: Updated vulnerabilities in 2018. Added CMS plugins to the picture. Added information according to imperva.com.
* Oct 22, 2018: Added security vulnerabilities in PHP, MySQL and MariaDB.
