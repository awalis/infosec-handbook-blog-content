+++
title = "Mastodon: Security and privacy settings"
author = "Benjamin"
date = "2018-08-24T09:29:14+02:00"
lastmod = "2019-04-28"
tags = [ "privacy", "mastodon", "yubikey", "keybase", "2fa" ]
categories = [ "tutorial" ]
ogdescription = "We show you important settings to make your Mastodon experience more secure and private."
slug = "mastodon-security"
banner = "banners/mastodon-security"
+++

Maybe you just joined a Mastodon instance or already _toot_ for months. Whatever the case may be, in this article we show you important settings to make your Mastodon experience more secure and private.
<!--more-->
## Contents
1. [What is Mastodon?]({{< relref "#about" >}})
2. [Security and privacy settings]({{< relref "#sps" >}})
  * [Security tip 1: Enable 2FA]({{< relref "#s1" >}})
  * [Security tip 2: Monitor sessions activity]({{< relref "#s2" >}})
  * [Security tip 3: Monitor authorized apps]({{< relref "#s3" >}})
  * [Privacy tip 1: Check your "post privacy", and "disclose application" settings]({{< relref "#p1" >}})
  * [Privacy tip 2: Use "authorized followers" feature]({{< relref "#p2" >}})
  * [Privacy tip 3: Host your own Mastodon instance]({{< relref "#p3" >}})
3. [Additional tips]({{< relref "#at" >}})
4. [Summary]({{< relref "#summary" >}})
5. [Changelog]({{< relref "#changelog" >}})

{{< rssbox >}}

## What is Mastodon? (For newbies!) {#about}
Mastodon is a _decentralized_ and _federated_ social network for microblogging. This means that there are thousands of Mastodon instances (= servers) on the internet which aren't controlled by a single company or person. This also means that most of these instances are connected so you can see what people on other instances write or do. You can follow them or like/share their toots (= messages). People using Twitter will immediately settle in.

Mastodon offers [two-factor authentication]({{< ref "/glossary.md#2fa" >}}) (2FA) and several privacy settings. Another important point is the risk of being [impersonated]({{< ref "/glossary.md#impersonation" >}}).

## Security and privacy settings {#sps}
In order to find all settings, you have to enter `https://[your-mastodon-instance]/settings/profile` in your web browser or click on the cogwheel icon resp. "Preferences".

### Security tip 1: Enable 2FA {#s1}
Two-factor authentication ensures that you have to provide two factors each time you try to log in. The first factor is your password. The second factor is a generated time-based one-time password ([OATH-TOTP]({{< ref "/glossary.md#oath-totp" >}})) in this example. We will use our [YubiKey](/tags/yubikey/), however, there are apps like "FreeOTP" which can be used to generate one-time passwords on your smartphone.

To enable 2FA, go to your settings and then "Two-factor Auth" (`/settings/two_factor_authentication`). You will see a button to set up 2FA on your screen:

{{< webpimg "/art-img/mastodon-security-2fa-1.png" "This is the initial screen when you set up 2FA for the first time." >}}

After proceeding, you see a QR code and the secret in plain text. You can either directly scan the QR code using your app or the Yubico Authenticator or you can manually enter the plain-text secret. We recommend to scan the QR code. **Important:** The secret is actually a secret, so keep it secret!

{{< webpimg "/art-img/mastodon-security-2fa-2.png" "Scan this QR code using your 2FA app or the Yubico Authenticator. Enter the generated one-time password below and click on 'Enable'." >}}

If you use the Yubico Authenticator, you have to go to "File > Scan QR Code". The Yubico Authenticator recognizes the QR code and shows the following dialog. Just click on 'Save credential'.

{{< webpimg "/art-img/mastodon-security-2fa-3.png" "The Yubico Authenticator shows a new entry after scanning the QR code. You only have to save this." >}}

You finally have to enter your freshly-generated one-time password below the QR code ('Two-factor code' field) and click on "Enable". After that, Mastodon shows you emergency recovery codes. Store them in a secure place (e.g., print them on paper). They are also secrets!

{{< webpimg "/art-img/mastodon-security-2fa-4.png" "After entering your freshly-generated one-time password, Mastodon will show you emergency codes. Store these codes offline (e.g., print them on real paper!). They are also secrets!" >}}

When you return to the Two-factor Auth setting, it will show "Two-factor authentication is enabled". Now you have to enter your password plus OTP token each time. Done!

{{< webpimg "/art-img/mastodon-security-2fa-5.png" "2FA is enabled! Now you have to enter your password plus OTP token each time." >}}

The process described above is similar for most other websites which provide TOTP-based 2FA. So feel free to enable OATH-TOTP whenever possible.

### Security tip 2: Monitor sessions activity {#s2}
Mastodon stores your last logins. It is important to monitor this list to ensure that there is no suspicious activity like unknown IP addresses or web browsers accessing your account. Go to the "Security" page (`/auth/edit`).

There is a "Sessions" section which shows you web browsers and IP addresses of your last logins. Revoke suspicious sessions and regularly check this page. You can see the User-Agent of a web browser if you hover your pointer over the short description.

{{< webpimg "/art-img/mastodon-security-sessions.png" "The 'sessions' section shows information about web browsers and operating systems you (or somebody else!) used to log in." >}}

### Security tip 3: Monitor authorized apps {#s3}
Of course, you can use third-party apps to access Mastodon. It is important to monitor these apps. Maybe you stopped using an app months ago but didn't log out? Go to "Authorized apps" (`/oauth/authorized_applications`) to see a list like below. Be sure that all apps shown on the list are your apps.

{{< webpimg "/art-img/mastodon-security-authorized-apps.png" "These apps are authorized to connect with your Mastodon account. Revoke unused apps immediately!" >}}

### Privacy tip 1: Check your "post privacy", and "disclose application" settings {#p1}
Besides security, there are privacy settings. These settings are on the "Preferences" page (`/settings/preferences`). Look for "Post privacy":

* **Public** posts are visible for everyone on the internet.
* **Unlisted** posts aren't visible on your timeline, but people can still see them if they know the link.
* **Followers-only** posts are somewhat private so only your followers can see them.

These are the default settings for all future posts. You can also change this explicitly for every toot by clicking on the "world" icon before tooting (this includes direct messaging).

You can also:

* Opt-out of search engine indexing: Avoid to be indexed by Google and other search engines.
* Hide your network: Enable this to hide the list of your followers and people you are following.
* Disclose application used to send toots: Disable this to hide the name of apps you use to post on your Mastodon instance

### Privacy tip 2: Use "authorized followers" feature {#p2}
If you lock your account (go to "Edit profile" `/settings/profile`), you have to manually approve each new follower. This enables the "authorized followers" feature (`/settings/follower_domains`).

The page "Authorized followers" allows you to effectively block complete Mastodon instances if you think that this has to be done (e.g., if somebody uses an instance for spamming).

{{< img "/art-img/mastodon-security-authorized-followers.png" "After locking your account, you can remove Mastodon instances from your follower list, effectively blocking the whole instance." >}}

### Privacy tip 3: Host your own Mastodon instance {#p3}
Yeah, we don't like the "simply host your own server" tip since securely hosting your own services is never easy. But as shown before in our [XMPP: Admin-in-the-middle]({{< ref "/blog/xmpp-aitm.md" >}}) article, admins can often access even private information and may be in full control of your [personal data]({{< ref "/glossary.md#personal-data" >}}).

Hosting your own Mastodon instance gives you more control over your data but also comes with more responsibilities in terms of server security and management.

## Additional tips {#at}
We have several more tips for you:

* Always use a Mastodon instance which has the latest version of Mastodon installed. Several instances are still running really old versions of Mastodon. This can be a security risk for your personal data. There are several publicly-known vulnerabilities in Mastodon versions before 2.4.4, resp. before 2.5.2.
* Check the security and privacy configuration of your Mastodon instance by using [online assessment tools]({{< ref "/blog/online-assessment-tools.md" >}}). Be aware that the results can be misleading or irrelevant!
* Use [Keybase](/tags/keybase/) to connect your Mastodon account(s) with other online identities. This reduces the risk of successfully being impersonated. While there is no direct way to add Mastodon proof to your Keybase account at the moment, there are several workarounds to make your account verifiable.
* Support the admin of your Mastodon instance!

{{< mastodonbox >}}

## Summary
Mastodon's security and privacy features allow you to make your Mastodon experience more secure and keep non-public posts private. Get familiar with these features and find your personal use cases. Last but not least, support the admin of your Mastodon instance and look for security-related updates!

## Changelog
* Apr 28, 2019: Added "Disclose application used to send toots".
