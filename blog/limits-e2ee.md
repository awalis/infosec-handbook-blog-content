+++
title = "Limits of end-to-end encryption"
author = "Jakub"
date = "2018-08-31T10:20:00+02:00"
tags = [ "e2ee", "privacy", "encryption", "metadata" ]
categories = [ "limits" ]
ogdescription = "More and more services offer E2EE. While this is good for privacy, it is not a magic bullet."
slug = "limits-e2ee"
banner = "banners/limits-e2ee"
+++

Skype, WhatsApp, Telegram, Signal, Protonmail—more and more services offer [end-to-end encryption]({{< ref "/glossary.md#end-to-end-encryption" >}}). While this is good for privacy, it is not a magic bullet. In this article, we show you some limits of E2EE.
<!--more-->
## Contents
1. [What is E2EE?]({{< relref "#definition" >}})
2. [Common problems]({{< relref "#common-problems" >}})
  * [Vulnerable endpoints]({{< relref "#p1" >}})
  * [Unencrypted cloud backups]({{< relref "#p2" >}})
  * [Insecure encryption algorithms or implementations]({{< relref "#p3" >}})
  * [Backdoors]({{< relref "#p4" >}})
  * [No verification]({{< relref "#p5" >}})
  * [Personal data and metadata on servers]({{< relref "#p6" >}})
  * [Digital communication at all]({{< relref "#p7" >}})
3. [Summary]({{< relref "#summary" >}})

{{< rssbox >}}

## What is E2EE? {#definition}
End-to-end encryption (E2EE) ensures [confidentiality]({{< ref "/glossary.md#confidentiality" >}}) of data sent from one endpoint to another. For instance, you send a message to a friend overseas using your instant messenger which supports E2EE. Only your device and the device of your friend are able to decrypt this message. All other parties involved, like your internet service provider, servers of the instant messaging service or criminals, wannabe hackers etc. can't decrypt and read your messages.

E2EE isn't new. Its principle is already mentioned in the 1964 memorandum "On Distributed Communications: IX. Security, Secrecy, and Tamper-Free Considerations". Users of [GnuPG]({{< ref "/glossary.md#gnupg" >}}) (released in 1999) or Off-the-Record Messaging (OTR, released in 2004) already use E2EE for years.

## Common problems
There are some pitfalls when you use E2EE and E2EE doesn't mean that you are in full control of your data.

### Vulnerable endpoints {#p1}
First of all, there are the endpoints of end-to-end encrypted communication. Nowadays, we mostly use our smartphones for communication and smartphones are generally less secure than classic personal computers. These smartphones likely contain all of your messages in cleartext, so an attacker can simply try to read your communication by attacking your smartphone. This isn't uncommon at all. Actually, intelligence services and police departments take this approach since it is easier to break into your smartphone than breaking state-of-the-art encryption.

**This means that you and all of your family members, friends and so on have to protect all endpoints involved.** E2EE doesn't protect your communication on endpoints. Use full-disk encryption to protect messages on your devices.

### Unencrypted cloud backups {#p2}
The next pitfall are unencrypted cloud backups. Some services offer backups on the internet, so in case of losing your device, you can restore all of your data. However, these backups aren't always encrypted. This effectively undermines end-to-end encryption which is no E2EE anymore. An attacker can try to attack your service provider now or in case of public authorities, your service provider may be required by law to hand over unencrypted backups.

**The best protection is to go without any cloud backups. Make use of local, offline backups.**

### Insecure encryption algorithms or implementations {#p3}
Some services implemented custom self-developed encryption algorithms and sometimes we read conspiracy theories that the National Security Agency undermined [AES]({{< ref "/glossary.md#aes" >}}) and [RSA]({{< ref "/glossary.md#rsa" >}}), so we all have to use "no-name algorithms" in order to stay secure. The golden rule is to only use publicly-known and audited algorithms.

However, focusing on algorithms isn't enough. A product isn't secure if it uses "AES-256", "RSA-4096" or some "military-grade encryption". The actual implementation of algorithms is also very important.

**Thus, check if a product uses only well-known encryption algorithms, crypto libraries and check for recent third-party security audits of the whole product.** Keep in mind that encryption algorithms which are currently considered "secure" can become obsolete in future.

### Backdoors {#p4}
Frequently, governments all over the world discuss backdoors in software and/or hardware to circumvent encryption. While privacy activists and well-known cryptographers warn against backdoors, politicians and authorities advocate backdoors as the only way to protect people against terrorism and so on.

A real world backdoor are TSA luggage locks. These locks allow Transportation Security Administration agents to open baggage for security screening. However, The Washington Post inadvertently published a photo of TSA's physical master keys in a 2014 article making it possible for criminals to copy all keys and also open these locks. This shows the risk of backdoors if implemented in products or algorithms.

While some people repeatedly say that the only protection against backdoors is open-source code, we don't think that this is sufficient. Open source doesn't automatically mean that somebody checked the code for backdoors. **Check for recent third-party security audits of the whole product.** Don't rely on "security due to being open source".

### No verification {#p5}
End-to-end encryption uses [public-key cryptography]({{< ref "/glossary.md#public-key-cryptography" >}}). This means that there are two different keys involved: your private, secret key and a public key which isn't secret at all. Modern encryption algorithms are more complex, however, there is always the need to verify that your device is really talking with the right remote device and that no [man-in-the-middle]({{< ref "/glossary.md#man-in-the-middle-attack" >}}) is involved.

Some services offer their own mechanisms to automatize verification. For instance, they automatically trust a new key if there was no known key before. **Don't rely on these mechanisms. Manually verify other devices out-of-band.** This means that you have to use other authenticated channels like websites with HTTPS, a phone call or a personal meeting.

### Personal data and metadata on servers {#p6}
End-to-end encryption protects content of your messages but it may not protect your [personal data]({{< ref "/glossary.md#personal-data" >}}) and/or [metadata]({{< ref "/glossary.md#metadata" >}}). There is always metadata involved since computers need to know how they have to handle data packets (sender, receiver, size, protocol etc.). You simply can't avoid metadata if you communicate. However, some protocols generate more metadata than others and some protocols encrypt some of the metadata while other protocols keep everything in cleartext.

Another point is the storage of personal data. Some services store most of your personal data on servers out of your control in cleartext. For example, [XMPP]({{< ref "/blog/xmpp-aitm.md" >}}) servers store all of your contacts, group memberships and other information in cleartext even if you are using end-to-end encryption. Furthermore, admins can monitor whether your device only received a certain message or if you actually read this particular message. On the other hand, Signal keeps most data on the devices to avoid disclosure to servers.

**Always check the amount of personal data stored by a service provider before using this service. Read the privacy policy and reports on the internet.** Sometimes, the documentation of server software clarifies which data is stored on the server and how it is protected.

### Digital communication at all {#p7}
Some people say that [decentralization and federation are more important than end-to-end encryption]({{< ref "/blog/myths-federation.md#m3" >}}) or they suggest that decentralization and federation protect against mass surveillance.

**Keep in mind that online communication is always somewhat centralized** due to the internet backbone, internet service providers, service providers of web servers etc. **There is no invisible traffic** which can't be spotted if you use the internet or even Tor. We always recommend to use end-to-end encryption to keep the content of your messages private.

{{< mastodonbox >}}

## Summary
End-to-end encryption ensures confidentiality of message contents. It neither automatically ensures [authenticity]({{< ref "/glossary.md#authenticity" >}}) nor anonymity.

If there is a service which offers end-to-end encryption, check for recent security audits covering the whole product, disable unencrypted cloud backups and verify that you are talking with your friends. Keep in mind that there is always metadata involved and check whether this service stores personal data on its servers.
