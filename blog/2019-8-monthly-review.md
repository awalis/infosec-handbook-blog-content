+++
title = "Monthly review – August 2019"
author = "Benjamin"
date = "2019-08-31T15:56:21+02:00"
tags = [ "knob", "bluetooth", "dejablue", "minisign", "gnupg" ]
categories = [ "Monthly review" ]
ogdescription = "Our monthly review of August 2019."
slug = "2019-8-monthly-review"
banner = "banners/monthly-review"
+++

Each month, we publish a review that covers the most important activities of the last 30 days. This month, we talk about the KNOB attack, DejaBlue, minisign, and more.
<!--more-->
## Contents
1. [News of the month]({{< relref "#notm" >}})
2. [Tool of the month]({{< relref "#toolotm" >}})
3. [Tip of the month]({{< relref "#tipotm" >}})
4. [Readers' questions of the month]({{< relref "#rqotm" >}})
5. [Our activities of the month]({{< relref "#ishotm" >}})
6. [Closing words]({{< relref "#cw" >}})
7. [Links]({{< relref "#links" >}})

{{< rssbox >}}

## News of the month {#notm}
In August 2019, two major [security vulnerabilities]({{< ref "/glossary.md#vulnerability" >}}) were disclosed:

1. The [KNOB attack]({{< relref "#links" >}}) (CVE-2019-9506) likely affects all Bluetooth devices (except devices using Bluetooth Low Energy, or BLE). The vulnerability allows attackers to inject packages during the encryption key length negotiation process to enforce only 1 byte of [entropy]({{< ref "/glossary.md#entropy" >}}) used for encrypting Bluetooth traffic. Attackers must be in range of the Bluetooth devices. Since attackers force both devices to use smaller but valid key sizes, it is basically a [downgrade attack]({{< ref "/glossary.md#downgrade-attack" >}}). Afterwards, the remaining key space can be easily [brute forced]({{< ref "/glossary.md#brute-force-attack" >}}) to decrypt all of the traffic between the devices during that session. Countermeasures are:
  * Enforcing 16 bytes of entropy in the Bluetooth firmware (requires firmware update),
  * checking the key size upon connection (requires software update),
  * securing communication on the layers above.
  * Besides, you can use BLE devices only, or turn of Bluetooth if not in use.
2. Then, we had [DejaBlue]({{< relref "#links" >}}) (CVE-2019-1181, CVE-2019-1182, CVE-2019-1222, CVE-2019-1226), a newly-discovered BlueKeep-like security vulnerability in the Remote Desktop Services (RDS) of current versions of Microsoft Windows. An unauthenticated attacker can send specially crafted packets to a vulnerable host to execute arbitrary code on the host without any user interaction. Microsoft released patches to address the vulnerability. Besides patching, you can turn off Remote Desktop Services, block TCP port 3389 in your firewall, and enable Network Level Authentication (NLA). Affected are:
  * Windows 7 SP1 (only affected if either RDP 8.0 or RDP 8.1 is installed),
  * Windows 8.1 and Windows RT 8.1,
  * Windows 10,
  * Windows Server 2008 R2 SP1 (only affected if either RDP 8.0 or RDP 8.1 is installed),
  * Windows Server 2012 and Windows Server 2012 R2,
  * Windows Server 2016,
  * Windows Server 2019,
  * and all Windows-10-based Windows Server editions.

Moreover, there were also some data breaches. Have I Been Pwned added information about the following breaches:

* CafePress (breached in February 2019)
* Canva (breached in May 2019)
* StockX (breached in July 2019)
* Cracked.to (breached in July 2019)
* Chegg (breached in April 2018)
* Coinmama (breached in August 2017)

Check if you were affected, and change your credentials. Besides, feel free to [subscribe to our RSS/Atom feed](https://mastodon.at/users/infosechandbook.atom), or directly [follow us in the Fediverse](https://mastodon.at/users/infosechandbook/remote_follow) to learn about data breaches and much more.

## Tool of the month {#toolotm}
This month, we present ["Minisign – A dead simple tool to sign files and verify signatures."]({{< relref "#links" >}}) If you don't want to use the Swiss Army knife [GnuPG]({{< ref "/glossary.md#gnupg" >}}) for [signing]({{< ref "/glossary.md#digital-signature" >}}), you can use minisign.

Minisign is a small tool that uses [Ed25519]({{< ref "/glossary.md#ed25519" >}}) for cryptographic signing. Several projects like the very popular crypto library libsodium, or dnscrypt-proxy use minisign to sign their releases. There are also libraries and implementations in Golang (go-minisign) and Rust (rsign2) available. The current version of minisign is 0.8, released in February 2018.

After downloading and installing minisign on your platform, you enter `minisign -G` to create a key pair. On Linux, the private, password-protected key is stored in `~/.minisign/minisign.key`, and the public key in `~/minisign.pub`.

The contents of `cat minisign.pub` look like:

{{< highlight txt "linenos=table" >}}
untrusted comment: current minisign public key of InfoSec Handbook
RWTobCZNZpK7QlEBFPj+eGxRxUrsF/wW+Rrm/XOL+RXaC1C6ZLplTsVL
{{< / highlight >}}

The first line is an "untrusted" comment. This means that it isn't signed and can be changed. The second line is the Base64 encoded public key.

After creating a key pair, the workflow is similar to tools like GnuPG: You publish your public key "minisign.pub", and use your local private key "minisign.key" to sign files. To sign files, just enter: `minisign -Sm [file-to-sign]`. After entering the password for the private key, a second file is created, named "[file-to-sign].minisig". Another person can then verify the signature of a file by entering: `minisign -Vm [file-to-sign] -p minisign.pub`. "[file-to-sign].minisig" must be in the same folder.

Furthermore, you can add "trusted" comments. Trusted comments are signed. Enter `minisign -Sm [file-to-sign] -t "[a-trusted-comment]"`.

The result looks like:

{{< highlight txt "linenos=table">}}
untrusted comment: signature from minisign secret key
RWTobCZNZpK7QnVLb7KjgV0QB+MaYemn/rjDMwIJUcnUyYwHqgCq5JQqwDDEbOAuk2f8WqDpQsYF15ZVgISJcC+NLPaD/WDG4wc=
trusted comment: a trusted comment by InfoSec Handbook
JTbwBH2GAtnYBbGq484em05IF9/PLY97mhsdqWSUbZP8UYOHDn0YZGKdQNImBHcyHwhKkQrW5kgsio1ixLltAw==
{{< / highlight >}}

By the way, signatures by minisign can be verified using OpenBSD's signify tool. Public key files and signature files are compatible.

Check it out, and tell about your use cases.

## Tip of the month {#tipotm}
This month, our tip is about faster generation of GnuPG keys. We regularly change our GnuPG keys. Instead of generating each key manually, we use the following command (gpg 2.2.17):

`gpg --yes --quick-gen-key '[your-name] <[your-e-mail-address]>' future-default default $(date --iso-8601 --date="3 months")`

Let's look at the command in detail:

* `--yes`: Tells GPG to proceed even if there is already a key for the provided ID.
* `--quick-gen-key`: This generates a standard key with one user ID. `quick` means that there is less user interaction required.
  * `'[your-name] <[your-e-mail-address]>'`: This is your user ID (name and e-mail address).
  * `future-default default`: "future-default" is an alias for the algorithm which will likely be used as default algorithm in future versions of GPG. Currently, "future-default" means [Ed25519]({{< ref "/glossary.md#ed25519" >}}) for signing, and [Curve25519]({{< ref "/glossary.md#curve25519" >}}) for encryption. "default" also creates a subkey for encryption.
  * `$(date --iso-8601 --date="3 months")`: This runs `date` and returns an ISO 8601 formatted date (in 3 months from now). The result is a GPG key that is valid for three months.

In summary, this command allows you to quickly create a fresh GPG key pair using modern algorithms.

## Readers' questions of the month {#rqotm}
Each month, readers send us questions via e-mail, Keybase, Mastodon (Fediverse), or via the forum of privacytools.io. In general, we directly reply to questions. However, we would like to list some questions and answers that are interesting for more than only one person:

* "Is there any downside of setting legacy HTTP headers?": There shouldn't be any downsides. The biggest problem is the false sense of security: Server admins set headers like X-Frame-Options, X-Xss-Protection, or HPKP, because they are mentioned by some old guides on the internet while every modern web browser ignores these headers.
* "Which operating system do you use at home?": Most of us use Linux (Arch, Ubuntu 18.04, Debian 10, Parrot OS).
* "Which e-mail provider do you recommend, or should I host my own mail server?": Keep in mind that 'hosting yourself' always means continuous monitoring, fast and regular updating, and much more. If you can't ensure this or if you want convenience, use a well-known provider. We use mailbox.org, Tutanota, and Protonmail.
* "Do you plan to continue your series about the OpenWRT-based router Turris Omnia?": Yes, we will likely publish several new articles this year. We still think that this is one of the best routers you can get.
* "I want to become a security professional. How do I start?": Read [Your career in information security]({{< ref "/blog/infosec-career.md" >}}).

Just send us your questions. Maybe, your question will be listed in the next monthly review.

## Our activities of the month {#ishotm}
In August, we published four new articles, and updated many others. New articles are:

* [The false sense of security]({{< ref "/blog/discussion-false-sense.md" >}}): In this article, we discuss three typical situations that result in a false sense of security.
* [The current state of the LineageOS-based /e/ ROM]({{< ref "/blog/e-foundation-second-look.md" >}}): This is a follow-up [of our first look]({{< ref "/blog/e-foundation-first-look.md" >}}) in March, requested by many readers.
* [There is more than only black and white in information security and privacy]({{< ref "/blog/discussion-filter-bubbles.md" >}}): Here, we addressed the "black and white thinking" of several people that results in filter bubbles, and neither improves information security nor privacy.
* [Web server security – Part 8: Basic log file analysis]({{< ref "/blog/wss8-log-file-analysis.md" >}}): In part 8 of our [Web server security series]({{< ref "/as-wss.md" >}}), we present a tool to analyze log files more easily.

Besides, we fixed a bug that occurred in Chrome, Chromium, and Edge by adding a CSS-based workaround. Chroma-based syntax highlighting seems to be broken in either Hugo or Chroma at the moment. Moreover, we added some new tools to our [terminal tips]({{< ref "/terminal-tips.md" >}}) page. Another change was migrating our mailbox from Protonmail to Tutanota.

Finally, a responsible reader from Canada privately reported several files on infosec-handbook.eu that contained metadata which shouldn't be there. This person received about 430 Lumens (€25) for observing the [testing requirements and code of conduct]({{< ref "/security.md#tr-and-coc" >}}) of [our disclosure policy]({{< ref "/security.md#disclosure-policy" >}}). Thanks again.

{{< mastodonbox >}}

## Closing words {#cw}
In September, we will mainly proceed to add information for nginx and Caddy users to our [Web server security series]({{< ref "/as-wss.md" >}}) since our poll "Which web server software do you prefer?" showed that many people are using nginx. On the other hand, Caddy seems to be a good choice for a web server in certain use cases. Thanks to everybody who voted this time.

## Links
* KNOB attack: {{< extlink "https://knobattack.com/" "Key Negotiation of Bluetooth Attack: Breaking Bluetooth Security" >}}
* DejaBlue: {{< extlink "https://portal.msrc.microsoft.com/en-US/security-guidance/advisory/CVE-2019-1181" "CVE-2019-1181" >}}, {{< extlink "https://portal.msrc.microsoft.com/en-US/security-guidance/advisory/CVE-2019-1182" "CVE-2019-1182" >}}, {{< extlink "https://portal.msrc.microsoft.com/en-US/security-guidance/advisory/CVE-2019-1222" "CVE-2019-1222" >}}, and {{< extlink "https://portal.msrc.microsoft.com/en-US/security-guidance/advisory/CVE-2019-1226" "CVE-2019-1226" >}}
* {{< extlink "https://jedisct1.github.io/minisign/" "Minisign – A dead simple tool to sign files and verify signatures." >}}
* {{< extlink "https://mastodon.at/@infosechandbook/102616778663782103" "InfoSec Handbook poll: Which web server software do you prefer?" >}}
