+++
title = "Hack The Box walkthrough: Netmon"
author = "Benjamin"
date = "2019-06-29T17:30:00+02:00"
tags = [ "hackthebox", "pentesting", "ctf", "prtg", "ftp" ]
categories = [ "Hack The Box" ]
ogdescription = "This is our walkthrough for the Netmon box."
slug = "htb-netmon"
banner = "banners/htb"
notice = true
+++

Today, the virtual machine "Netmon" on Hack The Box retired. In this walkthrough, we show one way to retrieve the "user.txt" and "root.txt" files.
<!--more-->
## Contents
1. [General information about "Netmon"]({{< relref "#gi" >}})
2. [Step 1: Initial nmap scan]({{< relref "#s1" >}})
3. [Step 2: Check if FTP allows anonymous login]({{< relref "#s2" >}})
4. [Step 3: Checking files and folders using FTP]({{< relref "#s3" >}})
5. [Step 4: Checking PRTG Network Monitor]({{< relref "#s4" >}})
6. [Step 5: Exploiting CVE-2018-9276 in PRTG Network Monitor]({{< relref "#s5" >}})
7. [Summary]({{< relref "#summary" >}})

{{< rssbox >}}

{{% notice note %}}
If you are totally new to Hack The Box, read the <a href="/as-htb/#how-does-htb-work">short description</a> on our page about our <a href="/as-htb/">Hack The Box series</a>.
{{% /notice %}}

## General information about "Netmon" {#gi}
On hackthebox.eu, we get general information about the target. It runs "Windows" and is rated "easy". There are more than 17,000 user owns (user.txt) and more than 10,000 system owns (root.txt). We also see that this machine can likely be exploited using publicly-known vulnerabilities.

{{< webpimg "/art-img/as-htb-netmon-1card.png" "The Netmon card on Hack The Box." >}}

## Step 1: Initial nmap scan {#s1}
After connecting to the HTB VPN and adding the machine's IPv4 address to our "/etc/hosts" file (ish-netmon.htb), we conduct our initial nmap scan: `nmap -sV -sT -o ish-htb-initial-scan.txt ish-netmon.htb`.

* `-sV`: Probe open ports to determine service/version info.
* `-sT`: Do a TCP Connect Scan.
* `-o ish-htb-initial-scan.txt`: Save the output to this file.
* `ish-netmon.htb`: Scan this machine.

{{< webpimg "/art-img/as-htb-netmon-2nmap.png" "Open ports based on our initial nmap scan." >}}

This scan results in the following open ports:

* 21/tcp: Microsoft ftpd
* 80/tcp: Indy httpd 18.1.37.13946 (Paessler PRTG bandwidth monitor)
* 135/tcp: Microsoft Windows RPC
* 139/tcp: Microsoft Windows netbios-ssn
* 445/tcp: Microsoft Windows Server 2008 R2 - 2012 microsoft-ds

Besides, the operating system is either "Windows Server 2008 R2" or "Windows Server 2012".

## Step 2: Check if FTP allows anonymous login {#s2}
So, there is FTP on port 21. If configured insecurely, servers may allow anonymous FTP access. This feature was originally introduced to allow people to access archive sites, as described in RFC 1635 (May 1994). However, the server may allow people to access secret data if enabled.

First, we check if anonymous login is possible: `ftp ish-netmon.htb`. The server asks for a name ("Name (ish-netmon.htb:elliot):"). We just enter `anonymous`, and get the reponse: "331 Anonymous access allowed, send identity (e-mail name) as password."

{{< webpimg "/art-img/as-htb-netmon-3ftpaccess.png" "We are able to access FTP anonymously." >}}

So, anonymous access is enabled and we can use FTP to learn about the system now.

## Step 3: Checking files and folders using FTP {#s3}
While still connected to the machine via FTP, we enter `dir` to list the visible contents of the current folder. (We use `dir` instead of `ls` since it is a Windows machine.) We see the following folders:

* inetpub
* PerfLogs
* Program Files
* Program Files (x86)
* Users
* Windows

{{< webpimg "/art-img/as-htb-netmon-4folders.png" "All visible folders in C: on the remote machine." >}}

So, we are obviously on the C: drive. In "Program Files", we see Internet Explorer, VMware, Windows Defender, WindowsPowerShell, and WinPcap. In "Program Files (x86)", there is additionally Microsoft.NET, and PRTG Network Monitor. We already know that PRTG Network Monitor runs on this server as reported by nmap.

If we go to "Users", there are the folders "Administrator" and "Public". Trying to access the "Administrator" folder, results in "550 Access is denied." If we access the "Public" folder, we already see the "user.txt" file. This was quite easy. We use `get user.txt` to retrieve the file. Now, we have to find the "root.txt" file. It is likely located in the "Administrator" folder that can't be accessed via FTP. Time to choose another strategy. We `quit` FTP for now.

## Step 4: Checking PRTG Network Monitor {#s4}
nmap already reported that there is "PRTG Network Monitor" running on port 80. nmap also reported its version number: 18.1.37.13946. If we go to "ish-netmon.htb" using a normal web browser, we see the login page of PRTG Network Monitor. In the footer section, it also shows "PRTG Network Monitor 18.1.37.13946".

{{< webpimg "/art-img/as-htb-netmon-5prtglogin.png" "The login page of PRTG Network Monitor. We still need credentials." >}}

As always, version information is valuable for attackers. So, we are looking for publicly-known vulnerabilities in this version. Of course, there is no guarantee that the web application is vulnerable. Our favorite search engine lists "PRTG < 18.2.39 Command Injection Vulnerability" and "PRTG Network Monitor 18.2.38 - (Authenticated) Remote Code Execution". Both vulnerabilities look interesting but we need credentials for the administrator of PRTG Network Monitor to exploit them.

One way is to use brute force. The Burp Suite provides the "Intruder" (attack type "Sniper") for this purpose. This may work if the administrator chose a weak or known password. However, brute-force attacks can become extremely time-consuming.

The better way is to find any cleartext credentials since we are already able to access the remote system using anonymous FTP. Sometimes—and contrary to best practices—web applications store credentials like passwords in cleartext. If we look for "PRTG cleartext credentials" on the internet, we get a hit on Reddit: "PRTG exposes Domain accounts and passwords in plain text." There is an excerpt of an e-mail from Paessler, reporting "[a]n internal PRTG Network Monitor error caused some passwords to be written to the PRTG Configuration.dat file in plain text."

If we look at the documentation of PRTG Network Monitor, we learn that the admin's username is "prtgadmin". The configuration is stored in XML in "PRTG Configuration.dat". The data directory on Windows Server 2008 or 2012 is "%programdata%\Paessler\PRTG Network Monitor". Let's go back to FTP to check this folder:

1. `ftp ish-netmon.htb`
2. `cd "ProgramData\Paessler\PRTG Network Monitor"`
3. `dir`

Interestingly, there are three configuration files: "PRTG Configuration.dat", "PRTG Configuration.old", and "PRTG Configuration.old.bak". We retrieve all of them using `get`. Could one of these files contain the password as mentioned in the Reddit post?

In "PRTG Configuration.dat" and "PRTG Configuration.old", we look for "password" and only find `<encrypted/>`. However, in "PRTG Configuration.old.bak", we get `<!-- User: prtgadmin --> PrTg@dmin2018`. Bingo!

{{< webpimg "/art-img/as-htb-netmon-6prtgpass.png" "The password 'PrTg@dmin2018' is actually included in a backup file." >}}

We go back to the web browser and enter username `prtgadmin` and password `PrTg@dmin2018`, resulting in "Your login has failed. Please try again!". However, the admin was lazy. We enter `PrTg@dmin2019` and have full admin access to PRTG Network Monitor.

## Step 5: Exploiting CVE-2018-9276 in PRTG Network Monitor {#s5}
So far, we retrieved the "user.txt" file, got admin access to PRTG Network Monitor, and we know that the installed version is likely vulnerable to CVE-2018-9276 as described in "PRTG < 18.2.39 Command Injection Vulnerability" and "PRTG Network Monitor 18.2.38 - (Authenticated) Remote Code Execution".

The blog post on codewatch.org ("PRTG < 18.2.39 Command Injection Vulnerability") describes that you can simply add arbitrary commands to a file name in the notifications settings. Normally, PRTG Network Monitor should only execute the file, but the vulnerability allows injecting commands.

What do we need? We already know that there is a folder "Administrator" and it looks like the "root.txt" is oftentimes directly in the "Desktop" folder (as written in many other walkthroughs). So, the path to the "root.txt" is likely "C:\Users\Administrator\Desktop\root.txt". We also know that there is PowerShell running on the server. So, maybe we only need a PowerShell script that copies the "root.txt" file to a folder that can be accessed using FTP (like Users\Public).

According to docs.microsoft.com, there is the command "Copy-Item". We have to use the following syntax: `Copy-Item [-Path] <String[]> [[-Destination] <String>]`. So, our script looks like `Copy-Item [path\root.txt] -Destination [accessible-path]`.

We need to go to the notifications settings on our web browser, as described in the blog post on codewatch.org:

1. Click "Setup"
2. Click "Notifications" in "Account Settings"
3. Click "Add new notification"
4. Enable "Execute Program"
5. Select "Demo exe notification - outfile.ps1" as the "Program File"
6. Enter `test.txt; Copy-item "C:\Users\Administrator\Desktop\root.txt" -Destination "C:\Users\Public\root.txt"` as the "Parameter"
7. Then, switch "Notification Summarization" to "Always notify ASAP, never summarize"
8. "Save" the notification
9. Click "Send test notification"

{{< webpimg "/art-img/as-htb-netmon-7prtgsettings.png" "We have to create a useless notification to exploit the vulnerability. The notification actually executes an injected command (our PowerShell script)." >}}

In theory, this should create a test notification. Then, the test notification should execute the program, resulting in execution of our PowerShell script. The script copies "root.txt" to a folder that is accessible via FTP.

So, we have to check if it actually worked. We switch back to FTP `ftp ish-netmon.htb` and go to the accessible folder as before `cd Users/Public`. Then we use `dir`. Now, there is a file called "root.txt". We `get` it and look at its contents. It's actually the root flag. Done.

* "user.txt": `d…5`
* "root.txt": `3…c`

"_Everything has beauty, but not everyone sees it._" — Confucius

{{< mastodonbox >}}

## Summary
In this walkthrough, we showed one way to own "Netmon" using FTP anonymous access and command injection. There may be other ways to own the machine. [As shown in step 1]({{< relref "#s1" >}}), there are more open ports (135, 139, 445). Read other walkthroughs on the internet to learn about alternative ways to own the machine.

"Netmon" demonstrates the risks of anonymous FTP access and outdated software running on a public server. If you manage any servers, either disable FTP anonymous login or disable FTP completely and switch to secure alternatives. Furthermore, always keep your software up-to-date and delete unnecessary server-side backup files.
