+++
title = "Home network security – Part 3a: Turris Omnia as network-attached storage via SMB"
author = "Benjamin"
date = "2018-08-10T21:15:55+02:00"
tags = [ "turris-omnia", "router", "lan", "nas", "privacy" ]
categories = [ "Home network security" ]
ogdescription = "In this part, we show you how you can use your Turris Omnia as network-attached storage."
slug = "hns3-nas"
banner = "banners/as-hns"
+++

In this part of our [home network security series]({{< ref "/as-hns.md" >}}), we will show you how you can use your Turris Omnia as network-attached storage (NAS) via SMB. This is very useful since your data is kept within your home network. You can share files with your family members and other devices at home. There is no need for flash drives or online storage.
<!--more-->
## Contents
1. [Requirements]({{< relref "#requirements" >}})
2. [Step by step to your NAS using SMB]({{< relref "#sbs-guide" >}})
  * [Step 1: Connecting hardware]({{< relref "#s1" >}})
  * [Step 2: Format the mSATA drive and create a file system]({{< relref "#s2" >}})
  * [Step 3: Create a new user for NAS access]({{< relref "#s3" >}})
  * [Step 4: Check mount points and create a network share]({{< relref "#s4" >}})
  * [Step 5: Configure your clients]({{< relref "#s5" >}})
3. [Troubleshooting]({{< relref "#troubleshooting" >}})
  * [Can't relocate my WLAN modules due to short antenna cables]({{< relref "#antenna" >}})
  * [No connection available]({{< relref "#offline" >}})
  * [No permissions]({{< relref "#permissions" >}})
4. [Summary]({{< relref "#summary" >}})
5. [Sources]({{< relref "#sources" >}})

{{< rssbox >}}

## Requirements
This time, we need:

* our Turris Omnia
* an mSATA drive
* a screwdriver (cross-head)
* an SSH client on our computer

{{< img "/art-img/as-hns3-1overview.jpg" "Our Turris and an mSATA drive." >}}

## Step by step to your NAS using SMB {#sbs-guide}
Now we will physically connect the mSATA drive, format and configure it, create a user for NAS, configure the Omnia to use the drive properly and configure clients to connect with the shared drive.

### Step 1: Connecting hardware {#s1}
{{< img "/art-img/as-hns3-2ot.jpg" "The mainboard of our Turris Omnia before relocating the modules and installing the mSATA drive." >}}

The first step is pure manual labor this time:

* Disconnect **all** cables from your Omnia
* Remove the three antennas
* Use the screwdriver to unscrew your Omnia
  * Remove the 6 screws which fix the case
  * Remove the 4 screws which secure the WLAN modules
  * Remove the 5 screws which fix the mainboard of the Omnia
  * Relocate the bolt nuts
  * Switch the position of your WLAN modules
  * Connect the mSATA to the slot in the middle
  * Screw the mainboard onto the base plate
  * Secure all modules (6 screws)
  * Reposition the case and screw it onto the lower part
* Reconnect all cables with the Omnia
* Restart the Omnia

Thankfully, there is an **official video guide** showing you all of these steps visually: [Turris Omnia: How to connect an mSATA disk]({{< relref "#sources" >}}).

{{< img "/art-img/as-hns3-3m.jpg" "The smaller WLAN modules is switched to the left and the mSATA drive is installed on the right." >}}

### Step 2: Format the mSATA drive and create a file system {#s2}
After crafting, we must format the drive and create a file system. To do so, use the terminal on your computer and connect to your Omnia using SSH: `ssh root@192.168.1.1` Use the password for "advanced administration" like [configured in the first part]({{< ref "/blog/hns1-hello-world.md#config-to" >}}) of this series. After that, use fdisk and mkfs:

{{< highlight bash "linenos=table,linenostart=1" >}}
#! /bin/bash
# Check if /dev/sda is really the mSATA disk
fdisk -l /dev/sda

# Enter interactive mode of fdisk
fdisk /dev/sda
# Press "p", then "n" and use the defaults to create a new partition
# Write this configuration by finally pressing "w"

# Create an ext4 file system (Linux)
mkfs.ext4 /dev/sda1
{{< / highlight >}}

### Step 3: Create a new user for NAS access {#s3}
We can conduct another step while we are still connected with our Omnia. Let's create a new user which will be used later to access our NAS. Change [username] accordingly.

{{< highlight bash "linenos=table,linenostart=1" >}}
#! /bin/bash
useradd --home-dir /var --gid 65534 --no-create-home --shell /bin/false [username]
smbpasswd -a [username]
# Enter the password for this user twice

exit
{{< / highlight >}}

You can close your terminal.

### Step 4: Check mount points and create a network share {#s4}
Now, use your web browser to connect to LuCI on your Omnia. We have to check/configure:

* **mount points**
  * Go to System / Mount Points
  * Scroll down to the "Mount Points" section and add the new drive (using /mnt/sda1 as custom mount point)
  * Save & Apply your settings
  * Reboot the Omnia
  * Check whether your configuration looks like the one in the picture below

{{< webpimg "/art-img/as-hns3-4mounts.png" "Overview of mounted file systems and mount points." >}}

* **network shares**
  * Go to Services / Network Shares
  * Enter a hostname and description and change "share home-directories" to 0
  * Scroll down to "Shared Directories"
  * Enter a **name** (this name is part of the link later to access this directory, e.g., smb://192.168.1.1/**name**)
  * Choose the path which you set as mount point before
  * Enter the username which you configured in [step 3]({{< relref "#s3" >}}) as "allowed users"
  * Save & Apply your settings

{{< webpimg "/art-img/as-hns3-5shares.png" "Overview of network shares." >}}

* idle mode (optional)
  * Go to Services / hd-idle
  * Enable hd-idle for "sda"
  * Save & Apply your settings
* **services**
  * Go to System / Startup
  * Check whether "samba" and "hd-idle" (when enabled before) are enabled and started

Done. Reboot your Omnia once again.

### Step 5: Configure your clients {#s5}
Finally, you can configure each client to connect to your NAS via SMB. This is client-depended, however, we show you one example using Gnome on Arch Linux:

1. Install `sudo pacman -S gvfs-smb`
2. Press `Ctrl + l` and enter smb://192.168.1.1/name (change the IP address and folder name accordingly)
3. Enter the username set before and its password
4. Now you should be able to access your mSATA

## Troubleshooting
Do you need help? See below for quick fixes:

### Can't relocate my WLAN modules due to short antenna cables {#antenna}
While the official video shows no need to relocate antenna cables, our Omnia made it hard to relocate the WLAN modules without switching two cables. Simply disconnect the short one and switch it with one of the other module. You can switch them freely.

### No connection available {#offline}
If you can't connect to your NAS, use nmap on your client to see if the samba service is up and running: `sudo nmap -p 139 -sT "192.168.1.1"`

If the service is up and running, the output should contain `139/tcp open  netbios-ssn`. Recheck your folder name and user name if you still can't connect.

{{< mastodonbox >}}

### No permissions {#permissions}
If you only have read access, create a new folder on your NAS using SSH like before and change the owner of this folder to the NAS user:

{{< highlight bash "linenos=table,linenostart=1" >}}
#! /bin/bash
cd /mnt/sda1
mkdir [folder]
chown [username] [folder]
{{< / highlight >}}

{{< hnsbox >}}

## Summary
If you are a frequent Linux user, this guide should be very easy to implement. If you don't want to use SMB or this guide is to difficult, we will show you how to configure Nextcloud for local usage [in part 3b of this series]({{< ref "/blog/hns3-nextcloud.md" >}}).

Whatever the case may be, this NAS is a big plus for privacy and you don't have to spend a lot of money for a dedicated NAS station.

## Sources
* {{< extlink "https://www.youtube.com/watch?v=71_M2N3ga7s" "YouTube guide: Turris Omnia: How to connect an mSATA disk" >}}
* {{< extlink "https://doc.turris.cz/doc/en/howto/nas" "Turris doc: Home NAS" >}}
