+++
title = "Pros and cons of online assessment tools for web server security"
author = "Jakub"
date = "2018-05-01T11:31:17+02:00"
lastmod = "2019-10-19"
tags = [ "privacy", "server-security", "web-server", "assessment", "webbkoll", "hardenize", "cryptcheck", "observatory", "privacyscore" ]
categories = [ "limits" ]
ogdescription = "There are several online assessment tools available. We show you their pros and cons."
slug = "online-assessment-tools"
banner = "banners/online-assessment-tools"
+++

Maybe you already use the Observatory by Mozilla, Webbkoll, SSL Labs or other online assessment tools to get a first expression of the security of a website. All of these tools are easy to use, however, their possibilities to assess the security of a website and especially of a web server are limited. We show you pros and cons of several tools in this article.
<!--more-->
## Contents
1. [General note]({{< relref "#general-note" >}})
2. [Online assessment tools]({{< relref "#oat" >}})
  * [Observatory by Mozilla – overview]({{< relref "#obm" >}})
  * [Hardenize – another overview]({{< relref "#hardenize" >}})
  * [Qualys SSL Labs – certificate details]({{< relref "#ssllabs" >}})
  * [High-Tech Bridge – detailed security configuration]({{< relref "#htbridge" >}})
  * [CryptCheck – detailed cipher suites]({{< relref "#cryptcheck" >}})
  * [securityheaders.com – security headers only]({{< relref "#securityheaders" >}})
  * [CSP Evaluator – assess CSPs]({{< relref "#cspe" >}})
  * [Webbkoll – connections and cookies]({{< relref "#webbkoll" >}})
  * [PrivacyScore – assessment mix]({{< relref "#privacyscore" >}})
  * [Web Cookies Scanner – information about cookies]({{< relref "#webcookies" >}})
3. [Summary]({{< relref "#summary" >}})
4. [Links]({{< relref "#links" >}})
5. [Changelog]({{< relref "#changelog" >}})

{{< rssbox >}}

## General note
First of all, there is **no** tool which provides a holistic view of the security level of a server. Many important aspects can only be tested when you can directly access the web server and check its configuration files. For example, most tools don't check if the installed web server (Apache, nginx, etc.), PHP packages, database software or operating system (Debian, Ubuntu, etc.) are up-to-date. This is really important, though.

Furthermore, a server-side web application firewall may block some or all tests of online assessment tools. This results in partially or totally wrong output. For instance, some of the tools mentioned below state that we requested to be excluded while others show that HTTP response headers are missing (both is wrong).

If you are a server administrator, you should also look for internal testing (e.g., nmap, openssl, sslyze and many more cli tools available). Do not solely rely on the following tools!

## Online assessment tools {#oat}
All of the following 9 online assessment tools can be used in the web browser without installing software on your client. You only have to enter the URL of the website you want to be assessed.

### Observatory by Mozilla – overview {#obm}
The Observatory by Mozilla has the following features:

* It checks the [Content Security Policy]({{< ref "/glossary.md#csp" >}}) (CSP)
* It checks security-relevant HTTP response headers (e.g., HSTS)
* It checks cookie configuration, HTTPS redirection, referrer policy and so on
* It checks [TLS]({{< ref "/glossary.md#tls" >}}) configuration, [CAA]({{< ref "/glossary.md#caa" >}}), [AEAD]({{< ref "/glossary.md#aead" >}}) and [PFS]({{< ref "/glossary.md#perfect-forward-secrecy" >}})
* It checks for [OCSP stapling]({{< ref "/glossary.md#ocsp" >}}) and cipher preference
* It shows other online assessment tools for further assessment

You have to keep in mind:

* HTTP Observatory:
  * Websites can get up to 135 points. A rating of more than 100 points means that you see a green A+. The problem: You get 10 bonus points for secure cookies (HttpOnly, Secure, SameSite) and [Subresource Integrity (SRI)]({{< ref "/glossary.md#subresource-integrity" >}}). You don't get these points if there are no cookies at all and there is no reason for implementing SRI.
  * X-Frame-Options are assessed by checking the CSP. If undefined in the CSP, the Observatory looks for a header. So, do you need an extra header at all if you set this in your CSP? Yes, because some assessment tools and web browsers only look for the header. Make sure that `frame-ancestors` (CSP) and your X-Frame-Options header have the same configuration.
  * Websites get 5 bonus points for being "Preloaded via the HTTP Strict Transport Security (HSTS) preloading process". Nowadays, mainstream web browsers are shipped with a built-in list containing websites which have to be loaded via HTTPS. Administrators can add their website to the list (HSTS preload list, [link below]({{< relref "#links" >}})). However, after a website is added to the list it can take a long time until the Observatory recognizes this. It is more reliable to directly check the HSTS preload list for your website.
* TLS Observatory:
  * It doesn't recognize TLS 1.3 at the moment.
  * The TLS Observatory doesn't recognize modern [cipher suites]({{< ref "/glossary.md#cipher-suites" >}}) with ChaCha20-Poly1305. They don't appear on the list. It only recognizes "CHACHA20-POLY1305-OLD".
  * There is additional (more technical) information when you click on Scan Summary → Certificate Explainer.
* SSH Observatory:
  * You can use the SSH Observatory by manually starting the assessment. Please note that the Observatory must be allowed to connect to port 22 of the web server to check the SSH configuration.

### Hardenize – another overview {#hardenize}
Hardenize is another tool to get a first impression and very similar to the Observatory by Mozilla. However, it also checks the mail server, if available. Its main features are:

* It checks DNS records, [DNSSEC]({{< ref "/glossary.md#dnssec" >}}) configuration and CAA
* It checks for TLS (including TLS 1.3) configuration, HTTPS redirection, cookies etc.
* It checks security features like MTA-STS, TLS-RPT, DANE, SPF and DMARC of the mail server
* It checks for [Certificate Transparency]({{< ref "/glossary.md#certificate-transparency" >}}) and cipher preference
* It checks for a Expect-CT header and submits a test report to the provided report-uri
* It checks provided IPv4 and IPv6 addresses of the domain

You have to keep in mind:

* You should click on every single check, because there are sometimes additional hints for better configuration or warnings due to bad configuration.
* The CSP check recommends to set either `block-all-mixed-content` or `upgrade-insecure-requests`. We think that this is unnecessary if you don't embed external content and your website is preloaded (HSTS).
* It shows that Subresource Integrity (SRI) is required even if files are provided by the same web server (e.g., you check xyz.cz which embeds resources of xyz.sk, but both domains are hosted on the same web server).
* It recommends HPKP while many security professionals hold the belief that you shouldn't implement this anymore.
* Only cipher suites supporting PFS and AEAD are showed in green color. If you are an administrator, you should ensure that you only provide cipher suites which are green.

### Qualys SSL Labs – certificate details {#ssllabs}
SSL Labs mainly checks certificates and TLS configuration of a web server. Its main features are:

* Certificate:
  * It evaluates Extended Validation, Certificate Transparency, OCSP Must-Staple and CAA.
  * It checks revocation status of certificates.
* TLS protocols:
  * It supports SSL (2, 3) and all TLS versions (1.0–1.3).
* Cipher suites:
  * It checks for PFS and AEAD. Cipher suites using CBC are considered "weak".
* General information:
  * It shows if the server exposes its signature (however, this isn't evaluated).
  * It checks whether a website is on the HSTS preload list.
  * It checks for several vulnerabilities like POODLE, GOLDENDOODLE, 0-Length OpenSSL, and Sleeping POODLE.
* SSL Labs checks all IPv4 and IPv6 addresses which belong to the domain name.

Interestingly, SSL Labs recognizes websites which were recently added to the HSTS preload list much more faster than other tools.

### High-Tech Bridge – detailed security configuration {#htbridge}
High-Tech Bridge's "ImmuniWeb" offers two different tests for free:

#### ImmuniWeb SSL Security Test
* This test evaluates certificates and ciphers suites including TLS 1.3.
* It clearly visualizes the certificate chain(s).
* It evaluates Extended Validation, Certificate Transparency, and OCSP Must-Staple.
* It shows supported elliptic curves (including X25519).
* It checks for DNS CAA, Expect-CT, TLS 1.3 Early Data, cipher suite preference, HTTPS redirect, HSTS, and more.

You have to keep in mind:

* Missing support for TLS 1.3 is considered as "misconfiguration or weakness".
* If the server has no preference for cipher suites, it is considered as "misconfiguration or weakness". ImmuniWeb states "The server does not prefer cipher suites. We advise to enable this feature in order to enforce usage of the best cipher suites selected." This advice is only correct if the server offers legacy cipher suites (cipher suites without support for PFS/AEAD) additionally to modern ciphers suites (supporting AEAD and PFS). The goal is to prevent client-side [downgrade attacks]({{< ref "/glossary.md#downgrade-attack" >}}). If a web server only offers modern cipher suites, it isn't recommended to set server preference anymore.
* This test checks for compliance with PCI DSS, HIPAA and NIST. HIPAA and NIST guidelines don't accept ECDSA certificates signed by RSA intermediates and require you to disable cipher suites using ChaCha20-Poly1305. If you don't have to comply with these standards, ignore the checks.
* In comparison with most of the other tools in this article, the rating of cipher suites is somewhat wishy-washy. For example, TLS 1.0 and TLS 1.1 are still considered as "good protocol compatibility, allowing users with older browsers to access your website." Our recommendation is to disable all TLS versions before 1.2.
* The test still mentions HPKP (HTTP Public Key Pinning). HPKP is outdated and most web browsers don't support it anymore. So, don't implement it.

#### ImmuniWeb Website Security Test
* This test evaluates enabled HTTP methods, ALPN (part of HTTP/2) and more.
* It also checks if the web server exposes its server signature and tries to recognize software used on the web server (jQuery, Bootstrap, etc.). This scan can also detect WAFs (web application firewalls).
* It checks for PCI DSS compliance (irrelevant for many private websites).
* It evaluates the CSP and shows tips for better configuration.
* It evaluates HSTS, Expect-CT and other response headers and shows misconfiguration.
* It analyzes cookies (HttpOnly, Secure, SameSite).
* It shows third-party content and the corresponding TLS security level of these external connections (even if the content is on the same web server).

You have to keep in mind:

* If you run a web application firewall, the test for different HTTP types (like TRACE, POST, DELETE, …) gets confused and shows every HTTP type as enabled. This result is wrong.
* NPN (Next Protocol Negotiation) was a draft for the SPDY protocol which isn't in use nowadays. When you use HTTP/2, you should enable ALPN and disable NPN. Most web browsers already dropped support for NPN.
* Testing websites with this scanner is really noisy and probably triggers web application firewalls. This leads to wrong testing results (as mentioned above).

### CryptCheck – detailed cipher suites {#cryptcheck}
CryptCheck is like a magnifier for TLS and cipher suites. Its features are:

* It checks algorithms used for key exchange, [authentication]({{< ref "/glossary.md#authentication" >}}), encryption and [MAC]({{< ref "/glossary.md#mac" >}}) in detail.
* It checks for PFS and HSTS and shows supported TLS protocols.
* It rates the strength of keys.
* It checks all IPv4 and IPv6 addresses belonging to the domain name.

You have to keep in mind:

* It doesn't recognize TLS 1.3 or AEAD at the moment.
* We didn't find any explanation for colors in use by CryptCheck. Our guess is:
  * blue: modern and good value, secure
  * green: good value, secure
  * yellow: bad value, possibly insecure
  * red: bad value, insecure
  * black: bad value, extremely insecure
  * gray: ?
* CryptCheck uses the HTTP method HEAD to check for HSTS headers. If you have disabled this in your server configuration, `HSTS` and `HSTS_LONG` won't be recognized by CryptCheck. This results in lower rating of a website.
* If you configured IPv6 (AAAA record) and your web server is only available via IPv4, CryptCheck won't recognize `HSTS` and `HSTS_LONG`.
* CryptCheck seems to recognize only one certificate. If you deployed two (e.g., RSA and ECDSA), you will only see one of them.
* As an administrator, you should immediately get rid of all entries which aren't blue, green or yellow. Furthermore, you should check if yellow entries are really necessary. Cipher suites using CBC should also be disabled (vulnerable to Lucky 13 attack and no AEAD). SHA-1 and MD5 aren't colored, but you shouldn't use both, too.
* You have to disable AES-128 if you want to get a 100% rating. We don't see any reasons to do so.
* CryptCheck can also check the SSH configuration of a server. Of course, you have to allow CryptCheck to connect with port 22 of your web server.

### securityheaders.com – security headers only {#securityheaders}
securityheaders.com rates the same response headers of a website as shown before: CSP, referrer policy, HSTS, X-Xss-Protection, X-Frame-Options, X-Content-Type-Options, Expect-CT, Expect-Staple, Feature-Policy, Network Error Logging and Report-To. It also shows all headers send by the web server (like the Observatory by Mozilla).

Since July 2019, securityheaders.com doesn't recommend using "X-Xss-Protection" anymore since major web browser don't support this header by default, or they are about to remove support for it. Furthermore, one should primarily use the CSP directive "frame-ancestors" instead of setting a "X-Frame-Options" header.

Additional checks are:

* It checks whether the web server exposes its software manufacturer and version information.
* It checks for the Expect-CT header. This header can be used to log errors if Certificate Transparency is deployed.
* It checks for the Expect-Staple header. This header can be used to log errors if OCSP Must-Staple is deployed.
* It checks for a Feature Policy. Feature Policies are very new, not widespread and allow admins to enable/disable certain features of web browsers. However, a web server needs a Feature Policy to achieve the best rating since July 2019.
* It checks for a NEL header. The Network Error Logging (NEL) header defines a mechanism enabling web applications to declare a reporting policy that can be used by the user agent to report network errors for a given origin. The logged network errors even include problems occurred before a connection was successfully established.
* It checks for a Report-To header. The Report-To header is defined in the Reporting API. It aims to provide a best-effort report delivery system that executes out-of-band with website activity and introduces new report types.

### CSP Evaluator – assess CSPs {#cspe}
The CSP Evaluator by Google can only evaluate the Content Security Policy of a website. It shows bad configuration and improvement tips. You can enter a URL or a CSP.

### Webbkoll – connections and cookies {#webbkoll}
Some people consider Webbkoll as the ultimate privacy tool. They enter a URL, click on check, wait several seconds and are happy if there are lots of zeros and green check marks. However, Webbkoll has several drawbacks which we already discussed in another article: "[Limits of Webbkoll]({{< ref "/blog/limits-webbkoll.md" >}})". Its main features are:

* It presents you information about HTTPS configuration, [HSTS]({{< ref "/glossary.md#hsts" >}}), [CSP]({{< ref "/glossary.md#csp" >}}), Referrer Policy, [SRI]({{< ref "/glossary.md#subresource-integrity" >}}), localStorage and other security-relevant HTTP response headers.
* It lists third-party requests and the approximate server location.
* It shows its users GDPR-relevant information.

Please note that the results are only valid for the link you entered. Other pages of the same website can differ ([see three examples]({{< ref "/blog/limits-webbkoll.md#examples" >}})). Webbkoll partially uses code of the [Observatory by Mozilla]({{< relref "#obm" >}}) for its checks.

### PrivacyScore – assessment mix {#privacyscore}
PrivacyScore is developed by German universities and lists four categories:

* NoTrack: Most of these tests are also conducted by Webbkoll. It is also checked if web server and/or mail server are located in the EU.
* EncWeb: These tests include evaluation of response headers, PFS, TLS protocol versions, cipher preference and several attacks on TLS.
* Attacks: This category mainly checks for response headers like securityheaders.com.
* EncMail: These checks are mostly the same as in the EncWeb category, but the mail server is checked.

You have to keep in mind:

* It doesn't recognize TLS 1.3 or AEAD at the moment.
* Like the Observatory by Mozilla, websites recently added to the HSTS preload list aren't recognized for a long time.
* Even web servers with strict configuration can't get 100% ratings. For example, we are vulnerable to the BREACH attack (according to this site) due to activated gzip compression. Most other websites are vulnerable to Lucky 13 due to cipher suites which support CBC instead of AEAD.
* Testing websites with this scanner is really noisy and probably triggers web application firewalls. This leads to wrong testing results. If your web application firewall repeatedly blocks PrivacyScore from scanning your website, it incorrectly shows "_This site is excluded from further scans. The owner of this website has asked us to not perform further scans. For reasons of transparency we display the results of the last scan before we received the request of the owner. The owner may have implemented changes in the meantime, which are not reflected in the results below._" In this case, you can't scan the website anymore.

### Web Cookies Scanner – information about cookies {#webcookies}
The Web Cookies Scanner basically shows the number of third-party domains contacted by the given web page as well as the number of persistent and session cookies set by the given page.

Additionally, it rates the overall "Privacy Impact Score", a "simple indicator of website's usage of potentially privacy intrusive technologies such as third-party or permanent cookies, CANVAS trackers etc". There is also a check of TLS protection, and a check for security-related HTTP headers (X-Frame-Options, HSTS, X-Content-Type-Options, X-XSS-Protection, CSP).

Please note:

* Session cookies and first-party cookies aren't assessed, and aren't part of the Privacy Impact Score.
* As always, it only scans the given web page. So there can be other pages on a web server that actually set cookies.
* The scanner doesn't recognize TLS on our and some other tested websites (error "Certificate path cannot be verified to a known root certificate"). This is likely related to our ECDSA certificate. We assume that it only supports RSA certificates.
* The scanner doesn't recognize that we set the legacy header X-Frame-Options in our CSP (via frame-ancestors).
* The scanner recommends the legacy header X-XSS-Protection.
* The scanner recommends Subresource Integrity for files that are hosted on the same web server. For us, this recommendation doesn't make sense.

{{< mastodonbox >}}

## Summary
Hopefully you got a first impression what most tools can test and what they can't. Many tools evaluate response headers, TLS protocols and cipher suites. Additionally, some tools give extensive information about their rating and tips how admins can improve their security configuration. You should evaluate websites regularly since tests are added, modified or deprecated from time to time.

As mentioned above, you should also always keep in mind that none of these tools (even if you use all of them) can provide a holistic view of the security level of a server. Even scanning a website with every tool mentioned above only offers a very limited insight into security.

## Links
* {{< extlink "https://observatory.mozilla.org/" "Observatory by Mozilla" >}}, {{< extlink "https://github.com/mozilla/http-observatory/blob/master/httpobs/docs/scoring.md" "scoring info" >}}
* {{< extlink "https://www.hardenize.com/" "Hardenize" >}}
* {{< extlink "https://www.ssllabs.com/ssltest/" "SSL Labs" >}}
* {{< extlink "https://www.immuniweb.com/ssl/" "High-Tech Bridge ImmuniWeb SSLScan" >}}
* {{< extlink "https://www.immuniweb.com/websec/" "High-Tech Bridge ImmuniWeb WebScan" >}}
* {{< extlink "https://tls.imirhil.fr/" "CryptCheck" >}}
* {{< extlink "https://securityheaders.com/" "Security Headers" >}}
* {{< extlink "https://csp-evaluator.withgoogle.com/" "CSP Evaluator" >}}
* {{< extlink "https://webbkoll.dataskydd.net/en/" "Webbkoll" >}}
* {{< extlink "https://privacyscore.org/" "PrivacyScore" >}}
* {{< extlink "https://hstspreload.org/" "HSTS Preload List Submission" >}}
* {{< extlink "https://webcookies.org/" "Web Cookies Scanner" >}}

## Changelog
* Oct 19, 2019: Add webcookies.org.
* Jul 27, 2019: Updated the securityheaders.com section, addressing their update from 2019-7-22.
* Jun 30, 2019: Updated High-Tech Bridge products according to their website. Added information regarding an incorrect statement on PrivacyScore if a WAF repeatedly blocks it.
* May 15, 2019: Updated the SSL Labs section according to their announcement in "Zombie POODLE and GOLDENDOODLE Vulnerabilities" (April 2019).
* Dec 11, 2018: Updated this article including Webbkoll's update from 2018-11-30.
* Nov 17, 2018: Updated nearly all feature lists.
* Nov 1, 2018: Updated securityheaders.com's feature list.
* May 26, 2018: Updated High-Tech Bridge products according to their website.
* May 23, 2018: Removed HPKP in Webbkoll section since Webbkoll removed its recommendation due to this article.
