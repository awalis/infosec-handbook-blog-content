+++
title = "Federation myths"
author = "Benjamin"
date = "2018-08-14T09:53:02+02:00"
lastmod = "2019-07-10"
tags = [ "open-source", "federation", "mastodon", "xmpp" ]
categories = [ "myths" ]
ogdescription = "Federation is good and there are many use cases for it. However, it isn't a magic bullet."
slug = "federation-myths"
banner = "banners/federation-myths"
+++

Federated services are popular among people who care about their online privacy. However, there are several myths rattling around which you will read over and over again. We exemplarily discuss Mastodon and XMPP in this article.
<!--more-->
## Contents
1. [Myth 1: Federated services aren't user-friendly]({{< relref "#m1" >}})
2. [Myth 2: Federation is a privacy feature]({{< relref "#m2" >}})
3. [Myth 3: Federation is more important than encryption]({{< relref "#m3" >}})
4. [Myth 4: Oh, you don't trust your admin? Run your own server!]({{< relref "#m4" >}})
5. [Summary]({{< relref "#summary" >}})
6. [Links]({{< relref "#links" >}})
7. [Changelog]({{< relref "#changelog" >}})

{{< rssbox >}}

## Myth 1: Federated services aren't user-friendly {#m1}
People telling you this myth may have tested federated services and decided that federation is just bad: outdated software, no regular updates, many bugs, hard to use, bad user experience. We think **it depends**.

There is actually a very good example for federated services with usability in mind: **Mastodon**. There are regular updates, it's easy to use, more and more people like and use it every day. It looks modern, it feels modern and you don't need additional software when using your web browser. For instance, in order to register for Mastodon you can choose any server (instance), enter your credentials and sign up. That's it.

However, there is also the other side of the coin: **XMPP messaging**. Wait? XMPP? You may have just read that using XMPP is as easy as using e-mail! (By the way, this statement is extremely sketchy and another myth!)

What's wrong here? While some messaging services like WhatsApp use customized XMPP and are very popular, many leisure projects emerged on GitHub and other repository hosting platforms over the years. Sometimes these projects consist of half-baked and buggy code with no updates for months. The biggest problem in this example is that XMPP itself is somewhat standardized but there are dozens of so-called XEPs which introduce new features. Most messengers support basic XMPP but the implementation of XEPs is arbitrary. This leads to a handful of messengers left if you want to use certain features. This subset of messengers may not contain any messengers which run on your cellphone's operating system.

So, finding an appropriate messenger for your operating system which also supports all features you want to use requires research and time. But that's not all. You have to find a server! Yes, there are websites that list XMPP servers. Yes, some clients even suggest servers. Yes, there are blogs around telling you which server you should use. But there are these XEPs again! Servers also have to support XEPs. You not only have to look for the "right" client but for the "right" server, too. (And many of these XEPs are "experimental proposals" or "drafts", not standards!) Then, strong advocates of the XMPP community come up with dozens of guides to tell you how you have to use XMPP. Finally, you found a server, registered and the next update of the server or client makes chatting impossible for days due to protocol issues.

Frankly speaking, nobody can seriously call this "user-friendly" and this is only the tip of the iceberg. No one wants to read dozens of articles, guides and learn about network protocols only to be able to send a message. You can spend hours to configure your XMPP setup only to realize that feature x is not yet implemented. (XMPP has more downsides which we won't discuss in this article.)

In sum, some federated services are as user-friendly as any other non-federated services but some are a PITA.

## Myth 2: Federation is a privacy feature {#m2}
We read this over and over again: Federation automatically means privacy! Why? You may well ask since there is no categorical answer. Federated services use ordinary servers, ordinary network protocols, ordinary software. There is no special privacy feature implemented. _Federation_ just means that different clients, servers and networks understand each other. On the other hand, _decentralization_ means that there is no central authority which controls all servers in a (federated) network.

Does this mean privacy? **We don't think so.**

First of all, most federated services like Mastodon and XMPP require registration in order to use it. This normally means that you have to provide more or less [personal data]({{< ref "/glossary.md#personal-data" >}}): your username, your e-mail address, your IP address. Then, this data is stored somewhere on the internet and mostly in cleartext.

Secondly, there is [metadata]({{< ref "/glossary.md#metadata" >}}): timestamps of your logins, timestamps of certain actions like sending or reading messages, amount of messages sent, size of data sent. Federation doesn't mean that there is more or less metadata in comparison with non-federated services.

Thirdly, there are companies which control large subsets of servers and companies which provide internet access for users of these federated services. These companies can clearly see who connects to which server, how often and so on. Using federated services doesn't mean that your traffic is invisible. We showed this for more than 1000 XMPP servers in July 2019: More than 50% of XMPP servers were controlled by only 7 companies in 3 countries. Besides, 50% of XMPP servers were hosted in Germany, 10% in the USA, and 7% in France. This clearly shows the problem of physical centralization of federated services.

Fourthly, there is the server. Who controls this server? Who can access your data stored on this server? Does the admin comply with current data protection laws? Did the admin harden the server's configuration and is the server's software kept up-to-date? You likely don't know and using [online assessment tools]({{< ref "/blog/online-assessment-tools.md" >}}) to check this covers only a small fraction of security measures.

Let's return to our exemplary services: Mastodon and XMPP. Most of your data is stored in cleartext on the servers. This is normal and doesn't differ from non-federated services. Mastodon requires a valid e-mail address and some XMPP servers also need you to provide one.

The **big difference** here is that you don't have to trust a global player like Google or Facebook but smaller companies, friendly societies or individuals.

In sum, federation shifts trust from one institution to another. Federation is about trusting other parties. You hope that admins don't access or sell your data. You hope that they don't merge their federated instance with global players anytime soon.

## Myth 3: Federation is more important than encryption {#m3}
Most people who spread myth 2 ("Federation is a privacy feature") also spread this myth. They are telling you that encryption is not that important. Using federated and decentralized services is most important!

This is another extremely sketchy statement. Let's differentiate between

* transport encryption (encrypting traffic between your client and your server)
* server-side encryption at rest (encrypting your data on the server if not in use)
* end-to-end encryption (encrypting traffic between your client and the client your dialog partner)

We think you agree that end-to-end encryption is the supreme discipline here. End-to-end encryption which uses state-of-the-art cryptographic algorithms ensures that you don't have to trust any server between you and your dialog partner. Enforced end-to-end encryption supporting [perfect forward secrecy]({{< ref "/glossary.md#perfect-forward-secrecy" >}}) even ensures that nobody can access encrypted data anytime soon as long as the endpoints won't be compromised.

Transport encryption only ensures partial security even if modern encryption and PFS are in use. A compromised server puts your data at risk. Server-side encryption reduces this risk but doesn't eliminate it and you can hardly check whether server-side encryption is permanently enabled or supported at all. There may be unencrypted backups or configuration issues that effectively disable server-side encryption.

Does federation on its own provide this level of security? No. Federation is about interoperability of protocols, clients and networks not about security like mentioned before. This myth is **wrong**. Yes, there are federated services without use cases for end-to-end encryption but transport encryption is important nearly everywhere.

## Myth 4: Oh, you don't trust your admin? Run your own server! {#m4}
This is half a myth and mostly stated to bid defiance: Federation advocates tell you that federation is a privacy feature, you insist on the opposite due to [reasons mentioned above]({{< relref "#m2" >}}) and then they tell you: "Oh, you don't trust your admin? Run your own server!".

Running your own server isn't a magic bullet either. You not only have to rent a server, you also have to install server software, harden your server, implement security measures, regularly update its software and keeping it secure. This implies that you understand web server security, check log files and monitor your server permanently.

However, your own server doesn't guarantee that your data stays on your server when using federation. For instance, some Mastodon instances mirror messages of other instances. Moreover, people can share your content, of course. When using XMPP, some of your account data is shared with other servers to be able to chat. Furthermore, your server provider most likely logs your server usage and is able to track who connects to your server.

In summary, you get a little more control over your data and metadata but you have to maintain your own server and pay for it.

{{< mastodonbox >}}

## Summary
You will read these myths over and over again. Discussions turn into matters of faith almost every time. You will read that federation implies less tracking, more control and everything is so easy.

Actually, the biggest advantage for users of federated services is higher [availability]({{< ref "/glossary.md#availability" >}}) if users actually use many different decentralized servers. In this context, "availability" doesn't necessarily mean that everybody can use this federated service all the time, but it is much harder to shut down every server of the whole network at the same time. This likely implies censorship resistance.

On the other hand, federation neither automatically means more security nor privacy.

## Links
* {{< extlink "https://gist.github.com/infosec-handbook/19f96587511d6d8ab79b564a7e0b3bdf" "List of hosters of 1000 XMPP servers (github.com)" >}}

## Changelog
* Jul 10, 2019: Added information about 1000 XMPP servers that are physically centralized.
