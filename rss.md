+++
title = "RSS/Atom"
ogdescription = "Information about our RSS/Atom feeds."
nodateline = true
noprevnext = true
notice = true
+++

We provide the following RSS/Atom feeds:

* [Blog articles](/blog/index.xml) (full-text feed)
* {{< extlink "https://mastodon.at/users/infosechandbook.atom" "InfoSec news (Atom feed)" >}} via mastodon.at
* [Changelog](/changelog.xml)

{{% notice info %}}
You do not need a Mastodon account to read our InfoSec news on mastodon.at. Use the link above to read our InfoSec news in your RSS/Atom feed reader. Alternatively, go directly to {{< extlink "https://mastodon.at/@infosechandbook" "infosechandbook on Mastodon.at" >}}.
{{% /notice %}}
