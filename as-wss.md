+++
title = "Web server security series"
ogdescription = "The Web server security series shows you how you can secure your web server (using Debian and Apache)."
nodateline = true
noprevnext = true
+++

<picture>
  <source srcset="/img/as-wss.png.webp" type="image/webp">
  <source srcset="/img/as-wss.png" type="image/png">
  <img src="/img/as-wss.png" alt="Web server security series" title="Web server security series"/>
</picture>

In this series, we show ways to secure your web server. We will use **Debian 9 and Apache httpd 2.4.25** in our examples, however, you can convert most configuration to other operating systems or web servers.

1. [Part 0: How to start]({{< ref "/blog/wss0-how-to-start.md" >}})
2. [Part 1: Basic hardening]({{< ref "/blog/wss1-basic-hardening.md" >}})
3. [Part 2: Harden the web server]({{< ref "/blog/wss2-webserver-hardening.md" >}})
4. [Part 3: TLS and security headers]({{< ref "/blog/wss3-tls-headers.md" >}})
5. [Part 4: WAF ModSecurity and IPS Fail2ban]({{< ref "/blog/wss4-modsecurity-fail2ban.md" >}})
6. [Part 5: Server-side DNS security features]({{< ref "/blog/wss5-dns-configuration.md" >}})
7. [Part 6: GDPR-friendly logging, and server monitoring]({{< ref "/blog/wss6-logging-monitoring.md" >}})
8. [Part 7: Policies, and security contact]({{< ref "/blog/wss7-policies-contact.md" >}})
9. [Part 8: Basic log file analysis]({{< ref "/blog/wss8-log-file-analysis.md" >}})

Upcoming parts of this series will be about getting ECDSA certificates, tools for server monitoring, certain HTTP response header directives, cookie security, and more.
