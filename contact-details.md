+++
title = "Contact information"
ogdescription = "Our contact information"
nodateline = true
noprevnext = true
notice = true
chip = true
+++

[E-mail]({{< relref "#e-mail" >}}) | [Encrypted e-mail]({{< relref "#encrypted-e-mail" >}}) | [Alternative ways to contact us]({{< relref "#alternatives" >}})

## E-mail
If you voluntarily send us an e-mail, **you consent that our mail provider processes your personal data**. Please read our [privacy policy]({{< ref "privacy-policy.md#emailprovider" >}}) (the "Tutao GmbH" section).

{{< chip "../img/mail-icon.png" "E-mail icon" "ish-team@tuta.io" "mailto:ish-team@tuta.io" >}}

## Encrypted e-mail
If you know how to use [GnuPG]({{< ref "/glossary.md#gnupg" >}}) (or compatible implementations of OpenPGP), use our public GPG key to encrypt the content of your e-mail. You don't need to sign your e-mails, so you don't have to create a GPG key to communicate with us. If you want to get encrypted replies, please provide your public GPG key or fingerprint. Have a look at our [short guide]({{< ref "/terminal-tips.md#gpg-gen" >}}) to create a modern [Curve25519]({{< ref "/glossary.md#curve25519" >}}) key.

{{< chip "../img/mail-enc-icon.png" "Encrypted e-mail icon" "Our public GPG key" "https://infosec-handbook.eu/gpg.asc" >}}

The current fingerprint of our public GPG key is:

valid from | valid to   | Fingerprint/**Key ID**
-----------|------------|-----------------------
2019-08-29 | 2019-12-31 | [F8CE AD90 8841 1EBE 722C 347A **3179 E817 703F 5D25**](https://infosec-handbook.eu/gpg.asc)

You can also use curl (or similar tools) to download our key:

* `curl https://infosec-handbook.eu/gpg.asc | gpg --import` (direct download)
* `curl https://keybase.io/infosechandbook/pgp_keys.asc | gpg --import` (Keybase mirror)

<details>
  <summary>Expired and revoked GPG keys (click to extend)</summary>
  {{% notice info %}}
  To reduce risks of long-lived GPG keys circulating on the internet, we regularly renew our key. The table below lists expired and revoked keys.
  {{% /notice %}}

  valid from     | valid to       | Fingerprint/**Key ID**
  ---------------|----------------|-----------------------
  ~~2019-08-22~~ | ~~2019-10-31~~ | ~~F6FB F3E9 9DC0 FD3E FE45 BEDC **6B68 98C3 32D1 70F7**~~
  ~~2019-05-26~~ | ~~2019-08-24~~ | ~~FE38 23F5 7A02 600E F080 9E58 **BCF4 BFD9 4ABF F017**~~
  ~~2019-03-03~~ | ~~2019-06-01~~ | ~~1C3D DFF7 2961 7BFC 7544 3E3B **E545 30DB 4072 2A8A**~~
  ~~2018-12-11~~ | ~~2019-03-11~~ | ~~4726 F48E 5607 B8A6 06CA F4F9 **09D3 4CFA C102 617D**~~
  ~~2018-09-25~~ | ~~2018-12-24~~ | ~~ED21 9FFD 58F0 B467 223A 6312 **D9FE 497B B424 11ED**~~
  ~~2018-06-12~~ | ~~2018-09-10~~ | ~~708C A4E3 8A9B E257 748D 01C9 **172B AD3E 40AA EA08**~~
</details>

## Alternative ways to contact us {#alternatives}
If you don't want to send us an e-mail, you can use below-mentioned options to contact us. Kindly note that using these service providers is out-of-scope of our [privacy policy]({{< ref "privacy-policy.md" >}}). We may add or remove options over time.

* {{< extlink "https://keybase.io/infosechandbook" "Keybase.io" >}}
* {{< extlink "https://mastodon.at/@infosechandbook" "Mastodon/Fediverse" >}}
* Signal: We are available on Signal upon request.
* [Threema](threema://add?id=ZPCM5K45)

If you send us messages via platforms that aren't listed on this page, there is no guarantee that we actually read your message.
