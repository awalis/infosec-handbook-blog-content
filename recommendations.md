+++
title = "Recommendations"
ogdescription = "Hardware, software, services and blogs we recommend."
nodateline = true
noprevnext = true
notice = true
+++

This page contains security-related recommendations. Kindly note that we <i>exclusively</i> recommend hardware, software and services which we actually use and own. We do not recommend any products based on sponsoring or things we only know from hearsay.

[General information security topics]({{< relref "#gst" >}}) | [Home network]({{< relref "#hn" >}}) | [DNS]({{< relref "#dns" >}}) | [Identity management]({{< relref "#idm" >}}) | [Instant messaging]({{< relref "#im" >}}) | [Operating systems]({{< relref "#os" >}}) | [Repositories]({{< relref "#repo" >}}) | [Secure key and password storage]({{< relref "#skps" >}})

{{< mastodonbox >}}

## General information security topics {#gst}
The following resources are useful to learn about InfoSec in general:

### Blogs {#gst-blogs}
* [Scott Helme](https://scotthelme.co.uk/) (InfoSec blog, focused on web application security)
* [n-o-d-e](https://n-o-d-e.net/) (interesting hardware projects)

### Podcasts {#gst-podcasts}
* [Security Now](https://twit.tv/shows/security-now) (weekly podcast with Steve Gibson and Leo Laporte) ([#securitynow](https://mastodon.at/web/timelines/tag/securitynow))
* [StormCast](https://isc.sans.edu/podcast.html) (daily 5-10 minute podcast about current InfoSec topics)

### Q&A websites/forums {#gst-qa}
* [Privacy Forum](https://forum.privacytools.io/) (hosted by privacytools.io; we are also on this forum)
* [Information Security Stack Exchange](https://security.stackexchange.com/) (Q&A website for information security professionals)

### Other useful websites {#gst-misc}
* [EFF Security Education Companion](https://sec.eff.org/) (for digital security educators)
* [EFF Surveillance Self-Defense](https://ssd.eff.org/) (tips, tools and how-tos for more secure online communications)
* [IT and Information Security Cheat Sheets](https://zeltser.com/cheat-sheets/) (cheat sheets on different topics)
* [OWASP Cheat Sheet Series](https://github.com/OWASP/CheatSheetSeries) (cheat sheets on different topics)

---

## Home network {#hn}
Your home network connects you and your family to the internet. The most vulnerable point is your router since it has to fulfill different functions and is the primary point of entry for a remote attacker. Feel free to read our [home network security series](/as-hns/).

### Books {#hn-books}
* [Introducing Basic Network Concepts](https://www3.nd.edu/~cpoellab/teaching/cse40814_fall14/networks.pdf) (PDF file)
* Meyers: CompTIA Network+ Certification, ISBN 978-0-07-184821-3
* Kizza: Guide to Computer Network Security, ISBN 978-3-319-55606-2
* Lowe: Networking for dummies, ISBN 978-1-119-25777-6
* Peterson/Davie: Computer Networks: A Systems Approach ([available online](https://book.systemsapproach.org/))

### Blogs {#hn-blogs}
* [Router security blog by Michael Horowitz](https://routersecurity.org/index.php)

### Hardware {#hn-hardware}
* [Turris Omnia](https://omnia.turris.cz/en/) (open hardware and open source router) ([#turris-omnia](/tags/turris-omnia/))

---

## DNS
Many private users are totally focused on HTTPS, and forget about their insecure DNS traffic. Cleartext DNS traffic can be modified or logged, and third parties can learn about your surfing habits. People who are familiar with network protocols and DNS can configure DNSSEC as well as DNS-over-TLS. If configured correctly, you get validated DNS responses, and your DNS traffic is authenticated and encrypted.

Check our [DNS-related articles](/tags/dns/).

### Websites {#dns-websites}
* [DNS Privacy Project](https://dnsprivacy.org/wiki/) (collaborative open project to promote, implement and deploy DNS Privacy)
* [DNS leak test](https://www.dnsleaktest.com/) (see the DNS server that is used by your client)
* [List of public recursive name servers on Wikipedia](https://en.wikipedia.org/wiki/Public_recursive_name_server)
* [DNS Privacy Public Resolvers](https://dnsprivacy.org/wiki/display/DP/DNS+Privacy+Public+Resolvers)
* [DNS Privacy Test Servers](https://dnsprivacy.org/wiki/display/DP/DNS+Privacy+Test+Servers)

---

## Identity management {#idm}
Identity theft is a main threat to individuals on the internet. The best way to defend your [personal data]({{< ref "/glossary.md#personal-data" >}}) online is to stop using online services at all. This is quite unrealistic. Another way is to use services which provide cryptographic proof so others can verify that you actually own certain online accounts.

We recommend [Keybase](https://keybase.io/) for private users. See also [our articles about Keybase](/tags/keybase/).

---

## Instant messaging {#im}
Ask 10 people about their preferred instant messenger and you'll get 15 recommendations. Some people say that federation is best for privacy ([no, this is wrong]({{< ref "/blog/myths-federation.md#m2" >}})), some recommend closed-source messengers like Threema and most people keep on using WhatsApp.  We aren't interested in wars of opinions and stay with the facts.

If it comes to security, privacy, usability, and support for different operating systems, [Signal](https://signal.org/download/) is the clear winner. See also our [articles on Signal](/tags/signal/).

If you still want to use XMPP-based messengers like Conversations, Gajim, Dino and so on, keep in mind that server-side parties can <a href="/blog/xmpp-aitm">access and manipulate</a> everything. We strongly recommend running your own XMPP server in this case. If you don't know how to do this, use a messenger like Signal. Unlike many XMPP-based messengers, Signal uses client-side account management and enforces end-to-end encryption by default.

---

## Operating systems {#os}
We recommend the following operating systems for advanced users:

* [Parrot OS Security Edition](https://www.parrotsec.org/download-security.php) (security testing + software development)
* [Debian](https://www.debian.org/) (server operating system)
* [Arch Linux](https://www.archlinux.org/) (client operating system)

---

## Repositories {#repo}
The following repositories contain useful resources and links:

* [Awesome Cellular Hacking](https://github.com/W00t3k/Awesome-Cellular-Hacking)
* [Awesome Infosec](https://github.com/onlurking/awesome-infosec)
* [Awesome Hacking](https://github.com/Hack-with-Github/Awesome-Hacking)
* [Awesome Security](https://github.com/sbilly/awesome-security)
* [Awesome Social Engineering](https://github.com/v2-dev/awesome-social-engineering)
* [Awesome Web Security](https://github.com/qazbnm456/awesome-web-security)
* [Probable Wordlists](https://github.com/berzerk0/Probable-Wordlists)

---

## Secure key and password storage {#skps}
If you use GnuPG, SSH etc., you probably store your keys on your computer. This isn't very secure and stolen keys can result in data breaches (SSH) and decrypted messages (GnuPG). Use dedicated security hardware to store your keys. Furthermore, use password management software like KeePass to store your passwords encrypted. Some products also support [OATH-TOTP]({{< ref "/glossary.md#oath-totp" >}}), U2F for [2FA]({{< ref "/glossary.md#2fa" >}}), and/or [WebAuthn]({{< ref "/glossary.md#webauthn" >}}).

We tested the following hardware and software:

* [Nitrokey Pro 2](https://www.nitrokey.com/) (and its predecessor) ([#nitrokey](/tags/nitrokey/))
* [YubiKey 5](https://www.yubico.com/) (and its predecessor) ([#yubikey](/tags/yubikey/))
* [Yubico Security Key](https://www.yubico.com/) (support for FIDO2, WebAuthn, and U2F) ([#yubikey](/tags/yubikey/))
* [KeePass Password Safe](https://keepass.info/) (well-tested password manager)
