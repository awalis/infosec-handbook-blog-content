+++
title = "Security and disclosure policy"
ogdescription = "Security-related information and the disclosure policy of InfoSec Handbook"
nodateline = true
noprevnext = true
+++

This page is about our security and disclosure policy. Have a look at our [privacy policy]({{< ref "privacy-policy.md" >}}), if you are looking for privacy-related topics.

## Contents
* [Security contact]({{< relref "#security-contact" >}})
* [For us, security and privacy take top priority]({{< relref "#our-security" >}})
* [Disclosure policy]({{< relref "#disclosure-policy" >}})
* [Acknowledgments]({{< relref "#acknowledgments" >}})

## Security contact
* We provide a [security.txt file](https://infosec-handbook.eu/.well-known/security.txt) for structured security contact information.
* See our [contact page]({{< ref "contact-details.md" >}}) for contact details and our GnuPG key.
* Moreover, you can send encrypted messages via {{< extlink "https://keybase.io/infosechandbook" "Keybase.io" >}}.

## For us, security and privacy take top priority {#our-security}
✅ No logging by default – ✅ Minimal data processing

We decided to choose the best protection for your [personal data]({{< ref "glossary.md#personal-data" >}}): We simply do not collect it. You don't have to trust us, because you keep your data. By default, we do not log anything, and we concluded a data processing agreement according to Article 28 GDPR with our server provider (see our [privacy policy]({{< ref "privacy-policy.md" >}})).

✅ Single-purpose server – ✅ No databases

For security, we provide our blog using a dedicated virtual server. There aren't any other public services on this server (e.g., no database server, no mail server, no messaging server). For instance, the decentralized Dat version of our blog runs on another virtual server.

✅ Security monitoring – ✅ Strong authentication – ✅ Defined processes

Our server is permanently monitored to check for modified files and login attempts. [Two-factor authentication]({{< ref "glossary.md#2fa" >}}) is needed to access our server. The core of our server is a [hardened]({{< ref "glossary.md#hardening" >}}) Linux installation. This means that we removed unnecessary packages and applied strict configuration at kernel level. Finally, we implemented processes to ensure installation of security updates within a narrow time frame and quick reaction to reported potential security vulnerabilities.

✅ 100% static content – ✅ No CMS, PHP, or JavaScript – ✅ No 3rd party content

Our blog consists of 100% static content. There is no content management system (CMS) installed and there is no dynamically-served content like PHP or JavaScript. We do not embed any third-party content, and all links to third-party websites are marked. Actually, third-party websites, which you access from our blog, run in a separate process in your web browser, and we strip any Referrer information.

## Disclosure policy
Did you find a security vulnerability? You find our [security-related contact details]({{< relref "#security-contact" >}}) above. Besides, we run a bug bounty program to ensure the highest security and privacy. Everyone is eligible to participate in the program subject to the below-mentioned conditions and requirements ("disclosure policy") of InfoSec Handbook.

### Disclosure process
We are big fans of "coordinated disclosure". Due to this, we stay with the following process:

1. You privately report a potential security vulnerability using the [above-mentioned channels]({{< relref "#security-contact" >}}) and observing the below-mentioned [testing requirements and code of conduct]({{< relref "#tr-and-coc" >}}).
2. We check your report for eligibility and respond within 5 days.
3. Depending on the result, we either:
  * fix the vulnerability and get in touch with you regarding your bug bounty and coordinated disclosure, or
  * get in touch with you to request additional information, or
  * inform you about ineligibility of your report.

### Scope and possible bug bounties {#scope}
The disclosure policy on this page is valid for the following domain names (and underlying servers):

| Domain name| Eligible for bug bounties |
|------------|---------------------------|
| https://infosec-handbook.eu/ | yes |
| [Dat version of our blog](dat://4354ef3fa9ae5df664fd4a40707cab7450a24d29d4d9f2770b29ebdc720c7151) | no |
| https://ish-test.eu | no |

The following bounties are only a guideline. The final bug bounty will be directly stated in our responses. **If all testing requirements were met**, we offer the following bounties:

| Type of vulnerability                      | Bug bounty up to |
|--------------------------------------------|------------------|
| Information leakage (except personal data) | €75              |
| Code injection (e.g., HTML, JS)            | €100             |
| Unauthorized access (user-level)           | €100             |
| Remote Code Execution (RCE)                | €150             |
| Leakage of personal data                   | €175             |
| Unauthorized access (root-level)           | €175             |

Out-of-scope are all classes of potential vulnerabilities that aren't mentioned above, including the absence of certain HTTP response headers, vulnerabilities that require physical access to our servers, and recently disclosed 0-day vulnerabilities. If you report out-of-scope vulnerabilities, you may be eligible to be listed on our acknowledgment page.

Bug bounties can only be paid via bank wire transfer or Stellar Lumens (XLM). There may exist additional legal regulations and requirements.

### Testing requirements and code of conduct {#tr-and-coc}
We won't take legal action against you as a penetration tester, if you observe the law. Moreover, we always respect your privacy. By default, we won't publish anything about you or your reports.

Furthermore, please observe our following rules:

1. You must be the first reporter of a vulnerability.
2. The reported vulnerability and domain name must [be in scope]({{< relref "#scope" >}}).
3. Your report
  * must be sent via [above-mentioned channels]({{< relref "#security-contact" >}})
  * must clearly describe the potential vulnerability
  * must include a step-by-step description that allows us to reproduce the issue
  * may include additional screenshots or proof of concept code, if necessary
4. Aggressive penetration testing is strictly prohibited. By "aggressive" we mean using automated tools for untargeted attacks on our servers. Flooding our servers with millions of requests or executing random attacks neither is something a professional penetration tester does nor something that we want to see.
5. In any case, you must not leak, manipulate, or destroy any data.
6. Without our prior permission, you must not release anything related to the vulnerability to the public.
7. We do not tolerate abusive language, harassment, any kind of criminal activity, or impersonation.

If you have any further questions, please do not hesitate to [contact us]({{< ref "/contact-details.md#e-mail" >}}).

## Acknowledgments
We would like to thank the following researchers and testers:

| Date       | Name        | Vulnerability                     | Bounty |
|------------|-------------|-----------------------------------|-----|
| 2019-08-28 | Undisclosed | Unintended metadata in some files | €25 |
