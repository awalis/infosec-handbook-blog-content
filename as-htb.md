+++
title = "Hack The Box series"
ogdescription = "The Hack The Box series is all about walkthroughs for retired HTB machines."
nodateline = true
noprevnext = true
+++

<picture>
  <source srcset="/img/as-htb.png.webp" type="image/webp">
  <source srcset="/img/as-htb.png" type="image/png">
  <img src="/img/as-htb.png" alt="Hack The Box series" title="Hack The Box series"/>
</picture>

In this series, we occasionally present walkthroughs for retired Hack The Box machines. [Hack The Box (HTB)](https://www.hackthebox.eu/) is a British company, which was founded in June 2017. HTB provides a set of vulnerable virtual machines that can be exploited using real-world tools. This allows people to train their penetration testing skills in a controlled and legal setting.

## Walkthroughs
* [Hack The Box walkthrough: Netmon]({{< ref "/blog/htb-netmon.md" >}})

## How does HTB work?
Each week, a new virtual machine is released (= active machine). A previously-active machine retires. It is only allowed to publish walkthroughs for retired machines. To access retired machines, you have to pay a fixed monthly charge. Keep in mind that these are all shared virtual machines. There may be other users simultaneously trying to exploit the same virtual machine like you. The monthly fee for private individuals includes less users per VPN. If you want your own dedicated VPN access, look at offerings for companies.

The goal is to retrieve two files from each vulnerable system: "user.txt" (user access) and "root.txt" (root access). After retrieving these files, you can enter the contained codes on hackthebox.eu.

## Quickstart
To register, you have to solve a little challenge. We don't help you with that. After that, you can download your OpenVPN access file (called "Connection Pack") to connect to the HTB VPN. Then, you are able to access 20 active machines for free. As mentioned above, you only have to pay if you want to access retired machines. There are also some challenges that you can solve. To access an active machine, just connect to its IPv4 address.

## Tips
* Use a dedicated operating system for this. [See our recommendations.]({{< ref "/recommendations.md#os" >}})
* Document everything! This is one of the most important parts of penetration testing. Take screenshots. Document every command you enter and every tool you use.
* For easier access, add the machine's IPv4 address to your "/etc/hosts" file. For instance, add "10.10.10.250 machine.htb".
* Always start with a scan for open ports. Use tools like nmap, masscan, or zmap.
