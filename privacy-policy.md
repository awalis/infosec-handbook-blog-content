+++
title = "Privacy policy"
ogdescription = "Privacy policy of infosec-handbook.eu"
nodateline = true
noprevnext = true
+++

Thank you for your interest in our privacy policy. This policy contains information about how we process your personal data and about your rights according to the European GDPR (General Data Protection Regulation). References below to "we" or "us" refer to the operator of this website. Our website and this privacy policy are provided in accordance with Czech and/or European law.

## Scope
The following privacy policy is valid for:

* https://infosec-handbook.eu/ (centralized HTTPS version of our blog)
* [dat://4354ef3fa9ae5df664fd4a40707cab7450a24d29d4d9f2770b29ebdc720c7151](dat://4354ef3fa9ae5df664fd4a40707cab7450a24d29d4d9f2770b29ebdc720c7151) (decentralized dat version of our blog)

---

## Short version of our privacy policy {#short-pp}
* By default, our web server processes your IP address. This is technically necessary to send our content to your client.
* By default, we do not log any personal data of you.
* Log files are automatically encrypted after 1 day, and stored in encrypted format for 10 days.
* Your rights according to the European GDPR are explained in Articles 15–21 and 77 GDPR.
* In case of any questions related to this privacy policy, feel free to [contact us]({{< ref "contact-details.md#e-mail" >}}).

---

## Contact details
This website and its web server are operated by private individuals domiciled in different European states. Our server is physically located in Germany.

Controller in terms of the GDPR is:

<span class="ilovecheese">.cS.M ,řítyR bukaJ .rM</span><br>
<span class="ilovecheese">ynačosyV ,aharP 00 091</span><br>
<span class="ilovecheese">cilbupeR hcezC</span><br>
[Contact details]({{< ref "contact-details.md#e-mail" >}})

---

## Contents
* [Definitions]({{< relref "#definitions-gdpr" >}})
* [Personal data we process]({{< relref "#processing-gdpr" >}})
* [Personal data third parties process for us]({{< relref "#processing-gdpr-3p" >}})
* [Accessing our website using dat protocol]({{< relref "#dat" >}})
* [Your rights (Articles 15–20 GDPR)]({{< relref "#rights-gdpr" >}})
* [Right to object (Article 21 GDPR)]({{< relref "#21-gdpr" >}})
* [Right to lodge a complaint with a supervisory authority (Article 77 GDPR)]({{< relref "#77-gdpr" >}})
* [Changelog]({{< relref "#changelog" >}})

---

## Definitions {#definitions-gdpr}
There are several definitions in the GDPR. The most important definitions are:

* ‘personal data’ means any information relating to an identified or identifiable _natural_ person
* ‘processing’ means any operation […] on personal data […] such as collection, recording, organisation, structuring, storage, adaptation or alteration, retrieval, consultation, use, disclosure by transmission, dissemination or otherwise making available, alignment or combination, restriction, erasure or destruction

Examples for "personal data" are your name, your username(s), your address, your e-mail address, (sometimes) your IP address and more sensitive data like your religious and sexual orientation. Especially IP addresses can also represent corporate data (for instance, when you access websites using your corporate internet access) or they can be anonymous (for instance, when you use Tor to access websites).

The term "processing" can be considered as an umbrella term for everything we do with your personal data.

---

## Personal data we process {#processing-gdpr}
When you visit our website, your **IP address** and user agent (e.g., information about your web browser and/or operating system) are automatically processed by our web server. This is technically necessary since your client requests resources from our web server. Then, our web server needs your IP address to send responses to your client. By default, we do not process any other personal data of you.

The legal basis for the processing of your IP address (and user agent) is Article 6(1) f GDPR. Our legitimate interest is to provide our content if you want to access it.

### Logging {#logging}
Our web server writes information about each client-side request to so-called log files. We use these log files as explained below. Our web server automatically encrypts all log files after one day using [public-key cryptography]({{< ref "/glossary.md#public-key-cryptography" >}}). The encrypted log files are automatically deleted after 10 days.

### Logging of normal requests {#log-normal}
In case of normal requests (for technical people: HTTP status codes 200, 302, 304), we don't log anything.

### Logging of abnormal or blocked requests {#log-abnormal}
In case of abnormal requests (for technical people: all HTTP status codes except 200, 302, 304), we only log the following. This includes repeated attempts to access blacklisted files, or other attack-like behavior:

* timestamp (`[31/Dec/2016:12:01:10 +0100]`)
* IP address (`123.123.123.123`)
* HTTP status code (`403`)
* bytes transmitted (`157`)
* first line of request for each request/HTTP version (`"GET /secrets.bak HTTP/1.1"`)
* user agent (`Mozilla/5.0 (Windows NT 6.1; rv:60.0) Gecko/20100101 Firefox/60.0`)
* in some cases, our web application firewall also logs full client-side requests and full server-side responses

We use this data to identify new attacks, audit blocked requests and unblock legitimate users, if necessary. Blocked IP addresses are stored for 14 days.

The legal basis for the processing of your personal data is Article 6(1) f GDPR. Our legitimate interest is to block attack-like behavior and illegal access attempts.

---

## Personal data third parties process for us {#processing-gdpr-3p}
The following third parties process personal data for us:

### netcup GmbH, Germany {#ng}
The netcup GmbH ([read their privacy policy](https://www.netcup.eu/kontakt/datenschutzerklaerung.php)) provides our (web) server. The netcup GmbH may log access attempts (IP address, user agent) for all of its customers (including us) to detect DDoS attacks, attack-like behavior, and so on.

We concluded a data processing agreement according to Article 28 GDPR with netcup GmbH.

The legal basis for the processing of your personal data is Article 6(1) f GDPR. Our legitimate interest and the interest of the netcup GmbH is to detect/block attack-like behavior, and to provide the content of our blog.

### Tutao GmbH, Germany (e-mail only) {#emailprovider}
The Tutao GmbH ([read their privacy policy](https://tutanota.com/privacy)) provides our mail server. It isn't necessary to send us any e-mails to access our blog/content. **If you decide to contact us, you agree that we/Tutao GmbH process your personal data (e.g., name, e-mail address) to answer your request.** We do not use your e-mail address for marketing purposes. We immediately delete your e-mails after your request is answered.

The legal basis for the processing of your personal data is Article 6(1) a GDPR. You may withdraw your consent with this at any time.

---

## Accessing our website using dat protocol {#dat}
Besides "traditional" HTTPS, we allow interested readers to access our content via the "dat" protocol. dat is a peer-to-peer protocol, offering a decentralized experience. It comes with built-in encryption and several other advantages.

However, as normal for peer-to-peer protocols, content can be freely re-shared by every client. This means that there is no central server, and all clients connect directly to each other. By doing so, your dat client may disclose your IP address and user agent to unknown third parties. If you don't want this, simply access our web server via HTTPS.

---

## Your rights (Articles 15–20 GDPR) {#rights-gdpr}
According to the Articles 15 to 20 of the GDPR, you have several rights concerning your personal data processed by us:

* Art. 15: Right of access
* Art. 16: Right to rectification
* Art. 17: Right to erasure
* Art. 18: Right to restriction of processing
* Art. 19: Notification obligation regarding rectification or erasure of personal data or restriction of processing
* Art. 20: Right to data portability

Besides your IP address (and maybe user agent) which is automatically transmitted by your client and only used for responding to its requests, we do not process personal data of you. Since there is no need for us to identify you when you normally access our website, we aren't obliged to maintain, acquire or process additional information in order to identify you for the sole purpose of complying with the GDPR.

This means that (according to Article 11 GDPR) Articles 15 to 20 don't apply except where you, for the purpose of exercising your rights mentioned above, provide additional information enabling your identification.

## Right to object (Article 21 GDPR) {#21-gdpr}
**You have the right to object, on grounds relating to your particular situation, at any time to processing of personal data concerning you which is based on point e or f of Article 6(1) GDPR, including profiling based on those provisions. We no longer process the personal data unless we demonstrate compelling legitimate grounds for the processing which override the interests, rights and freedoms of you or for the establishment, exercise or defence of legal claims. This doesn't affect the lawfulness of processing based on consent before its withdrawal (point c of Article 13(2) GDPR).**

## Right to lodge a complaint with a supervisory authority (Article 77 GDPR) {#77-gdpr}
Without prejudice to any other administrative or judicial remedy, you have the right to lodge a complaint with a supervisory authority, in particular in the Member State of your habitual residence, place of work or place of the alleged infringement if you consider that the processing of personal data relating to you infringes the GDPR.

For further information about our security measures, read our [security policy]({{< ref "security.md" >}}).

---

## Changelog
* Jul 22, 2019: Simplified logging (we don't log anything by default, only in abnormal cases or if our WAF blocks an IP address).
* May 12, 2019: Moved note regarding GDPR/contact to the contact page. Added adequacy decision for Switzerland.
* Feb 11, 2019: Moved hosting provider to a new section. Added scope.
* Feb 9, 2019: Added changelog. Updated location of server operator. Added detailed information how we log client-side requests, and which personal data is stored. Added short version of the policy. Added information about accessing our website via dat protocol.
* May 24, 2018: Updated privacy policy for European GDPR.
