+++
title = "Changelog"
ogdescription = "List of blog-wide changes."
nodateline = true
noprevnext = true
+++

There is an [RSS/Atom feed available](/changelog.xml) for this page.

## Changes on October 20, 2019 {#10-20-2019}
* Move blog content repo from GitHub to codeberg.org

## Changes on September 18, 2019 {#09-18-2019}
* Move blog content to its own Git repository
* Add smooth scrolling
* Add new style for contact page
* Add several alternative possibilities for contacting ISH
* Change select color to blog style

## Changes on August 09, 2019 {#08-09-2019}
* Added a CSS-based workaround to fix Chroma-based syntax highlighting in Chrome/Chromium/Edge etc. This is a bug in Hugo/Chroma.

## Changes on August 02, 2019 {#08-02-2019}
* Added 4 tools to the terminal tips page (cheat, curl, subnetcalc, zmap)
* Updated 1 tool on the terminal tips page (openssl)

## Changes on July 07, 2019 {#07-07-2019}
* Added disclosure policy and bug bounty program

## Changes on June 21, 2019 {#06-21-2019}
* Switched to a new server
* Switched to nginx web server
* Enabled support for TLS 1.3
* Enabled several new security features

## Changes on June 14, 2019 {#06-14-2019}
* Updated "About us" page

## Changes on May 09, 2019 {#05-09-2019}
* Update number of corporate networks
* Fix formatting and links in notice boxes
* Fix mentioning of Curve25519
* Add notice boxes to several articles

## Changes on March 10, 2019 {#03-10-2019}
* Added 2 entries to the glossary
* Added context-aware CSS loading, and shortcode for note boxes
* Added WebP support for PNG files (except if loaded via CSS)
* Added SECURITY.md on GitHub

## Changes on February 17, 2019 {#02-17-2019}
* Marked external links using extlink shortcode
* Added GPG key creation guide to terminal tips

## Changes on February 09, 2019 {#02-09-2019}
* Updated privacy policy
* Standardized placeholders
* Moved code in several articles as a workaround for bug 232 (https://github.com/alecthomas/chroma/issues/232)

## Changes on January 26, 2019 {#01-26-2019}
* Improved CSS (reduced file size by 5kB)
* Minor layout changes

## Changes on January 23, 2019 {#01-23-2019}
* Fixed RSS feed error (Entity 'rsquo' not defined) for Chrome, Chromium, and spaRSS users

## Changes on January 21, 2019 {#01-21-2019}
* Migrated development backend from Keybase Git to GitHub
* Compressed font file (again). Initial page size reduced to about 31kB
* Compressed all PNG files using a new algorithm
* Removed unused HTML tags

## Changes on January 14, 2019 {#01-14-2019}
* Update security.txt to draft 05

## Changes on January 07, 2019 {#01-07-2019}
* Added experimental full-text RSS feed

## Changes on December 15, 2018 {#12-15-2018}
* Updated favicons
* Added schema.org fields
* Added og graph fields
* Removed unused images
* Removed unused CSS
* Fixed HTML code (h1, h2)
* Added 'feeds and social media' header
* Added 'latest articles' header
* Updated 'new and updated articles' header
* Sidebar shows new and recently updated articles now
* Updated security.txt.sig

## Changes on December 11, 2018 {#12-11-2018}
* Updated Webbkoll information on several pages
* Updated GPG key
* Changed e-mail address (this is the same provider as before, however, the domain is shorter)

## Changes on November 30, 2018 {#11-30-2018}
* Fixed .Lastmod fields
* Changed mastodon.at tag

## Changes on November 25, 2018 {#11-25-2018}
* Added code highlighting
* Added experimental dat support
* Changed templates for dat

## Changes on November 24, 2018 {#11-24-2018}
* Added 2 entries to the glossary
* Changed CSS for code and highlighted code
* Removed Chroma style in CSS

## Changes on November 17, 2018 {#11-17-2018}
* Added 5 entries to the glossary, updated 5 entries
* Added 'updated' indicator (sidebar, article lists)
* Updated theme (removed .Data, added some additional schema.org itemprops)

## Changes on November 11, 2018 {#11-11-2018}
* Added NEL and Report-To headers
* Readded Mastodon boxes
* Updated CSP
* Updated "About us" page
* Enabled IPv6 support for the blog
* Minor CSS changes

## Changes on November 05, 2018 {#11-05-2018}
* Mastodon: Verified all links on the account page
* Mastodon: Updated contact link

## Changes on November 04, 2018 {#11-04-2018}
* Added 17 new glossary entries
* Added centralized contact page
* Added recommendations for operating systems
* Added centralized copyright page
* Removed contact details from all other pages
* Updated security page, privacy policy and about us
* Updated RSS/Atom feed page
* Updated security.txt signature
* Minor theme changes
* Revised all blog articles (see changelog of every article)

## Changes on October 21, 2018 {#10-21-2018}
* Added U2F, WebAuthn, FIDO2 to glossary
* Added official Reddit account

## Changes on September 27, 2018 {#09-27-2018}
* Updated GPG key
* Added 7 new glossary entries

## Changes on August 25, 2018 {#08-25-2018}
* Added: Web server security pages
* Changed: CSS (minor tweaks)

## Changes on August 23, 2018 {#08-23-2018}
* Added: Automatically generated "See also" section on each article page
* Added: New shortcode for images added, changed all images accordingly
* Changed: Added CSS for figure and figcaption
* Changed: Fonts are a little bit larger now

## Changes on August 17, 2018 {#08-17-2018}
* Added [Recommendations]({{< ref "recommendations.md" >}}) page

## Changes on August 10, 2018 {#08-10-2018}
* Changed relref page references to ref (Hugo 0.45 change)
* Updated [About]({{< ref "about.md" >}}) page
* Added contributor [Verena]({{< ref "about.md#Verena" >}})

## Changes on July 16, 2018 {#07-16-2018}
* Enabled Feature Policy for testing purposes
* Updated our security.txt implementation ([draft-foudil-securitytxt-04](https://tools.ietf.org/html/draft-foudil-securitytxt-04))

## Changes on May 26, 2018 {#05-26-2018}
* We are now supporting Brotli compression.

## Changes on May 16, 2018 {#05-16-2018}
* Changed en-dashes (–) to em-dashes (—)
* [Privacy policy]({{< ref "privacy-policy.md" >}}): Added information about corporate IP addresses plus User-Agents and improved readability.

## Changes on May 10, 2018 {#05-10-2018}
* [Terminal tips]({{< ref "terminal-tips.md" >}}) are back!

## Changes on May 8, 2018 {#05-08-2018}
* Added Chroma-based syntax highlighting
* Updated "[Limits of Webbkoll]({{< ref "blog/limits-webbkoll.md#changelog" >}})"

## Changes on May 1, 2018 {#05-01-2018}
* Added CAA, DNSSEC, CSP to the [glossary]({{< ref "glossary.md" >}}).
* Slightly increased the default font size.

## Changes on April 3, 2018 {#04-03-2018}
* Updated "[Signal messenger myths]({{< ref "blog/myths-signal.md#changelog" >}})"
* Enabled Certificate Transparency for both certificates

## Changes on March 25, 2018 {#03-25-2018}
* Moved content of previous website to new one.

175 changes before March 25, 2018 aren't listed.
