+++
title = "Glossary"
ogdescription = "Definitions of terms in information security and data protection"
nodateline = true
noprevnext = true
+++

This glossary defines terms often used on infosec-handbook.eu.

[2]({{< relref "#2" >}}) | [A]({{< relref "#a" >}}) | [B]({{< relref "#b" >}}) | [C]({{< relref "#c" >}}) | [D]({{< relref "#d" >}}) | [E]({{< relref "#e" >}}) | [F]({{< relref "#f" >}}) | [G]({{< relref "#g" >}}) | [H]({{< relref "#h" >}}) | [I]({{< relref "#i" >}}) | [K]({{< relref "#k" >}}) | [M]({{< relref "#m" >}}) | [N]({{< relref "#n" >}}) | [O]({{< relref "#o" >}}) | [P]({{< relref "#p" >}}) | [R]({{< relref "#r" >}}) | [S]({{< relref "#s" >}}) | [T]({{< relref "#t" >}}) | [U]({{< relref "#u" >}}) | [V]({{< relref "#v" >}}) | [W]({{< relref "#w" >}}) | [X]({{< relref "#x" >}}) | [Y]({{< relref "#y" >}}) | [Z]({{< relref "#z" >}})

## 2
### 2FA
Two-factor [authentication]({{< relref "#authentication" >}}) requires individuals or systems to prove their identity by providing two different factors: something they have, something they know, and/or [something they are]({{< relref "#biometrics" >}}). For example, you must provide your credit card (sth. you have) and PIN (sth. you know) when you withdraw money.

## A
### Accountability
Accountability is a [security goal]({{< relref "#security-goal" >}}) of [RMIAS]({{< relref "#rmias" >}}). It means that a system can hold users responsible for their actions.

### Advanced persistent threat
APT basically means that an attacker infiltrates a system over a long period. He adapts his actions to his victims to pass undetected and gain a permanent foothold. Therefore, APTs are very customized attacks.

### AEAD
Authenticated Encryption with Associated Data (AEAD) includes Authenticated Encryption (AE). AE combines encryption and [MAC]({{< relref "#mac" >}}) to accomplish [confidentiality]({{< relref "#confidentiality" >}}), [integrity]({{< relref "#integrity" >}}) and [authenticity]({{< relref "#authenticity" >}}). AEAD allows its users to transfer additional unencrypted but authenticated data. This accomplishes the [security goals]({{< relref "#security-goal" >}}) integrity and authenticity. For example, modern [TLS]({{< relref "#tls" >}}) [cipher suites]({{< relref "#cipher-suites" >}}) implement AEAD.

### AES
AES (Advanced Encryption Standard) is a widespread [symmetric]({{< relref "#symmetric-cryptography" >}}) encryption algorithm.

### ALPN
Application-Layer Protocol Negotiation (ALPN) is a [TLS]({{< relref "#tls" >}}) extension. It is needed by HTTP/2 to improve the compression of web pages and to reduce latency. ALPN replaced Next Protocol Negotiation (NPN).

### Argon2
Argon2 is a [key derivation function]({{< relref "#kdf" >}}). It uses a [password]({{< relref "#password" >}}) and additional parameters to derive a stronger cryptographic key. This process is called [key stretching]({{< relref "#key-stretching" >}}) and makes [brute-force attacks]({{< relref "#brute-force-attack" >}}) less feasible. Argon2 can be used to store passwords securely in a database. Another widespread KDF is [PBKDF2]({{< relref "#pbkdf" >}}).

### Attack tree
Attack trees are diagrams to show how something (root of the tree) can be attacked. The root of the tree is the ultimate goal of the attacker. Leaves and their children show different attack paths. All child nodes of a certain node must be satisfied to make the parent node true. This allows exclusion of nodes when there are protection measures in place.

### Audit
Auditing means basically to compare the desired condition of something with its actual state. For instance, there are code audits to find bugs and [vulnerabilities]({{< relref "#vulnerability" >}}). Companies, their subsidiaries or single data centers can also be audited, for instance, to get a ISO/IEC 27001 certification.

### Auditability
Auditability is a [security goal]({{< relref "#security-goal" >}}) of [RMIAS]({{< relref "#rmias" >}}). It means that a system can conduct persistent, non-bypassable monitoring of all actions performed by humans or machines within the system.

### Authentication
Authentication means that a system/individual confirms the identity of a system/individual. Normally this is done by providing some kind of proof (something you have, something you know, and/or [something you are]({{< relref "#biometrics" >}})) and the verifier knows that an identity is linked to this proof. For example, you must provide your credit card (sth. you have) and PIN (sth. you know) when you withdraw money. This is called [two-factor authentication]({{< relref "#2fa" >}}).

### Authenticity
Authenticity (also called "trustworthiness") is a [security goal]({{< relref "#security-goal" >}}) of [RMIAS]({{< relref "#rmias" >}}). It means that a system can verify the identity of a third party and establish trust in a third party and in information it provides. An attack on this security goal is the [replay attack]({{< relref "#replay-attack" >}}).

### Availability
Availability is a [security goal]({{< relref "#security-goal" >}}) of [RMIAS]({{< relref "#rmias" >}}) and the [CIA triad]({{< relref "#cia-triad" >}}). It means that a system is available when expected. This also means that a system (e.g., mail server) can be down for maintenance when previously announced. For instance, attackers can conduct a [DDoS attack]({{< relref "#ddos-attack" >}}) to affect the availability of a system/service.

### Awareness
There is no clear definition of awareness in the context of _Information Security Awareness_. It basically means to raise awareness of threats to information security and to change behavior of people. Raising awareness remains an important part in information security due to the fact that [social engineering]({{< relref "#social-engineering" >}}) attacks target humans who can unwittingly disable security measures or leak information.

## B
### Backdoor
A backdoor in software or hardware allows an unauthorized party to bypass access control. For instance, an undocumented developer account in a router allows developers of this product to bypass the login form. Obviously, third parties can also use backdoors to access software/hardware.

### Biometrics
Biometrics refers to metrics related to human characteristics and is used for [authentication]({{< relref "#authentication" >}}) (sth. you are). However, biometrics as a single factor for authentication is still considered insecure.

### Blacklisting
Blacklisting means that one allows all actions by default and explicitly denies certain actions. For instance, a custom e-mail spam filter contains only explicitly defined rules to block certain e-mails. The opposite is [whitelisting]({{< relref "#whitelisting" >}}).

### Block cipher
Block ciphers are algorithms to transform fixed-length blocks (groups of bits) using [symmetric keys]({{< relref "#symmetric-cryptography" >}}). There are different modes of operation. Some modes are considered insecure (like ECB). It is recommended to use only modes that combine [confidentiality]({{< relref "#confidentiality" >}}) and [authenticity]({{< relref "#authenticity" >}}) (authenticated encryption).

### Body area network
A body area network (BAN)/wireless body area network (WBAN) connects wearable devices of one single person. For instance, an activity tracker is connected with a smartphone using Bluetooth. BANs are smaller than [PANs]({{< relref "#personal-area-network" >}}).

### Botnet
After being taken over (e.g., due to [malware]({{< relref "#malware" >}}) infection) a system (bot) can become part of a large remotely controlled network of bots (botnet). Attackers can use these networks for [DDoS attacks]({{< relref "#ddos-attack" >}}) or [phishing]({{< relref "#phishing" >}}).

### Brute-force attack
An attacker who "simply" tries every possible key to access a service or decrypt a file uses "brute force". This is called brute-force attack. Brute-force attacks become more feasible due to more efficient computers. This requires the implementation of better algorithms to slow down the process of guessing.

### Buffer overflow
A buffer overflow occurs when data is written to a buffer which is to small. The data overruns the boundary of the buffer and overwrites adjacent memory areas then. This is a very common type of attack and there are several protective countermeasures available.

## C
### CAA
CAA means "DNS Certification Authority Authorization". Domain name holders can define which certificate authorities should be able to issue certificates for this domain. The idea is to prevent unauthorized certificate issuance. However, certificate authorities must support CAA and there were some reports showing that certificate authorities ignored this policy. As of April 2018, only 3.1% of the 150,000 most popular websites implemented CAA (according to Qualys).

### CAPEC
CAPEC stands for "Common Attack Pattern Enumeration and Classification", and is currently maintained by the Mitre Corporation, a US-based not-for-profit organization. Like [CVE]({{< relref "#cve" >}}) and [CWE]({{< relref "#cwe" >}}), Mitre created the CAPEC system to structure and define attack patterns.

### Certificate
A digital certificate is issued and signed by a trustworthy certificate authority (CA) and contains information like [public key]({{< relref "#public-key-cryptography" >}}) of the owner, its [fingerprint]({{< relref "#fingerprint" >}}) and validity period of the certificate. This allows the verifier to check whether the public key is valid and also trustworthy ([integrity]({{< relref "#integrity" >}}) and [authenticity]({{< relref "#authenticity" >}})). One problem with certificates is to check their revocation status (see [CRL]({{< relref "#crl" >}}) and [OCSP]({{< relref "#ocsp" >}})).

### Certificate Transparency
Certificate Transparency (CT) is a more complex system which basically logs information about all [certificates]({{< relref "#certificate" >}}) issued by trustworthy certificate authorities. This allows clients and other parties to validate certificates provided by servers.

### Challenge–response authentication
The basic principle of this [authentication]({{< relref "#authentication" >}}) method is simple: The verifier sends a challenge to the prover. Then, the prover sends his response to the verifier. Finally, the verifier checks whether the response matches the expected one. However, the actual implementation is more complicated: Both parties normally share a secret and an attacker can capture exchanged messages between both parties. This requires protection against [replay attacks]({{< relref "#replay-attack" >}}) and [brute-force attacks]({{< relref "#brute-force-attack" >}}). It is common practice to send a [nonce]({{< relref "#nonce" >}}) (which is only valid for a short time) to the prover who includes it in his response.

### CIA triad
The CIA triad is a core concept of [information security]({{< relref "#information-security" >}}). Its elements are [confidentiality]({{< relref "#confidentiality" >}}), [integrity]({{< relref "#integrity" >}}) and [availability]({{< relref "#availability" >}}). However, this view is very limited to information which is why there are newer concepts like [RMIAS]({{< relref "#rmias" >}}).

### Cipher suites
Cipher suites (as used in [TLS]({{< relref "#tls" >}})) are sets of algorithms used for key exchange, [authentication]({{< relref "#authentication" >}}), encryption and [MAC]({{< relref "#mac" >}}). Client and server can support different cipher suites but they must have at least one cipher suite in common to establish connections.

### Clickbaiting
Typically, clickbait is a short text or a headline that is designed to make readers curious, so they want to access linked content. While it is primarily a marketing technique, it can be seen as [social engineering]({{< relref "#social-engineering" >}}) in the broadest sense.

### Clickjacking
Clickjacking tricks the user into clicking on concealed links. A clickjacked website looks normal, however, there is an invisible layer over the normal website. Users who think that they click the buttons of the normal website actually interact with the invisible layer. Known [exploits]({{< relref "#exploit" >}}) are downloading and running [malware]({{< relref "#malware" >}}), sharing links on social media or enabling the victim's webcam and/or microphone.

### Confidentiality
Confidentiality is a [security goal]({{< relref "#security-goal" >}}) of [RMIAS]({{< relref "#rmias" >}}) and the [CIA triad]({{< relref "#cia-triad" >}}). It means that only authorized individuals/systems have the ability to read/modify confidential messages.

### Cookie
HTTP/HTTPS is stateless. This means that web browsers need other ways to store data when necessary. Cookies are small files stored client-side which serve this purpose. However, cookies are sometimes used for user tracking. Therefore, it is recommended to deny cookies by default ([whitelisting]({{< relref "#whitelisting" >}})) and delete all cookies when you close your web browser.

### CRL
Certificate Revocation Lists (CRL) are simple lists that contain revoked [certificates]({{< relref "#certificate" >}}). However, there are different problems with CRLs. Due to this, some web browsers implement [OCSP]({{< relref "#ocsp" >}}) while Chrome uses its own mechanism.

### CSP
CSP means "Content Security Policy". Website owners can set a CSP to tell clients (like web browsers) how they should handle content of the website. For example, a CSP can forbid loading JavaScript, images and fonts from other web servers. Website owners shouldn't use `unsafe-inline` directives at all.

### CSRF
Cross-site request forgery (CSRF or XSRF) is used to send unauthorized commands to a website that trusts the user's web browser. The opposite is [XSS]({{< relref "#xss" >}}).

### Curve25519
Curve25519 is an [elliptic curve]({{< relref "#ecc" >}}) that offers 128 bits of security. Its reference implementation is public domain, and it is widely supported. Curve25519 is a SafeCurve (see https://safecurves.cr.yp.to/ for further information).

### CVE
CVE stands for "Common Vulnerabilities and Exposures", and is currently maintained by the Mitre Corporation, a US-based not-for-profit organization. Like [CAPEC]({{< relref "#capec" >}}) and [CWE]({{< relref "#cwe" >}}), Mitre created the CVE system to create globally unique identifiers for security [vulnerabilities]({{< relref "#vulnerability" >}}). Identifiers look like "CVE-YEAR-NUMBER". The year included in the identifier is the year when the CVE ID was assigned, not the year when the vulnerability became publicly known.

### CWE
CWE stands for "Common Weakness Enumeration", and is currently maintained by the Mitre Corporation, a US-based not-for-profit organization. Like [CVE]({{< relref "#cve" >}}) and [CAPEC]({{< relref "#capec" >}}), Mitre created the CWE system to provide a structured list of clearly defined software weaknesses.

### CVSS
The "Common Vulnerability Scoring System" is an open standard to rate the severity of [vulnerabilities]({{< relref "#vulnerability" >}}). CVSS v3.0 provides a score (0 to 10.0, 10.0 means most severe) and a vector string based on a formula that evaluates several metrics to approximate ease and impact of [exploits]({{< relref "#exploit" >}}). The metrics are base metrics (is calculated once), temporal metrics (changes over time), and environmental metrics (gives organizations and individuals the opportunity to adjust the scoring by considering their own infrastructure).

## D
### Data protection
Data protection is basically protection of [personal data]({{< relref "#personal-data" >}}) so that it is only lawfully processed by the processor and third parties aren't able to access this data. However, there is more data in companies which must be protected (see [information security]({{< relref "#information-security" >}})).

### DDoS attack
The goal of DDoS (Distributed Denial of Service) attacks is to affect the [availability]({{< relref "#availability" >}}) of a service or system, e.g., making a web server unreachable for web browsers.

### Defacement attack
Breaking into a web server to modify (add, change, delete) content of the hosted website is called website defacement. Normally, it's easy to spot defacement since defacers want to arouse attention.

### Diceware
Diceware is a technique to generate [passphrases]({{< relref "#passphrase" >}}) using dice as a hardware random number generator. A group of five digits represents a word on a word list. Sufficiently long Diceware passphrases aren't vulnerable to [dictionary attacks]({{< relref "#dictionary-attack" >}}) because there is the same probability for every word on the list to be chosen and words are picked randomly (actually, you randomly generate numbers and replace these numbers with words to be easily readable).

### Dictionary attack
An attacker who uses a word list (like a dictionary) and tries every string on this list to access a service or decrypt a file conducts this type of attack. A suitable [hash function]({{< relref "#hash-function" >}}) and [salt]({{< relref "#salt" >}}) can defeat this attack as long as the attacker doesn't have an appropriate word list and sufficient computing power.

### Digital signature
Digital signatures base on [public-key cryptography]({{< relref "#public-key-cryptography" >}}) and are used to provide [non-repudiation]({{< relref "#non-repudiation" >}}), [authenticity]({{< relref "#authenticity" >}}) and [integrity]({{< relref "#integrity" >}}). To digitally sign data, a user uses a private key while the corresponding public key is used by third parties to validate the signature. It's important to verify the owner of the public key.

### DMZ
The _demilitarized zone_ is a physical or logical subnetwork that contains external-facing services of a network. External hosts on the internet can only connect to servers/services in the DMZ, however, they can't connect to hosts of the private network outside of the DMZ since the private part is protected by a [firewall]({{< relref "#firewall" >}}).

### DNSSEC
DNSSEC stands for "Domain Name System Security Extensions". Its main purpose is [authentication]({{< relref "#authentication" >}}) by signing DNS data, so DNS resolvers can check if DNS records remained unchanged. This enables [integrity]({{< relref "#integrity" >}}) checks. DNSSEC responses are only signed. It does not provide [confidentiality]({{< relref "#confidentiality" >}}) of data.

### Downgrade attack
A protocol which allows different levels of security can be vulnerable to downgrade attacks. This means that an attacker tries to downgrade the security level to the lowest one, so it is easier for him to attack. A well-known example is POODLE.

### Doxing
Publicly releasing [private data]({{< relref "#personal-data" >}}) about an individual or organization is called doxing. Prior to publication, the person conducting doxing uses public databases, social media and/or [social engineering]({{< relref "#social-engineering" >}}) to acquire information.

## E
### ECC
Elliptic-curve cryptography (ECC) is an approach to [public-key cryptography]({{< relref "#public-key-cryptography" >}}) based on the algebraic structure of elliptic curves over finite fields. ECC requires smaller keys compared to non-EC cryptography (like used by [RSA]({{< relref "#rsa" >}})) to provide equivalent security.

### ECDSA
Elliptic Curve Digital Signature Algorithm is a variant of the Digital Signature Algorithm (DSA). It is used to digitally sign data using [elliptic-curve cryptography]({{< relref "#ecc" >}}).

### Ed25519
Ed25519 is an [EdDSA]({{< relref "#eddsa" >}}) signature scheme using SHA-512 and [Curve25519]({{< relref "#curve25519" >}}). It offers roughly the same security as NIST curve P-256, [RSA]({{< relref "#rsa" >}}) with 3000 bit keys, or 128 bit [block ciphers]({{< relref "#block-cipher" >}}). Ed25519 [signatures]({{< relref "#digital-signature" >}}) fit into 64 bytes, and public keys consume only 32 bytes.

### EdDSA
EdDSA stands for "Edwards-curve Digital Signature Algorithm". It uses [elliptic curves]({{< relref "#ecc" >}}) for [digital signatures]({{< relref "#digital-signature" >}}). One famous variant is [Ed25519]({{< relref "#ed25519" >}}).

### End-to-end encryption
End-to-end encryption (also E2EE) ensures that all communication between two endpoints is encrypted and can only be decrypted by the endpoints (e.g., [Signal messenger]({{< relref "#signal-protocol" >}}), [GnuPG]({{< relref "#gnupg" >}})).

### Entropy
The strength of keys is given in bits entropy. One bit represents two possible outcomes (0 or 1). A key with 100 bit entropy is equal to 2<sup>100</sup> possibilities to create this key. Every additional bit duplicates the amount of possibilities.

### Exploit
An exploit is basically code to exploit a [vulnerability]({{< relref "#vulnerability" >}}). Even worse are [zero-day exploits]({{< relref "#zero-day-exploit" >}}).

## F
### Federation
Federation basically means that users of a network can communicate with users of another network without being part of the other network. For instance, Facebook isn't federated because all users have to be on Facebook to communicate with each other. By contrast, e-mail is federated because a Gmail user can send an e-mail to a mailbox.org user.

### Firewall
Hardware or software that limits access between two networks and/or systems and follows a security policy. Firewalls can be network-based or host-based. There are different types of firewalls like packet filters, stateful filters and application layer firewalls. Another specific type of firewall is a [web application firewall]({{< relref "#web-application-firewall" >}}).

### FIDO2
The FIDO2 Project is an effort to create a new FIDO [authentication]({{< relref "#authentication" >}}) standard that incorporates the upcoming [W3C WebAuthn API]({{< relref "#webauthn" >}}) and the Client-to-Authenticator Protocol (CTAP) developed by the FIDO Alliance.

### Fingerprint
A fingerprint is a checksum. You can use [hash functions]({{< relref "#hash-function" >}}) to create fingerprints. One advantage is that you only need to check whether the (shorter) fingerprint matches. For example, you calculate the hash sum of a PDF file and send the fingerprint and file to a friend. She only needs to calculate the hash sum and check if it matches yours. Fingerprints are often used in cryptography (e.g., in [certificates]({{< relref "#certificate" >}}) or to verify [public keys]({{< relref "#public-key-cryptography" >}}) in general).

Besides, the term fingerprint is used in conjunction with machine, device or web browser (e.g., device fingerprint). Such fingerprints consist of (unique) information about devices and can be used to identify individual devices or even users.

## G
### GnuPG
GnuPG (GNU Privacy Guard) provides cryptographic functions to encrypt, decrypt and sign e-mail content, files etc. [Metadata]({{< relref "#metadata" >}}) (like e-mail addresses or subject) remains unencrypted.

## H
### Hardening
Hardening is a generic term for the process of securing systems against attacks. This includes deactivating unused interfaces like USB ports and appropriate rights management.

### Hash function
A hash functions maps input (e.g., files or data) of arbitrary size to output of fixed size (e.g., 128 bit string). In cryptography, hash functions must be infeasible to invert (one-way function) and have several additional properties. Hash functions can be used to check [integrity]({{< relref "#integrity" >}}) of data.

### HMAC
HMAC means "keyed-hash message authentication code". HMAC combines [MAC]({{< relref "#mac" >}}), a secret key and a cryptographic [hash function]({{< relref "#hash-function" >}}). It can be used to check [authenticity]({{< relref "#authenticity" >}}) and [integrity]({{< relref "#integrity" >}}) of data. Unlike MACs, HMACs aren't prone to length extension attacks.

### HSTS
The HTTP Strict Transport Security header tells clients to always use HTTPS connections for this domain name. HSTS becomes only effective when sent over HTTPS. HSTS can be misused for user tracking, however, since HTTPS-only became best practice for most websites, server admins should set this header.

## I
### Industrial control systems
ICS are control systems used for industrial process control. They contain Supervisory Control and Data Acquisition (SCADA) systems or distributed control systems (DCS) and programmable logic controllers (PLCs). ICS differ from "traditional" information technology and most technical security practices can't be simply deployed in ICS environments. Therefore, ICS are considered [operational technology]({{< relref "#ot" >}}).

### IDN homograph attack
Homoglyphs are characters with shapes that appear identical or very similar. Attackers make use of homoglyphs to create internationalized domain names (IDN) which look similar to well-known domain names. For example, `infosес-handbook.eu` looks similar to `infosec-handbook.eu`. However, the first domain name contains Cyrillic е and с. This enables attackers to impersonate individuals and domain names.

### Impersonation
In terms of information security, an impersonator is somebody who pretends to be another person (identity theft) to commit fraud or other illegal activies. It is basically another [social engineering]({{< relref "#social-engineering" >}}) technique.

### Information security
According to Wikipedia, "[i]nformation security, sometimes shortened to InfoSec, is the practice of preventing unauthorized access, use, disclosure, disruption, modification, inspection, recording or destruction of information. It is a general term that can be used regardless of the form the data may take (e.g., electronic, physical)."

On the contrary, _IT security_ is focused on the protection of computer systems (hardware, software, information). Therefore, IT security is a subset of information security.

The term "information" doesn't include [personal data]({{< relref "#personal-data" >}}). This is called [data protection]({{< relref "#data-protection" >}}).

### Integrity
Integrity is a [security goal]({{< relref "#security-goal" >}}) of [RMIAS]({{< relref "#rmias" >}}) and the [CIA triad]({{< relref "#cia-triad" >}}). It means that one can determine whether a certain resource was modified compared with the original resource.

### Intrusion detection system
An IDS monitors networks (network IDS, NIDS) or hosts (host-based IDS, HIDS) for malicious activity or policy violations. Mostly, detected events are reported to a centralized security monitoring solution.

IDS that have the capability to respond to malicious activity or policy violations are referred to as intrusion prevention systems (IPS).

### IPsec
Internet Protocol Security (IPsec) is a secure network protocol suite that authenticates and encrypts the packets of data sent over an internet protocol network. It is used in [virtual private networks]({{< relref "#vpn" >}}). IPsec can be operated in transport mode (only the payload of the IP packet is encrypted and authenticated) or in tunnel mode (the entire IP packet is encrypted and authenticated).

Contrary to [TLS]({{< relref "#tls" >}}), IPsec operates on the Internet Layer.

## K
### KDF
A key derivation function derives at least one secret key from an input like a [password]({{< relref "#password" >}}). Some KDFs can be used for [key stretching]({{< relref "#key-stretching" >}}).

### Kerckhoffs's principle
Kerckhoffs's principle is derived from six design principles for military ciphers written by Dutch linguist and cryptographer Auguste Kerckhoffs. It says that "A cryptosystem should be secure even if everything about the system, except the key, is public knowledge."

### Key stretching
Key stretching is used to make [brute-force attacks]({{< relref "#brute-force-attack" >}}) more difficult by increasing the time it takes to test each possible key. Two widespread key stretching algorithms are [PBKDF2]({{< relref "#pbkdf" >}}) and [Argon2]({{< relref "#argon2" >}}).

### Kill chain
The term "kill chain" originates from a military concept to structure a physical attack. In information security, the "cyber kill chain" is a framework developed by the Lockheed-Martin corporation to structure a digital attack. The cyber kill chain describes different phases of an attack. However, it has some downsides (e.g., several phases happen outside the defended organization, or it is focused on malware-based attacks). Due to this, there is the newer "unified kill chain" based on the cyber kill chain and MITRE’s ATT&CK framework. The unified kill chain describes 18 phases that may occur in digital attacks.

## M
### MAC
A message authentication code (MAC) is a short value used to check [authenticity]({{< relref "#authenticity" >}}) and [integrity]({{< relref "#integrity" >}}) of data. It protects against message forgery by anyone who doesn't know the secret key. Since this [key is shared among sender and receiver]({{< relref "#symmetric-cryptography" >}}) of a message, MACs don't provide [non-repudiation]({{< relref "#non-repudiation" >}}).

### Malware
Malware (malicious software) is a generic term for software containing unwanted or malicious functions. Malware includes [ransomware]({{< relref "#ransomware" >}}), Trojan horses, computer viruses, worms, spyware, scareware, adware etc. Nowadays, malware can't be clearly categorized because sophisticated malware often combines properties of different categories. For instance, WannaCry propagated like a worm but encrypted files and demanded ransom (ransomware).

### Man-in-the-middle attack
While Alice communicates with Bob via the internet, Eve (Eavesdropper) joins the conversation "in the middle" and becomes "man-in-the-middle". Eve can modify, insert, [replay]({{< relref "#replay-attack" >}}) or read messages at will. Protective measures are encryption ([confidentiality]({{< relref "#confidentiality" >}})) and checking [authenticity]({{< relref "#authenticity" >}}) and [integrity]({{< relref "#integrity" >}}) of all messages. However, one must also ensure that one is actually communicating with the expected party. For instance, when you use [GnuPG]({{< relref "#gnupg" >}}) (or [public-key cryptography]({{< relref "#public-key-cryptography" >}}) in general) you have to verify that you own the real public key of the respective recipient.

### Metadata
Metadata is data that provides information about other data. For instance, a JPG file contains the actual picture (data) but also metadata like creation date, type of camera, GPS coordinates etc. Metadata can be valuable for attackers (e.g., finding appropriate exploits for outdated software used by the victim), government agencies (e.g., collecting information about people to create social graphs), and other parties (e.g., show location-based advertisements). As soon as you use any computer (like your smartphone, laptop, PC, IP camera, smart refrigerator), you very likely leave metadata behind.

## N
### Nitrokey
Nitrokey is an open source USB key produced by the Nitrokey UG in Germany. It implements OpenPGP card algorithms. One can generate and store [GnuPG]({{< relref "#gnupg" >}}) key pairs on it. Some models also support generating [OATH-TOTP]({{< relref "#oath-totp" >}}) codes, contain secure password storage, secure data storage and other cryptographic functions.

### Nonce
In cryptography, a nonce is an random number that is only used once. Nonces are used to prevent [replay attacks]({{< relref "#replay-attack" >}}).

### Non-repudiation
Non-repudiation is a [security goal]({{< relref "#security-goal" >}}) of [RMIAS]({{< relref "#rmias" >}}). It means that a system can prove (with legal validity) occurrence/non-occurrence of an event or participation/non-participation of a party in an event.

## O
### OATH-HOTP
OATH-HOTP stands for "OATH [HMAC]({{< relref "#hmac" >}})-based [one-time password]({{< relref "#otp" >}}) algorithm". This algorithm generates OTPs based on a secret key, a counter value and an HMAC.

### OATH-TOTP
OATH-TOTP stands for "OATH time-based [one-time password]({{< relref "#otp" >}}) algorithm". In addition to [OATH-HOTP]({{< relref "#oath-hotp" >}}), the current timestamp is included to create an OTP.

### OCSP
The Online Certificate Status Protocol can be used to obtain the revocation status of digital [certificates]({{< relref "#certificate" >}}). It's an alternative to [CRL]({{< relref "#crl" >}}). Pure _OCSP_ exposes the client's IP address to the OCSP responder while _OCSP stapling_ enables the server to store pre-authenticated OCSP information to avoid this. _OCSP Must-Staple_ is a certificate extension which enables the client to learn about the presence of OCSP information during the [TLS]({{< relref "#tls" >}}) handshake. Most web browsers support OCSP, however, Chrome uses its own mechanism to obtain revocation information.

### OMEMO
OMEMO (XEP-0384) is an _experimental_ extension to XMPP which allows [end-to-end encrypted]({{< relref "#end-to-end-encryption" >}}) communication and offers [perfect forward secrecy]({{< relref "#perfect-forward-secrecy" >}}). It is based on the [Signal Protocol]({{< relref "#signal-protocol" >}}).

### OT
Operational technology includes hardware and software that is used to monitor or modify the physical state of a system. The term OT is used to demonstrate differences between information technology (IT) and [ICS]({{< relref "#industrial-control-systems" >}}) environments.

### OTP
OTP stands for one-time password. This [password]({{< relref "#password" >}}) is only valid for single use. Common algorithms to generate OTPs are [OATH-HOTP]({{< relref "#oath-hotp" >}}) and [OATH-TOTP]({{< relref "#oath-totp" >}}).

### OTR
OTR stands for "Off-the-Record Messaging", allows [end-to-end encrypted]({{< relref "#end-to-end-encryption" >}}) communication and offers [perfect forward secrecy]({{< relref "#perfect-forward-secrecy" >}}). However, it is only suitable for single-client use and synchronous messaging. This means that both parties must be online at the same time to be able to communicate.

## P
### Passphrase
A passphrase is similar to [passwords]({{< relref "#password" >}}), however, it consists of words instead of characters. You can create passphrases with [Diceware]({{< relref "#diceware" >}}).

### Password
A password is basically a string of characters used for [authentication]({{< relref "#authentication" >}}). A strong password consists of randomly-chosen characters which all have identical probability of occurrence.

### Password spraying
Password spraying is basically a [brute-force attack]({{< relref "#brute-force-attack" >}}). Normally, an attacker tries to guess the password for a fixed username. For instance, the attacker sets "root" as the username and iterates over a long list of different [passwords]({{< relref "#password" >}}). Of course, [intrusion detection systems]({{< relref "#intrusion-detection-system" >}}) (IDS) could easily detect a huge number of unsuccessful login attempts.

Contrary to this, password spraying sets a fixed password (e.g., "123456") and iterates over a long list of different usernames. So, there is only one failed login per username and iteration. As a result, IDS aren't triggered. The attack requires a list of (likely) valid usernames and a list of common passwords. Countermeasures are strong credentials and [two-factor authentication]({{< relref "#2fa" >}}).

### PBKDF
PBKDF (Password-Based [Key Derivation Function]({{< relref "#kdf" >}})) creates cryptographic keys based on a [password]({{< relref "#password" >}}), [HMAC]({{< relref "#hmac" >}}), iterations and [salt]({{< relref "#salt" >}}). For instance, WPA2 (Wi-Fi Protected Access 2) uses PBKDF2. The aim is to reduce the vulnerability of keys to [brute-force attacks]({{< relref "#brute-force-attack" >}}). According to RFC 8018 section 4.2., "an iteration count of 10,000,000 may be appropriate".

### Penetration test
Penetration tests are documented checks and scans on applications, systems or websites to identify [vulnerabilities]({{< relref "#vulnerability" >}}). Penetration tests are either black box (pen tester has no inside knowledge), gray box (limited knowledge) or white box (pen tester has inside knowledge) tests. Typically, pen test reports include found vulnerabilities, guidance and severity of each vulnerability.

### Perfect forward secrecy
PFS combines a system of long-term keys and session keys to protect encrypted communications against key compromise in the future. An attacker who is able to record every encrypted message ([man-in-the-middle]({{< relref "#man-in-the-middle-attack" >}})) won't be able to decrypt these messages when keys are compromised in future. Modern encryption protocols like [TLS]({{< relref "#tls" >}}) 1.3 and [Signal Protocol]({{< relref "#signal-protocol" >}}) offer PFS.

### Personal area network
A personal area network (PAN)/wireless personal area network (WPAN) is a small network that connects personal devices in the direct workspace of a single person. Technologies used for WPANs are Bluetooth, ZigBee, IrDA, and Wireless USB. Sometimes, one device in the PAN is used to connect all other devices to another network. PANs are smaller than LANs but bigger than [BANs]({{< relref "#body-area-network" >}}).

### Personal data
According to Article 4 of the European General Data Protection Regulation (GDPR), "‘personal data’ means any information relating to an identified or identifiable natural person (‘data subject’); an identifiable natural person is one who can be identified, directly or indirectly, in particular by reference to an identifier such as a name, an identification number, location data, an online identifier or to one or more factors specific to the physical, physiological, genetic, mental, economic, cultural or social identity of that natural person".

For instance, company names, addresses of authorities or secret manufacturing data isn't personal data. However, it can also be necessary to protect this non-personal data (see [information security]({{< relref "#information-security" >}})).

A similar but non-synonymous term is **personally identifiable information (PII)** in the US. According to NIST Special Publication 800-122, PII is "any information about an individual maintained by an agency, including (1) any information that can be used to distinguish or trace an individual‘s identity, such as name, social security number, date and place of birth, mother‘s maiden name, or biometric records; and (2) any other information that is linked or
linkable to an individual, such as medical, educational, financial, and employment information."

### Phishing
Phishing is a [social engineering]({{< relref "#social-engineering" >}}) technique. Attackers send forged SMS, e-mails, chat messages etc. to their victims to get their [personal data]({{< relref "#personal-data" >}}). After that, attackers can try to impersonate their victims or do sth. criminal. [Spear phishing]({{< relref "#spear-phishing" >}}) is a more sophisticated phishing technique.

### Plausible deniability
Plausible deniability can be another [security goal]({{< relref "#security-goal" >}}). It is accomplished if you can't prove that a particular message was sent by a certain person/system. Then, this person/system can plausibly deny to be the sender of the message.

### Privacy
Privacy is a [security goal]({{< relref "#security-goal" >}}) of [RMIAS]({{< relref "#rmias" >}}). It means that a system should obey privacy legislation and it should enable individuals to control, where feasible, their personal information (user-involvement).

Sometimes, [data protection]({{< relref "#data-protection" >}}) is also called "data privacy".

### Public-key cryptography
Public-key cryptography (or asymmetric cryptography) is the opposite of [symmetric cryptography]({{< relref "#symmetric-cryptography" >}}). Every party has two keys (public and private). The private one must be kept secret and is used for decryption while the public one has to be published and is used for encryption. All other parties must verify that a published public key belongs to the anticipated owner to avoid [man-in-the-middle attacks]({{< relref "#man-in-the-middle-attack" >}}).

The are different approaches to public-key cryptography, for example, some cryptosystems are [based on the algebraic structure of elliptic curves over finite fields]({{< relref "#ecc" >}}) while others are [based on the difficulty of the factorization of the product of two large prime numbers]({{< relref "#rsa" >}}).

Public-key cryptography can also be used for [digital signatures]({{< relref "#digital-signature" >}}).

## R
### Ransomware
Ransomware is a type of [malware]({{< relref "#malware" >}}). There are different subtypes of ransomware but the basic idea is to press victims for money by threaten them with doing sth. harmful (e.g., publishing confidential data or attackers encrypt important data using [public-key cryptography]({{< relref "#public-key-cryptography" >}}) and threaten to delete the corresponding private key after x hours, rendering the encrypted data useless). An important countermeasure is to backup all of your data on a regular basis.

### Referrer
The referrer is part of a HTTP request. Clients send the referrer header to the server. There are different use cases for this. However, it can be used for tracking of users. Most web browsers allow you to disable referrer headers.

### Replay attack
Replay attacks are attacks on [authenticity]({{< relref "#authenticity" >}}). An attacker records messages and sends recorded messages again (replay). The recipient can't be sure whether the second message (sent by the attacker) was actually sent by the sender of the first (identical) message or someone else. Using current timestamps, [nonces]({{< relref "#nonce" >}}) and [end-to-end encryption]({{< relref "#end-to-end-encryption" >}}) are important countermeasures.

### Risk
The level of impact on operations, assets or individuals based on the impact of a threat and the likelihood of that threat occurring.

### Risk assessment
Risk assessment or risk analysis is the process of identifying [risks]({{< relref "#risk" >}}) to operations, assets or individuals by determining the probability of occurrence, the resulting impact and controls that would mitigate this impact.

### Risk management
Risk management is the process of the identification, measurement, control and minimization of [risks]({{< relref "#risk" >}}). It includes [assessing risks]({{< relref "#risk-assessment" >}}), taking actions to reduce risks to an acceptable level and maintaining at an acceptable level.

### RMIAS
RMIAS (A Reference Model of Information Assurance & Security) is a reference model introduced in 2013 which consists of four dimensions: _Security Life Cycle_, _Information Taxonomy_, _Security Goals_ and _Security Countermeasures_. The goal of this model is to overcome restrictions of prior models like the [CIA triad]({{< relref "#cia-triad" >}}) and meet the needs of new trends. Besides "traditional" [security goals]({{< relref "#security-goal" >}}) of the CIA triad this model also contains [authenticity/trustworthiness]({{< relref "#authenticity" >}}), [privacy]({{< relref "#privacy" >}}), [accountability]({{< relref "#accountability" >}}), [auditability]({{< relref "#auditability" >}}) and [non-repudiation]({{< relref "#non-repudiation" >}}). These security goals are viewed in the context of components of an information system which are information, people, processes, hardware, software and networks.

### RSA
RSA (Rivest–Shamir–Adleman) is a well-known [public-key cryptosystem]({{< relref "#public-key-cryptography" >}}) based on the practical difficulty of the factorization of the product of two large prime numbers. For instance, RSA is used as part of [GnuPG]({{< relref "#gnupg" >}}) for e-mail encryption and signing.

## S
### Salt
In cryptography, salt is random data. Commonly, salt is appended to a key and then processed with a [hash function]({{< relref "#hash-function" >}}). Finally, the output and salt are stored in a database. A long salt which is randomly generated for each key protects against [dictionary attacks]({{< relref "#dictionary-attack" >}}).

### Sandboxing
Sandboxing is software-based isolation of applications to mitigate system failures or [vulnerabilities]({{< relref "#vulnerability" >}}).

### Security goal
Concepts in [information security]({{< relref "#information-security" >}}) like the [CIA triad]({{< relref "#cia-triad" >}}) or [RMIAS]({{< relref "#rmias" >}}) define security goals which have to be fulfilled. Well-known security goals are [confidentiality]({{< relref "#confidentiality" >}}), [integrity]({{< relref "#integrity" >}}) and [availability]({{< relref "#availability" >}}).

### Signal Protocol
The Signal Protocol (formerly TextSecure Protocol/Axolotl Protocol) is a modern cryptographic protocol allowing [end-to-end encrypted]({{< relref "#end-to-end-encryption" >}}) communication. Contrary to [OTR]({{< relref "#otr" >}}), asynchronous and multi-client communication is possible. [Perfect forward secrecy]({{< relref "#perfect-forward-secrecy" >}}) is also supported.

### Social engineering
Social engineering is a generic term for psychological manipulation of humans into performing actions. Social engineering isn't dependent on technology and quite common in everyday life. For example, children cry to manipulate their parents or commercials manipulate viewers. In [information security]({{< relref "#information-security" >}}), [phishing]({{< relref "#phishing" >}}) is a widespread social engineering technique.

### Spear phishing
Spear phishing is more sophisticated than [phishing]({{< relref "#phishing" >}}). Attackers customize their forged messages and send them to a smaller amount of potential victims. This requires more research, however, the success rate of spear phishing attacks is higher than the success rate of phishing attacks.

### Stream cipher
Stream ciphers are [symmetric]({{< relref "#symmetric-cryptography" >}}) algorithms to combine plaintext with a pseudorandom keystream. Each digit is encrypted/decrypted one at a time with the corresponding keystream digit. It's extremely important to use different keys each time.

### Subresource Integrity
SRI can be used to ensure [integrity]({{< relref "#integrity" >}}) of third-party content embedded on a website. Websites which don't embed any third-party content don't need SRI. The basic idea is that one [hash]({{< relref "#hash-function" >}}) per external resource is provided. If the external resource is changed (which can put visitors of this website at risk), the hashes provided by the server and calculated by the client don’t match anymore and the resource is discarded.

### Supply-chain attack
A supply-chain attack can basically affect any user of IT/[OT]({{< relref "#ot" >}}) components (hardware or software). Attackers manipulate a component during its manufacturing process. In most cases, the actual attack happens before the targeted user possess the manipulated component. Examples are manipulated compilers or firmware, and attacks like Stuxnet or NotPetya.

### Symmetric cryptography
Symmetric cryptography is the opposite of [public-key cryptography]({{< relref "#public-key-cryptography" >}}). Two parties who want to communicate need exactly the same private key. Both of them use this key for encryption and decryption. Symmetric encryption is faster than public-key encryption, however, you have to securely exchange keys. [AES]({{< relref "#aes" >}}) is a well-known representative of symmetric cryptography.

Symmetric cryptography uses either [block ciphers]({{< relref "#block-cipher" >}}) or [stream ciphers]({{< relref "#stream-cipher" >}}).

## T
### TLS
TLS (Transport Layer Security) allows secure data transfer via the internet. Nowadays, operators of servers should only allow TLS 1.2/TLS 1.3 and [cipher suites]({{< relref "#cipher-suites" >}}) supporting [perfect forward secrecy]({{< relref "#perfect-forward-secrecy" >}}) as well as [AEAD]({{< relref "#aead" >}}). The TLS 1.3 standard only offers cipher suites which provide PFS and AEAD.

## U
### U2F
Universal 2nd Factor (U2F) is an [authentication]({{< relref "#authentication" >}}) standard originally developed by Google and Yubico. The standard is currently hosted by the FIDO Alliance. U2F tokens can be used for [two-factor authentication]({{< relref "#2fa" >}}). These tokens contain a unique secret key which can't be extracted. The specification allows unlimited accounts per U2F token since web applications that offer U2F authentication generate and store their own private/public key pair for each U2F token in use.

## V
### VPN
A virtual private network (VPN) extends a private network (e.g., your network at home) across a public network (like the internet). Devices connected to the VPN are logically part of the private network, even if there are physically somewhere else. This means that applications running across a VPN are subject to the functionality, security, and management of the private network. [IPsec]({{< relref "#ipsec" >}}) or [TLS]({{< relref "#tls" >}}) are widely in use to secure VPNs.

### Vulnerability
Commonly, vulnerabilities are [exploitable]({{< relref "#exploit" >}}) security flaws in software or hardware. Well-known vulnerabilities receive names like Heartbleed, Shellshock, Spectre, or Stagefright and at least one [CVE]({{< relref "#cve" >}}) identifier. There aren't always exploits available for vulnerabilities. A widespread system to classify the severity of vulnerabilities is [CVSS]({{< relref "#cvss" >}}).

## W
### Web application firewall
A WAF is a [firewall]({{< relref "#firewall" >}}) that filters, monitors and blocks HTTP traffic to an from web applications. For instance, this can prevent SQL injection or [XSS]({{< relref "#xss" >}}) attacks.

### WebAuthn
"Web Authentication: An API for accessing Public Key Credentials Level 1" is a specification developed by W3C which defines creation and use of strong, attested, scoped, public key-based credentials by web applications. It is also part of [FIDO2]({{< relref "#fido2" >}}).

### Whitelisting
Whitelisting means that one denies all actions by default and explicitly allows certain actions. For instance, application whitelisting means that only explicitly allowed applications can be executed. The opposite is [blacklisting]({{< relref "#blacklisting" >}}).

## X
### XSS
XSS (cross-site scripting) is a [vulnerability]({{< relref "#vulnerability" >}}) found in web applications. There are different types of XSS attacks. Mostly, the attacker is able to inject client-side scripts into websites. This exploits the trust a user has in a website. The opposite is [CSRF/XSRF]({{< relref "#csrf" >}}).

## Y
### YubiKey
YubiKey is a closed source USB key produced by Yubico in the US and Sweden. It implements OpenPGP card algorithms. One can generate and store [GnuPG]({{< relref "#gnupg" >}}) key pairs on it. Some models also support generating [OATH-TOTP]({{< relref "#oath-totp" >}}) codes, U2F and other cryptographic functions.

## Z
### Zero-day exploit
A zero-day exploit [exploits]({{< relref "#exploit" >}}) a [vulnerability]({{< relref "#vulnerability" >}}) in software or hardware and this vulnerability is unknown to the public, publisher or other parties who would normally mitigate it.
