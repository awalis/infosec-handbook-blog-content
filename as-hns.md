+++
title = "Home network security series"
ogdescription = "The Home network security series shows you how you can improve the security of your home network. This series is based on the Czech open-source router Turris Omnia."
nodateline = true
noprevnext = true
+++

<picture>
  <source srcset="/img/as-hns.png.webp" type="image/webp">
  <source srcset="/img/as-hns.png" type="image/png">
  <img src="/img/as-hns.png" alt="Home network security series" title="Home network security series"/>
</picture>

In this series, we show ways to improve the security of your network at home. We use the Czech open-source router **Turris Omnia** for this, however, you can also use your own router. Many steps are almost identical if you use **a router running OpenWrt**.

1. [Part 1: Hello World]({{< ref "/blog/hns1-hello-world.md" >}})
2. [Part 2: HTTPS and TLS hardening]({{< ref "/blog/hns2-tls-hardening.md" >}}) ([harden SSH using this guide]({{< ref "/blog/wss1-basic-hardening.md#s3" >}}))
3. NAS (network-attached storage)
  * [Part 3a: Turris Omnia as network-attached storage via SMB]({{< ref "/blog/hns3-nas.md" >}})
  * [Part 3b: Turris Omnia as network-attached storage via Nextcloud]({{< ref "/blog/hns3-nextcloud.md" >}})
4. [Part 4: Turris Omnia as an ad blocker]({{< ref "/blog/hns4-adblocking.md" >}})
5. [Part 5: Client-side DNS security features]({{< ref "/blog/hns5-dns-configuration.md" >}})
